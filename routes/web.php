<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * WEB
 */

Route::get('/', 'Web\HomeController@index')->name('web.index');
Route::get('/referred/{code}', 'Web\UserController@referred')->name('web.referred');

Route::group(['middleware' => ['auth', 'notifications', 'CVChecked']], function () {


    Route::get('/home', 'Web\HomeController@home')->name('web.home');
    Route::get('/suscription/{plan}/checkout', 'Web\SuscriptionControllers@checkout')->name('web.suscription.checkout')->middleware('verified');;
    Route::get('/suscription/{plan}/finish', 'Web\SuscriptionControllers@checkoutFinish')->name('web.suscription.finish')->middleware('verified');;

    //Route::get('/notifications/add', 'Web\NotificationsControllers@add')->name('web.notifications.add');

    /*
     * Events
     */
    Route::get('/events/{event?}', 'Web\EventsControllers@event')->name('web.events');
    Route::get('/category', function (){
        return view('web.category-page');
    });
    /*
     * Profile
     */
    Route::get('/profile/{user}', 'Web\UserController@profile')->name('web.profile')->middleware('verified');
    Route::post('/profile/cv/store', 'Web\CVController@store')->name('web.profile.cv.store')->middleware('verified');
    Route::post('/profile/cv/additem', 'Web\CVController@addItem')->name('web.profile.cv.additem')->middleware('verified');
    //Route::match(['get', 'post'], '/profile/cv/additem', 'Web\CVController@addItem')->middleware('verified');


    Route::get('/profile/cv/delete/{fromTable}/{item}', 'Web\CVController@deleteItem')->name('web.profile.cv.deleteitem')->middleware('verified');
    Route::post('/profile/avatar/update', 'Web\CVController@avatarUpdate')->name('web.profile.avatar.update')->middleware('verified');

    /*
     * Search
     */
    Route::get('/search/{subcategory?}', 'Web\SearchController@index')->name('web.search')->middleware('verified');;
    Route::get('/home/search', 'Web\SearchController@homeSearch')->name('web.home.search')->middleware('verified');;
    //Route::get('/search/get', 'Web\SearchController@search')->name('web.search.get');
    Route::get('/search/subcategories/{id}', 'Web\SearchController@subcategories')->name('web.search.subcategories')->middleware('verified');;

});

/*
 * ADMIN
 */
Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/email', function (){
        return view('web.email.verify');
    });

    Route::get('/dashboard', 'Admin\HomeController@index')->name('dashboard');

    Route::resource('users', 'Admin\UsersController');

    Route::resource('events', 'Admin\EventsController',['only' => ['index', 'store', 'create', 'update', 'show', 'edit']]);
    Route::get('events/destroy/{id}', 'Admin\EventsController@destroy')->name('events.destroy');

    Route::resource('categories', 'Admin\CategoriesController',['only' => ['index', 'store', 'create', 'update', 'show', 'edit']]);
    Route::get('categories/destroy/{id}', 'Admin\CategoriesController@destroy')->name('categories.destroy');

    Route::resource('subcategories', 'Admin\SubCategoriesController',['only' => ['index', 'store', 'create', 'update', 'show', 'edit']]);
    Route::get('subcategories/destroy/{id}', 'Admin\SubCategoriesController@destroy')->name('subcategories.destroy');;

    Route::resource('memberships', 'Admin\MembershipsController');

    Route::resource('plans', 'Admin\PlansController');

    Route::resource('reviews', 'Admin\ReviewsController',['only' => ['index', 'store', 'create', 'update', 'show', 'edit']]);
    Route::get('reviews/destroy/{id}', 'Admin\ReviewsController@destroy')->name('reviews.destroy');

    /*Route::get('users', function () {
        // Matches The "/admin/users" URL
    });*/
});




Route::group([/*'middleware' => ['auth', 'role:admin'], */'prefix' => 'admin', 'as' => 'admin.'], function () {





});







/**
 * DESIGN
 */

/*Route::get('/', function (){
   return view('web.landing');
})->name('web');;*/

/*   return view('web.home');
});*/


/*Route::get('/home', function (){
    return view('web.landing');
 })->name('web.landing');*/

/*Route::get('/search', function (){
    return view('web.search');
})->name('web.search');*/

/**
 * EVENTOS
 */
Route::get('/eventos', function (){
    return view('web.eventos');
})->name('web.eventos');


/*
 * ORGANIZADOR
 */
/*Route::get('/organizador', function (){
    return view('web.organizador');
})->name('web.organizador');*/


//Auth::routes();

Auth::routes(['verify' => true]);

Route::get('/register/{role?}', 'Auth\RegisterController@showRegistrationForm')
    ->name('register');

Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider')
    ->name('social.login');
Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')
    ->name('social.callback');



/*Route::get('/', 'HomeController@index')->name('home');*/

/*Route::get('/search-category', function (){
    return view('web.category-page');
});*/

Route::get('/preguntas-frecuentes', function (){
    return view('web.preguntas');
})->name('web.preguntas');


/*Route::get('/checkout', function (){
    return view('web.checkout');
})->name('web.checkout');*/
