<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->references('id')->on('orders');
            //$table->foreignId('payment_method_id')->references('id')->on('payment_methods');
            $table->string('voucher_number')->nullable();
            $table->integer('order_number')->nullable();
            $table->boolean('paid')->nullable();
            $table->string('method')->nullable();
            $table->integer('method_id')->nullable();
            $table->dateTime('paid_date')->nullable();
            $table->integer('amount')->nullable();
            $table->dateTime('max_pay_date')->nullable();
            $table->string('hash')->nullable();
            $table->boolean('cancelled')->nullable();
            $table->string('token')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
