<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_information', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('doc_number');
            $table->bigInteger('cellphone');
            $table->date('born_date');
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->string('city_place_id')->nullable();
            $table->string('city_name')->nullable();
            $table->integer('profile_score');
            $table->foreignId('sex_id')->nullable()->references('id')->on('sexs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_information');
    }
}
