<?php

use Illuminate\Database\Seeder;

class SubCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_categories')->insert([
            'category_id' => 1,
            'subcategory' => 'Electricista',
            'active' => 1,
        ]);

        DB::table('sub_categories')->insert([
            'category_id' => 1,
            'subcategory' => 'Plomero',
            'active' => 1,
        ]);

        DB::table('sub_categories')->insert([
            'category_id' => 1,
            'subcategory' => 'Albañil',
            'active' => 1,
        ]);

        DB::table('sub_categories')->insert([
            'category_id' => 2,
            'subcategory' => 'Ingeniero Civil',
            'active' => 1,
        ]);

        DB::table('sub_categories')->insert([
            'category_id' => 2,
            'subcategory' => 'Arquitecto',
            'active' => 1,
        ]);

        DB::table('sub_categories')->insert([
            'category_id' => 2,
            'subcategory' => 'Otros',
            'active' => 1,
        ]);
    }
}
