<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Sexs extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('sexs')->truncate();
        DB::table('sexs')->insert([
            'sex' => 'Masculino',
        ]);

        DB::table('sexs')->insert([
            'sex' => 'Femenino',
        ]);
    }
}
