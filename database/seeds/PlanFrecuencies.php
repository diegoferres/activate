<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlanFrecuencies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plan_frecuencies')->insert([
            'frecuency' => 'Mensual',
        ]);

        DB::table('plan_frecuencies')->insert([
            'frecuency' => 'Trimestral',
        ]);

        DB::table('plan_frecuencies')->insert([
            'frecuency' => 'Anual',
        ]);
    }
}
