<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(Sexs::class);
        $this->call(PlanFrecuencies::class);
        $this->call(OrderStatuses::class);
        $this->call(Roles::class);
        $this->call(MembershipStatuses::class);
        $this->call(AdminUser::class);
        $this->call(Plans::class);
        $this->call(Days::class);
        $this->call(Categories::class);
        $this->call(SubCategories::class);
        $this->call(NotificationTypes::class);
    }
}
