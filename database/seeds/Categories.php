<?php

use Illuminate\Database\Seeder;

class Categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'category' => 'Servicio Técnico',
            'img_icon' => 'fas fa-cogs',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Construcción',
            'img_icon' => 'fas fa-hard-hat',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Belleza',
            'img_icon' => 'fas fa-spa',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Educación',
            'img_icon' => 'fas fa-chalkboard-teacher',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Bienestar',
            'img_icon' => 'fas fa-heartbeat',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Entretenimiento',
            'img_icon' => 'fas fa-dice',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Eventos',
            'img_icon' => 'fas fa-birthday-cake',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Hogar',
            'img_icon' => 'fas fa-cogs',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Home OFfice',
            'img_icon' => 'fas fa-laptop-house',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Moda',
            'img_icon' => 'fas fa-drafting-compass',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Ayuda',
            'img_icon' => 'fas fa-drafting-compass',
            'active' => 1,
        ]);

        DB::table('categories')->insert([
            'category' => 'Cursos',
            'img_icon' => 'fas fa-drafting-compass',
            'active' => 1,
        ]);
    }
}
