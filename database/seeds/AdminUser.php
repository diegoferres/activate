<?php

use Illuminate\Database\Seeder;

class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'Administrador',
            'email'     => 'admin@admin.com',
            'password'  => \Hash::make('admin'),
            'is_active' => 1,
        ]);

        DB::table('user_information')->insert([
            'user_id'       => 1,
            'doc_number'    => 1234567,
            'cellphone'     => 981666999,
            'born_date'     => '1985-01-01',
            'profile_score' => 1,
            'sex_id'        => 1,
        ]);

        DB::table('model_has_roles')->insert([
            'role_id'    => 1,
            'model_type' => 'App\User',
            'model_id'   => 1,
        ]);
    }
}
