<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'guard_name' => 'web',
        ]);

        DB::table('roles')->insert([
            'name' => 'profesional',
            'guard_name' => 'web',
        ]);

        DB::table('roles')->insert([
            'name' => 'ofertante',
            'guard_name' => 'web',
        ]);
    }
}
