<?php

use Illuminate\Database\Seeder;

class Plans extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            'name' => 'Plan 1',
            'subtitle' => '60 días Gratis invitando a 3 amigos',
            'price' => 120000,
            'items' => 'Disfruta del uso sin anuncios,Chat on-Time 24hs,Contacto vía Whatsapp,Puntuación y comentarios de tus Servicios,Integra tu perfil a Eventos,Promociona tu curso,Imprime tu CV.',
            'frecuency_id' => 1,
            'active' => 1,
        ]);


        DB::table('plans')->insert([
            'name' => 'Plan 2',
            'subtitle' => 'Auspicia tu perfil, sé el primero en la lista',
            'price' => 165000,
            'items' => 'Disfruta del uso sin anuncios,Chat on-Time 24hs,Contacto vía Whatsapp,Puntuación y comentarios de tus Servicios,Integra tu perfil a Eventos,Promociona tu curso,Imprime tu CV',
            'frecuency_id' => 1,
            'active' => 1,
        ]);

        DB::table('plans')->insert([
            'name' => 'Plan 3',
            'subtitle' => 'Mantén activo tu perfil los 365 días del año',
            'price' => 220000,
            'items' => 'Disfruta del uso sin anuncios,Chat on-Time 24hs,Contacto vía Whatsapp,Puntuación y comentarios de tus Servicios,Integra tu perfil a Eventos,Promociona tu curso,Imprime tu CV',
            'frecuency_id' => 3,
            'active' => 1,
        ]);
    }
}
