<?php

use Illuminate\Database\Seeder;

class Days extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('days')->insert([
            'day' => 'Domingo',
        ]);

        DB::table('days')->insert([
            'day' => 'Lunes',
        ]);

        DB::table('days')->insert([
            'day' => 'Martes',
        ]);

        DB::table('days')->insert([
            'day' => 'Miércoles',
        ]);

        DB::table('days')->insert([
            'day' => 'Jueves',
        ]);

        DB::table('days')->insert([
            'day' => 'Viernes',
        ]);

        DB::table('days')->insert([
            'day' => 'Sábado',
        ]);
    }
}
