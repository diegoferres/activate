<?php

use Illuminate\Database\Seeder;

class NotificationTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notification_types')->insert([
            'name' => 'Chat',
            'type' => 'chat',
        ]);
    }
}
