<?php

use Illuminate\Database\Seeder;

class OrderStatuses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->insert([
            'status' => 'Pendiente',
        ]);

        DB::table('order_statuses')->insert([
            'status' => 'Procesado',
        ]);
    }
}
