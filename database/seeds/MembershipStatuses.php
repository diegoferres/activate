<?php

use Illuminate\Database\Seeder;

class MembershipStatuses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('membership_statuses')->insert([
            'status' => 'Activo',
        ]);

        DB::table('membership_statuses')->insert([
            'status' => 'Inactivo',
        ]);

        DB::table('membership_statuses')->insert([
            'status' => 'Suspendido',
        ]);

        DB::table('membership_statuses')->insert([
            'status' => 'Cancelado',
        ]);
    }
}
