<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationTypes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
