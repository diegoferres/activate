<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CVLanguages extends Model
{
    protected $table = 'cv_languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lang_code',
        'language',
        'read',
        'understand',
        'speak',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
