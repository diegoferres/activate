<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_start',
        'date_expiration',
        'user_id',
        'plan_id',
        'status_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function plan()
    {
        return $this->belongsTo('App\Plans', 'plan_id');
    }

    public function status()
    {
        return $this->belongsTo('App\MembershipStatuses', 'status_id');
    }
}
