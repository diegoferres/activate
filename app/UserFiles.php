<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFiles extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'file_name', 'file_path', 'subcategory_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Users', 'user_id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\SubCategories', 'subcategory_id');
    }
}
