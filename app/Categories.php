<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category', 'img_icon', 'active'
    ];

    public function subcategories()
    {
        return $this->hasMany('App\SubCategories', 'category_id');
    }
}
