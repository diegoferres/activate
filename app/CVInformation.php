<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CVInformation extends Model
{
    protected $table = 'cv_information';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'about',
        'subcategory_id',
        'user_id',
        'work_status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\SubCategories', 'subcategory_id');
    }
}
