<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDays extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'day_id',
        'user_id',
        'hour_from',
        'hour_to'
    ];

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function day()
    {
        return $this->belongsTo('App\Days', 'day_id');
    }
}
