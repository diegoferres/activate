<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanFrecuencies extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'frecuency'
    ];

    public function plan()
    {
        return $this->hasMany('App\Plans', 'frecuency_id');
    }
}
