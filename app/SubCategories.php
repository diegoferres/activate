<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
    protected $table = 'sub_categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subcategory', 'category_id','event_id', 'active',
    ];

    public function category()
    {
        return $this->belongsTo('App\Categories', 'category_id');
    }

    public function event()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }

    public function users()
    {
        return $this->hasMany('App\CVInformation', 'subcategory_id');
    }
}
