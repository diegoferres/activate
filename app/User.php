<?php

namespace App;

use App\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use HasSlug;

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'users.name' => 10,
            'user_services.service' => 10,
            'user_services.description' => 10,
            'sub_categories.subcategory' => 6,
            'categories.category' => 5,
        ],
        'joins' => [
            'user_information' => ['user_information.user_id','users.id'],
            'user_services' => ['user_services.user_id','users.id'],
            'sub_categories' => ['user_services.subcategory_id','sub_categories.id'],
            'categories' => ['sub_categories.category_id','categories.id'],
        ],
        'groupBy'=>'users.id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug', 'avatar', 'avatar_thumb','referred_code','referred_count',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'social' => 'array',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    // BTW: in most of case you should keep email in lowercase
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    // User Information
    public function information()
    {
        return $this->hasOne('App\UserInformation', 'user_id');
    }

    // Notificatons
    public function notifications()
    {
        return $this->hasMany('App\Notifications', 'user_id');
    }

    // subcategories
    public function subcategories()
    {
        return $this->hasMany('App\UserSubCategories', 'user_id');
    }

    // ratings
    public function ratings()
    {
        return $this->hasMany('App\Ratings', 'user_id');
    }

    // membership
    public function membership()
    {
        return $this->hasOne('App\Mermership', 'user_id');
    }

    /*
     * CV
     */
    public function cv_information()
    {
        return $this->hasOne('App\CVInformation', 'user_id');
    }

    public function cv_work_experience()
    {
        return $this->hasMany('App\CVWorkExperience', 'user_id');
    }

    public function cv_academic_information()
    {
        return $this->hasMany('App\CVAcademicInformation', 'user_id');
    }

    public function cv_skills()
    {
        return $this->hasMany('App\CVSkills', 'user_id');
    }

    public function cv_languages()
    {
        return $this->hasMany('App\CVLanguages', 'user_id');
    }

    public function days()
    {
        return $this->hasMany('App\UserDays', 'user_id');
    }

    public function services()
    {
        return $this->hasMany('App\UserServices', 'user_id');
    }

    public function reviews()
    {
        return $this->hasManyThrough(
            'App\UserReviews',
            'App\UserServices',
            'user_id', // Foreign key on users table...
            'service_id' // Foreign key on posts table...
        );
    }

    public function files()
    {
        return $this->hasMany('App\UserFiles', 'user_id');
    }
}
