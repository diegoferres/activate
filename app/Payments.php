<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'payment_method_id',
        'voucher_number',
        'order_number',
        'paid',
        'method',
        'method_id',
        'paid_date',
        'amount',
        'max_pay_date',
        'hash',
        'cancelled',
        'token',
    ];
}
