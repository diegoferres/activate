<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatuses extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
    ];

    public function orders()
    {
        return $this->hasMany('App\Orders', 'status_id');
    }
}
