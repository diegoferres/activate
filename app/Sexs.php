<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sexs extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sex',
    ];

    public function users()
    {
        return $this->hasMany('App\Users', 'sex_id');
    }
}
