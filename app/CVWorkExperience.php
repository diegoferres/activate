<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CVWorkExperience extends Model
{
    protected $table = 'cv_work_experience';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position',
        'business',
        'from_date',
        'to_date',
        'user_id',
        'current_work',
        'antiquity'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
