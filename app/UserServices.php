<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserServices extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service',
        'description',
        'price_from',
        'price_to',
        'subcategory_id',
        'user_id'
    ];

    public function subcategory()
    {
        return $this->belongsTo('App\SubCategories', 'subcategory_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
