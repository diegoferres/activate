<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'doc_number',
        'cellphone',
        'born_date',
        'profile_score',
        'sex_id',
        'latitud',
        'longitud',
        'city_name',
        'city_place_id',
    ];

    // User
    public function user()
    {
        $this->belongsTo('App\User', 'user_id');
    }
    // Sex
    public function sex()
    {
        $this->belongsTo('App\Sexs', 'sex_id');
    }
}
