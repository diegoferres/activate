<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReviews extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'review',
        'service_id',
        'user_id',
        'report',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function service()
    {
        return $this->belongsTo('App\UserServices', 'service_id');
    }
}
