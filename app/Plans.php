<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'subtitle',
        'price',
        'items',
        'active',
        'frecuency_id'
    ];

    public function frecuency()
    {
        return $this->belongsTo('App\PlanFrecuencies', 'frecuency_id');
    }

    public function memberships()
    {
        return $this->hasMany('App\Membership', 'plan_id');
    }
}
