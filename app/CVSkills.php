<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CVSkills extends Model
{
    protected $table = 'cv_skills';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'skill',
        'percent',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
