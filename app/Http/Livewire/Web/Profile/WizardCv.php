<?php

namespace App\Http\Livewire\Web\Profile;

use App\Categories;
use App\CVAcademicInformation;
use App\CVInformation;
use App\CVWorkExperience;
use App\Days;
use App\Helpers\Message;
use App\UserDays;
use App\UserInformation;
use App\UserServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Livewire\WithFileUploads;

class WizardCv extends Component
{
    use WithFileUploads;

    public $currentStep = 1, $subcategories, $days;

    /*
     * STEP 1
     */
    public $about;

    /*
     * STEP 2
     */
    public $subcategory_id_main;

    /*
     * STEP 3
     */
    public $subcategory_id, $service, $price_from, $price_to;

    /*
     * STEP 4
     */
    public $pregunta, $work_status, $business, $antiquity, $current_work;

    /*
     * STEP 5
     */
    public $day, $hour_from, $hour_to;

    /*
     * STEP 6
     */
    public $institution, $institution_1, $institution_2, $from_year, $from_year_1, $from_year_2, $to_year, $to_year_1, $to_year_2, $title, $title_1, $title_2, $academic_file, $academic_file_1, $academic_file_2;

    /*
     * STEP 7
     */
    public $latitud, $longitud, $city_name, $city_place_id;

   /* protected $rules = [
        'about' => 'required',
        //'email' => 'required|email',
    ];*/

    /*protected $messages = [
        'email.required' => 'The Email Address cannot be empty.',
        'email.email' => 'The Email Address format is not valid.',
    ];*/

    protected $validationAttributes = [
        'about' => 'sobre ti'
    ];

    public function firstStep()
    {


        /*$this->validate([
            'about' => 'required',
        ]);*/

        $this->currentStep = 2;
    }

    public function secondStep()
    {
        /*$this->validate([
            'subcategory_id_main' => 'required',
        ]);*/

        $this->currentStep = 3;
    }

    public function thirdStep(/*$field*/)
    {
        //dd($field);
       /* $this->validate([
            'subcategory_id' => 'required',
            'service'       => 'required',
            'price_from'     => 'required',
            'price_to'       => 'required',
        ]);*/

        $this->currentStep = 4;
    }

    public function FourthStep()
    {
        /*$this->validate([
            'pregunta'      => 'required',
            'business'      => 'required_if:pregunta,=,1',
            'antique'       => 'required_if:pregunta,=,1',
            'current_work'  => 'required_if:pregunta,=,1',
            'work_status'  => 'required_if:pregunta,=,2',
        ]);*/

        $this->currentStep = 5;
    }

    public function FifthStep()
    {
       /* $this->validate([
            'day'           => 'required',
            'hour_from'     => 'required',
            'hour_to'       => 'required'
        ]);*/

        $this->currentStep = 6;
    }

    public function SixthStep()
    {

        //session()->flash('notifications', Message::message('AA', 'BB', 'success'));
        $this->validate([
            'institution' => 'required_with:from_year,to_year, title',
            'from_year' => 'required_with:institution,to_year, title, from_year',
            'to_year' => 'required_with:from_year,to_year, title, institution',
            'title' => 'required_with:from_year,to_year, institution',

            'institution_1' => 'required_with:from_year_1,to_year_1, title_1',
            'from_year_1' => 'required_with:institution_1,to_year_1, title_1, from_year_1',
            'to_year_1' => 'required_with:from_year_1,to_year_1, title_1, institution_1',
            'title_1' => 'required_with:from_year_1,to_year_1, institution_1',

            'institution_2' => 'required_with:from_year_2,to_year_2, title_2',
            'from_year_2' => 'required_with:institution_2,to_year_2, title_2, from_year_2',
            'to_year_2' => 'required_with:from_year_2,to_year_2, title_2, institution_2',
            'title_2' => 'required_with:from_year_2,to_year_2, institution_2',
        ]);

        $this->currentStep = 7;
    }

    /*public function mount($post)
    {
        dd($post);
        //$this->latitud = $post->latitud;
        //$this->content = $post->content;
    }*/

    public function SeventhStep()
    {
        //dd(str_replace(' ', '', $this->price_from));
        //dd($this->latitud);
        //dd($latitud);
        /*$this->validate([
            'subcategory_id_main' => 'required',
        ]);*/
        try {
            Log::info('Begin');
            DB::beginTransaction();

            $userinfo = UserInformation::where('user_id', Auth::id())->first();
            $userinfo->latitud          = $this->latitud;
            $userinfo->longitud         = $this->longitud;
            $userinfo->city_name        = $this->city_name;
            $userinfo->city_place_id    = $this->city_place_id;
            $userinfo->save();

            $cvinformation = CVInformation::firstOrNew(['user_id' => Auth::id()]);
            $cvinformation->about           = $this->about;
            $cvinformation->subcategory_id  = $this->subcategory_id_main;
            $cvinformation->work_status     = $this->work_status;
            $cvinformation->user_id         = Auth::id();
            $cvinformation->save();

            if ($this->business){
                Log::info('Work');
                $workexperience = new CVWorkExperience();
                $workexperience->business       = $this->business;
                $workexperience->current_work   = $this->current_work ? 1 : 0;
                $workexperience->antiquity      = $this->antiquity;
                $workexperience->user_id        = Auth::id();
                $workexperience->save();
            }

            if ($this->day == 'lv'){
                $days = Days::whereIn('id', [2,3,4,5,6])->get();
                foreach ($days as $d) {
                    $userdays = new UserDays();
                    $userdays->user_id = Auth::id();
                    $userdays->day_id = $d->id;
                    $userdays->hour_from = $this->hour_from;
                    $userdays->hour_to = $this->hour_to;
                    $userdays->save();
                }
            }else{
                $userdays = new UserDays();
                $userdays->user_id = Auth::id();
                $userdays->day_id = $this->day;
                $userdays->hour_from = $this->hour_from;
                $userdays->hour_to = $this->hour_to;
                $userdays->save();
            }


            if ($this->service){

                $services = new UserServices();
                $services->subcategory_id   = $this->subcategory_id;
                $services->service          = $this->service;
                $services->price_from       = str_replace(' ', '', $this->price_from);
                $services->price_to         = str_replace(' ', '', $this->price_to);
                $services->user_id          = Auth::id();;
                $services->save();
            }

            if ($this->institution && $this->from_year && $this->to_year && $this->title) {
                $academic = new CVAcademicInformation();
                $academic->institution = $this->institution;
                $academic->from_year = $this->from_year;
                $academic->to_year = $this->to_year;
                $academic->title = $this->title;
                $academic->user_id = Auth::id();
                $academic->save();

                if ($this->academic_file){
                    $academic->academic_file = basename($this->academic_file->store('public/user-files'));
                    $academic->save();
                }
            }

            if ($this->institution_1 && $this->from_year_1 && $this->to_year_1 && $this->title_1) {
                $academic = new CVAcademicInformation();
                $academic->institution = $this->institution_1;
                $academic->from_year = $this->from_year_1;
                $academic->to_year = $this->to_year_1;
                $academic->title = $this->title_1;
                $academic->user_id = Auth::id();
                $academic->save();
            }

            if ($this->institution_2 && $this->from_year_2 && $this->to_year_2 && $this->title_2) {
                $academic = new CVAcademicInformation();
                $academic->institution = $this->institution_2;
                $academic->from_year = $this->from_year_2;
                $academic->to_year = $this->to_year_2;
                $academic->title = $this->title_2;
                $academic->user_id = Auth::id();
                $academic->save();
            }

            DB::commit();
            //Session::put('notifications', Message::message('A','B','success'));
            //session('notifications') =
            Log::info('Commit');

            session()->flash('notifications', Message::message('Información guardada', 'Se ha guardado tu información correctamente', 'success'));

            return redirect()->route('web.profile', Auth::user()->slug);

        }catch (\Exception $e){
            DB::rollBack();
            session()->flash('notifications', Message::message('Error al guardar', 'No se pudo guardar tu información', 'danger'));
            Log::info('Rollback: '.$e->getMessage() );

        }

        /*$cvinformation = CVInformation::firstOrNew(['user_id' => Auth::id()]);
        $cvinformation->about           = $this->about;
        $cvinformation->subcategory_id  = $this->subcategory_id_main;
        $cvinformation->work_status     = $this->work_status;
        $cvinformation->user_id         = Auth::id();
        $cvinformation->save();

        if (!$this->work_status){
            $workexperience = new CVWorkExperience();
            $workexperience->business       = $this->business;
            $workexperience->current_work   = $this->current_work ? 1 : 0;
            $workexperience->antiquity      = $this->antiquity;
            $workexperience->user_id        = Auth::id();
            $workexperience->save();
        }

        if ($this->service){

            $services = new UserServices();
            $services->subcategory_id   = $this->subcategory_id;
            $services->service          = $this->service;
            $services->price_from       = $this->price_from;
            $services->price_to         = $this->price_to;
            $services->user_id          = Auth::id();;
            $services->save();
        }

        if ($this->institution) {
            $academic = new CVAcademicInformation();
            $academic->institution = $this->institution;
            $academic->from_year = $this->from_year;
            $academic->to_year = $this->to_year;
            $academic->title = $this->title;
            $academic->user_id = Auth::id();
            $academic->save();
        }

        if ($this->institution_1) {
            $academic = new CVAcademicInformation();
            $academic->institution = $this->institution_1;
            $academic->from_year = $this->from_year_1;
            $academic->to_year = $this->to_year_1;
            $academic->title = $this->title_1;
            $academic->user_id = Auth::id();
            $academic->save();
        }

        if ($this->institution_2) {
            $academic = new CVAcademicInformation();
            $academic->institution = $this->institution_2;
            $academic->from_year = $this->from_year_2;
            $academic->to_year = $this->to_year_2;
            $academic->title = $this->title_2;
            $academic->user_id = Auth::id();
            $academic->save();
        }

        if ($this->day) {

            $userdays = new UserDays();
            $userdays->user_id = Auth::id();
            $userdays->day_id = $this->day;
            $userdays->hour_from = $this->hour_from;
            $userdays->hour_to = $this->hour_to;
            $userdays->save();
        }*/

        /*$academic = new CVAcademicInformation();
        $academic->institution  = $this;
        $academic->from_year    = $request->from_year[$key];
        $academic->to_year      = $request->to_year[$key];
        $academic->title        = $request->title[$key];
        $academic->user_id      = Auth::id();
        $academic->save();*/

       /* $userdays = new UserDays();
        $userdays->user_id = Auth::id();
        $userdays->day_id = $day;
        $userdays->hour_from = $request->hour_from[$key];
        $userdays->hour_to = $request->hour_to[$key];
        $userdays->save();*/
    }

    public function back()
    {
        $this->currentStep = $this->currentStep - 1;
    }

    public function render()
    {
        $categories = Categories::with('subcategories')->orderBy('category', 'asc')->get();
        $this->days = Days::pluck('day', 'id');
        $this->days['lv'] = 'Lunes a Viernes';

        foreach ($categories as $category) {
            $this->subcategories[$category->category] = $category->subcategories->pluck('subcategory', 'id');
        }

        return view('livewire.web.profile.wizard-cv');
    }
}
