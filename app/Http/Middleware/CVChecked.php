<?php

namespace App\Http\Middleware;

use App\Helpers\Message;
use Closure;
use Illuminate\Support\Facades\Auth;

class CVChecked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user()->roles->first()->name);
        if (auth()->user()->getRoleNames()->first() == 'ofertante'){
            if (!Auth::user()->cv_information){
                if ($request->route()->getName() != 'web.profile' && $request->route()->getName() != 'web.profile.cv.store'){
                    return redirect()->route('web.profile', auth()->user()->slug)->with('cvimcomplete', 'incomplete')->with('notifications', Message::message('Primeramente', 'debes completar tu perfil básico', 'info'));
                }
            }
        }

        return $next($request);
    }
}
