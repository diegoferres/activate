<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Web\NotificationsController;
use Closure;
use Illuminate\Support\Facades\Auth;

class Notifications
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $notifications = new NotificationsController();
        $notifications->add(Auth::id());

        return $next($request);
    }
}
