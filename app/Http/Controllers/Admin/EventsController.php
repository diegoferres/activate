<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use App\Events;
use App\Http\Controllers\Controller;
use App\SubCategories;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $events = Events::all();

        if (request()->ajax()) {
            return datatables()->of($events)
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('admin.events.edit', $row->id).'" data-toggle="tooltip"  data-original-title="Editar" class="edit btn btn-primary btn-sm editItem">Editar</a>';

                    $btn = $btn.' <a href="'.route('admin.events.destroy', $row->id).'" data-toggle="tooltip" data-original-title="Eliminar" class="btn btn-danger btn-sm deleteItem">Eliminar</a>';
                    return $btn;
                })
                ->addColumn('subcategories', function($row){
                    $subcat = null;
                    foreach ($row->subcategories as $subcategory) {
                        $subcat .= '<div class="badge badge-info">'.$subcategory->subcategory.'</div><br>';
                    }

                    return $subcat;
                })
                ->rawColumns(['action', 'subcategories'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.events.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Categories::with('subcategories')->orderBy('category', 'asc')->get();
        foreach ($categories as $category) {
            $subcategories[$category->category] = $category->subcategories->pluck('subcategory', 'id');
        }

        return view('admin.events.form', compact('subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $event = Events::create(['event_title' => $request->event_title]);

        foreach ($request->subcategories as $subcategory) {
            $subcategory = SubCategories::find($subcategory)->update(['event_id' => $event->id]);
        }

        return redirect()->back()->with('success', 'Registro creado correctamente');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {

        $event = Events::find($id);
        $categories = Categories::with('subcategories')->orderBy('category', 'asc')->get();
        foreach ($categories as $category) {
            $subcategories[$category->category] = $category->subcategories->pluck('subcategory', 'id');
        }


        return view('admin.events.form', compact('event', 'subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $event = Events::find($id);
        $event->event_title = $request->event_title;
        $event->save();

        foreach ($request->subcategories as $subcategory) {
            $subcategory = SubCategories::find($subcategory)->update(['event_id' => $event->id]);
        }

        return redirect()->route('admin.events.index')->with('success', 'Registro actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $event = Events::find($id);
        $event->subcategories()->update(['event_id' => null]);
        $event->delete();

        return redirect()->back()->with('success', 'Registro eliminado correctamente');
    }
}
