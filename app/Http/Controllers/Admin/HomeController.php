<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ofertantes = User::whereHas(
            'roles', function($q){
            $q->where('name', 'ofertante');
        }
        )->get();

        $contratantes = User::whereHas(
            'roles', function($q){
            $q->where('name', 'contratantes');
        }
        )->get();

        $lastUsers = User::whereHas(
            'roles', function($q){
            $q->where('name', 'contratante')->orWhere('name', 'ofertante');
        }
        )->limit(10)->get();

        return view('admin.dashboard', compact('ofertantes','contratantes', 'lastUsers'));
    }
}
