<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use App\Http\Controllers\Controller;
use App\SubCategories;
use Illuminate\Http\Request;

class SubCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = SubCategories::with('category');

        if (request()->ajax()) {
            return datatables()->of($subcategories)
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('admin.subcategories.edit', $row->id).'" data-toggle="tooltip"  data-original-title="Editar" class="edit btn btn-primary btn-sm editItem">Editar</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Eliminar" class="btn btn-danger btn-sm deleteItem">Eliminar</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.subcategories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::pluck('category', 'id');

        return view('admin.subcategories.form', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subcategory = SubCategories::create($request->all());

        return redirect()->route('admin.subcategories.index')->with('success', 'Registro creado correctamente');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Categories::pluck('category', 'id');
        $subcategory = SubCategories::find($id);

        return view('admin.subcategories.form', compact('subcategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subcategory = SubCategories::find($id);
        $subcategory->category_id = $request->category_id;
        $subcategory->subcategory = $request->subcategory;
        $subcategory->save();

        return redirect()->route('admin.subcategories.index')->with('success', 'Registro actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
