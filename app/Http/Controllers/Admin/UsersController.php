<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Sexs;
use App\User;
use App\UserInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->with('roles');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('roles', function($row){
                    $roles = null;
                    foreach ($row->roles as $rol) {
                        $roles .= '<div class="badge badge-info">'.$rol->name.'</div><br>';
                    }

                    return $roles;
                })
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('admin.users.edit', $row->id).'" data-toggle="tooltip"  data-original-title="Editar" class="edit btn btn-primary btn-sm editItem">Editar</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Eliminar" class="btn btn-danger btn-sm deleteItem">Eliminar</a>';
                    return $btn;
                })
                ->rawColumns(['roles','action'])
                ->make(true);
        }

        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sexs = Sexs::pluck('sex','id');
        $roles = Role::pluck('name','id');

        return view('admin.users.form', compact('sexs', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                   => 'required',
            'email'                  => 'required|email',
            'information.doc_number' => 'required|numeric',
            'information.sex_id'     => 'required',
            'information.cellphone'  => 'required|numeric',
            'information.born_date'  => 'required|date',
            'password'               => 'required|string|min:8|confirmed',
        ])->setAttributeNames([
            'name'                   => 'Nombres y Apellidos',
            'email'                  => 'Correo',
            'information.doc_number' => 'Nro. Documento',
            'information.sex_id'     => 'Sexo',
            'information.cellphone'  => 'Celular',
            'information.born_date'  => 'Fecha Nac.',
            'password'               => 'Contraseña',

        ]);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput()->with('info', 'Faltan algunos datos');
        }

        $request->merge(['password' => Hash::make($request->password)]);
        $user = User::create($request->all());

        $information = new UserInformation();
        $information->user_id      = $user->id;
        $information->doc_number   = $request->information['doc_number'];
        $information->cellphone    = $request->information['cellphone'];
        $information->born_date    = $request->information['born_date'];
        $information->sex_id       = $request->information['sex_id'];
        $information->profile_score = 1;
        $information->save();

        $user->syncRoles($request->role_id);

        $user->save();

        return redirect()->back()->with('success', 'Registro creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $sexs = Sexs::pluck('sex', 'id');
        $roles = Role::pluck('name', 'id');

        return view('admin.users.form', compact('user', 'sexs', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'                   => 'required',
            'email'                  => 'required|email',
            'information.doc_number' => 'required|numeric',
            'information.sex_id'     => 'required',
            'information.cellphone'  => 'required|numeric',
            'information.born_date'  => 'required|date',
            'password'               => 'nullable|string|min:8|confirmed',
        ])->setAttributeNames([
            'name'                   => 'Nombres y Apellidos',
            'email'                  => 'Correo',
            'information.doc_number' => 'Nro. Documento',
            'information.sex_id'     => 'Sexo',
            'information.cellphone'  => 'Celular',
            'information.born_date'  => 'Fecha Nac.',
            'password'               => 'Contraseña',

        ]);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput()->with('info', 'Faltan algunos datos');
        }

        $user = User::find($id);
        $user->name = $request->name;

        if ($request->password){
            $user->password = Hash::make($request->password);
        }

        $user->syncRoles($request->role_id);

        $user->save();

        $user->information->doc_number  = $request->information['doc_number'];
        $user->information->cellphone   = $request->information['cellphone'];
        $user->information->born_date   = $request->information['born_date'];
        $user->information->sex_id      = $request->information['sex_id'];
        $user->information->save();


        return redirect()->back()->with('success', 'Registro actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
