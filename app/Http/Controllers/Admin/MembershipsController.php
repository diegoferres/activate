<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Membership;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MembershipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Membership::latest();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('admin.users.edit', $row->id).'" data-toggle="tooltip"  data-original-title="Editar" class="edit btn btn-primary btn-sm editItem">Editar</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Eliminar" class="btn btn-danger btn-sm deleteItem">Eliminar</a>';
                    return $btn;
                })
                ->rawColumns(['roles','action'])
                ->make(true);
        }

        return view('admin.memberships.index');
    }
}
