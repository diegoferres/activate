<?php

namespace App\Http\Controllers\Web;

use App\Categories;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\User;
use Chatify\Facades\ChatifyMessenger as Chatify;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the web index or home.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {


        if (Auth::check()) {

            return redirect()->route('web.home');
                //->with('notifications', Message::message('Bienvenido', 'Te has registrado exitosamente', 'info'));

        } else {
            $navShow = true;
            $categories = Categories::all();
            return view('web.home', compact('navShow', 'categories'));
        }
    }

    public function home()
    {
        /*dd(Auth::user()->roles->first()->name);*/

        //$query = Message::where(['to_id' => Auth::id(), 'seen' => 0])->count();
        //dd($query);

        $bidders = User::whereHas('roles', function ($q) {
            $q->where('name', 'ofertante');
        })->limit(5)->get();

        return view('web.landing', compact('bidders'));
    }
}
