<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Orders;
use App\Plans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuscriptionControllers extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function checkout($plan_id)
    {
        $plan = Plans::find($plan_id);

        return view('web.checkout', compact('plan'));
    }

    public function checkoutFinish($id)
    {
        $plan = Plans::find($id);
        $order = new Orders();
        $order->subtotal    = $plan->price;
        $order->plan_id     = $plan->id;
        $order->user_id     = Auth::id();
        $order->status_id   = 1;
        $order->save();

        $payment = new PaymentController();
        $hash = $payment->insert($order);

        return redirect('https://www.pagopar.com/pagos/' . $hash);
    }
}
