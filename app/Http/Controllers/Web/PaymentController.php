<?php


namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\Payments;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->publicKey = '3429e29882aca3760e0779fdbc23cbbb';
        $this->privateKey = 'b3721274c9fdd210fb0a0ad93fda22a3';
    }

    public function insert($order = false)
    {
        try {
            $amount = 0;

            $amount = $order->subtotal;
            $items = [
                "ciudad" => "1",
                "nombre" => $order->plan->name,
                //"cantidad" => $detail->quantity,
                "cantidad" => 1,
                "categoria" => "909",
                "public_key" => $this->publicKey,
                "url_imagen" => null,
                "descripcion" => $order->plan->subtitle,
                "id_producto" => $order->id,
                //"precio_total" => $detail->price * $detail->quantity,
                "precio_total" => $order->subtotal,
                "vendedor_telefono" => "",
                "vendedor_direccion" => "",
                "vendedor_direccion_referencia" => "",
                "vendedor_direccion_coordenadas" => ""
            ];

            $id = $order->id;

            $token = null;
            $token .= $this->privateKey;
            $token .= $id;
            $token .= strval(floatval($amount));
            $token = sha1($token);

            $data = [
                "token" => $token,
                "comprador" => [
                    "ruc" => "5554308-1",
                    "email" => "diegoferreiraescobar@gmail.com",
                    "ciudad" => null,
                    "nombre" => "Diego Ferreira",
                    "telefono" => "0981698506",
                    "direccion" => "",
                    "documento" => "5554308",
                    "coordenadas" => "",
                    "razon_social" => "Diego Ferreira",
                    "tipo_documento" => "CI",
                    "direccion_referencia" => null
                ],
                "public_key" => $this->publicKey,
                "monto_total" => $amount,
                "tipo_pedido" => "VENTA-COMERCIO",
                "compras_items" => [
                    [
                        "ciudad" => "1",
                        "nombre" => $order->plan->name,
                        "cantidad" => 1,
                        "categoria" => "909",
                        "public_key" => $this->publicKey,
                        "url_imagen" => "",
                        "descripcion" => "Plan ".$order->plan->frecuency->frecuency.' '.number_format($order->plan->price,0,',','.'),
                        "id_producto" => 2,
                        "precio_total" => $amount,
                        "vendedor_telefono" => "",
                        "vendedor_direccion" => "",
                        "vendedor_direccion_referencia" => "",
                        "vendedor_direccion_coordenadas" => ""
                    ]
                ],
                //"compras_items" => $items,
                "fecha_maxima_pago" => date('Y-m-d'),
                "id_pedido_comercio" => $id,
                "descripcion_resumen" => ""

            ];

            $data = json_encode($data);
            Log::info('JSON DATA | PAGOPAR: '.$data);

            // Create a client with a base URI
            $client = new Client(
                [
                    'base_uri' => 'https://api.pagopar.com/api/comercios/1.1/',
                    'timeout' => 60,
                    'allow_redirects' => true,
                ]
            );
            // Send a request
            $res = $client->request('POST', 'iniciar-transaccion',
                ['body' => $data]);

            $hash = collect(json_decode($res->getBody()->getContents()))['resultado'][0]->data;

            $token = $this->privateKey;
            $token .= $hash;
            $token = sha1($token);

            $payment = new Payments();
            $payment->order_id           = $order->id;
            //$payment->payment_method_id  = 2;
            $payment->paid               = false;
            $payment->hash               = $hash;
            $payment->token              = $token;
            $payment->save();

            return $hash;


        } catch (\Exception $e) {
            Log::warning('Error | PaymentController@insert: ' . $e->getMessage());
            dd('Error');
        }

    }

    public function response(Request $request)
    {
        dd($request['resultado'][0]['numero_pedido']);
        $payment = Payments::where('hash', $request['resultado'][0]['hash_pedido'])->first();

        $payment->paid         = $request['resultado'][0]['pagado'];
        $payment->order_number = $request['resultado'][0]['numero_pedido'];
        $payment->method       = $request['resultado'][0]['forma_pago'];
        $payment->method_id    = $request['resultado'][0]['forma_pago_identificador'];
        $payment->paid_date    = $request['resultado'][0]['fecha_pago'];
        $payment->amount       = $request['resultado'][0]['monto'];
        $payment->max_pay_date = $request['resultado'][0]['fecha_maxima_pago'];
        $payment->cancelled    = $request['resultado'][0]['cancelado'];
        $payment->save();

        $response = [[
            "pagado"                    => $payment->paid,
            "forma_pago"                => $payment->method,
            "fecha_pago"                => $payment->paid_date,
            "monto"                     => $payment->amount,
            "fecha_maxima_pago"         => $payment->max_pay_date,
            "hash_pedido"               => $payment->hash,
            "numero_pedido"             => $payment->order_number,
            "cancelado"                 => $payment->cancelled,
            "forma_pago_identificador"  => $payment->method_id,
            "token"                     => $payment->token
        ]];

        return response()->json($response);
    }

    public function checkOrder($hash)
    {
        try {
            $token = $this->privateKey;
            $token .= 'CONSULTA';
            $token = sha1($token);
            $data = [
                "hash_pedido" => $hash,
                "token" => $token,
                "token_publico" => $this->publicKey,
            ];

            $data = json_encode($data);

            // Create a client with a base URI
            $client = new Client(
                [
                    'base_uri' => 'https://api.pagopar.com/api/pedidos/1.1/',
                    'timeout' => 60,
                    'allow_redirects' => true,
                ]
            );
            // Send a request
            $res = $client->request('POST', 'traer',
                ['body' => $data]);

            return collect(json_decode($res->getBody()->getContents()));
        } catch (\Exception $e) {

            dd($e->getMessage());
        }
    }

    public function pagoparPaymentMethods()
    {
        try {

            $token = $this->privateKey;
            $token .= 'FORMA-PAGO';
            $token = sha1($token);
            $data = [
                "token" => $token,
                "token_publico" => $this->publicKey,
            ];

            $data = json_encode($data);

            // Create a client with a base URI
            $client = new Client(
                [
                    'base_uri' => 'https://api.pagopar.com/api/forma-pago/1.1/',
                    'timeout' => 60,
                    'allow_redirects' => true,
                ]
            );
            // Send a request
            $res = $client->request('POST', 'traer',
                ['body' => $data]);

            $methods = collect(json_decode($res->getBody()->getContents()))['resultado'];

            dd($methods);

        } catch (\Exception $e) {
            dd($e->getMessage());
        }

    }
}
