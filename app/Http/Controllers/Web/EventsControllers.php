<?php

namespace App\Http\Controllers\Web;

use App\Events;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Orders;
use App\Plans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventsControllers extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function event($event = false)
    {
        if ($event){
            $event = Events::where('event_slug', $event)->first();

            if (!$event){
                return redirect()->back()->with('notifications', Message::message('No existe', 'Aún no tenemos el evento', 'info'));
            }
            return view('web.organizador', compact('event'));
        }else{
            $events = Events::all();
            return view('web.eventos', compact('events'));
        }
    }

    public function checkoutFinish($id)
    {
        $plan = Plans::find($id);
        $order = new Orders();
        $order->subtotal    = $plan->price;
        $order->plan_id     = $plan->id;
        $order->user_id     = Auth::id();
        $order->status_id   = 1;
        $order->save();

        $payment = new PaymentController();
        $hash = $payment->insert($order);

        return redirect('https://www.pagopar.com/pagos/' . $hash);
    }
}
