<?php

namespace App\Http\Controllers\Web;

use App\Categories;
use App\Days;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\User;
use App\UserFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the web index or home.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('web.home');
        } else {
            $navShow = true;
            return view('web.home', compact('navShow'));
        }
    }

    public function profile($user)
    {
        $user = User::where('id', $user)->orWhere('slug', $user)->first();

        $categories = Categories::with('subcategories')->orderBy('category', 'asc')->get();
        $days = Days::pluck('day', 'id');

        foreach ($categories as $category) {
            $subcategories[$category->category] = $category->subcategories->pluck('subcategory', 'id');
        }


        $userSubcategories = UserFiles::where('subcategory_id', '!=', null)->where('user_id', $user->id)->groupBy(['id','subcategory_id'])->get();
        //dd($user->files()->where('subcategory_id', '!=', null)->get());

        $similars = User::query();
        //$similars->where('')

        return view('web.profile.profile', compact('user', 'subcategories', 'days', 'userSubcategories'));
    }

    public function referred($code)
    {
        //dd($code);
        if ($user = User::where('referred_code', $code)->first()){
            $user->referred_count = $user->referred_count + 1;
            $user->save();

            return redirect()->route('web.index')->with('notifications', Message::message('Tu código fue procesado', 'Bienvenido/a a Actívate', 'success'));
        }else{
            return redirect()->route('web.index')->with('notifications', Message::message('Tu código de referido es inválido', '', 'info'));
        }

    }
}
