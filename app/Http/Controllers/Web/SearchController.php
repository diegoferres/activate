<?php

namespace App\Http\Controllers\Web;

use App\Categories;
use App\Http\Controllers\Controller;
use App\SubCategories;
use App\User;
use App\UserInformation;
use http\QueryString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the web index or home.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        $categories = Categories::all();
        $users = User::all();
        $subc = null;

        if ($request->all()) {
            //dd('A');
            $input = $request->all();

            $users = User::query();

            if ($request->category_id) {

                $subc = Categories::find($request->category_id)->category;
                //dd($request->category_id);

                $category = $request->category_id;
                $users->whereHas('services.subcategory', function ($q) use ($category) {
                    $q->where('category_id', $category);
                })->orWhereHas('cv_information.subcategory', function ($z) use ($category) {
                    $z->where('category_id', $category);
                });
            }

            if ($request->subcategory_id) {

                $subc = $request->category_id;
                $search = Categories::whereHas('subcategories', function ($q) use ($subc){
                    $q->where('id', $subc);
                })->first()->category;

                $subcategory = $request->subcategory_id;
                $users->whereHas('services', function ($q) use ($subcategory) {
                    $q->where('subcategory_id', $subcategory);
                })->orWhereHas('cv_information', function ($z) use ($subcategory) {
                    $z->where('subcategory_id', $subcategory);
                });
            }

            if ($request->q) {

                $subc = $request->q;

                $users->whereHas('services', function ($q) use ($input) {
                    $q->whereHas('subcategory', function ($x) use ($input) {
                        $x->where('subcategory', 'like', '%' . $input['q'] . '%');
                    })->orWhere('service', 'like', '%' .$input['q'].'%')->orWhere('description', 'like', '%' .$input['q'].'%');
                })->orWhereHas('cv_information', function ($y) use ($input) {
                    $y->whereHas('subcategory', function ($x) use ($input) {
                        $x->where('subcategory', 'like', '%' . $input['q'] . '%');
                    });
                })->orWhere('name', 'like', '%'.$request->q.'%');
            }

            if ($request->place_id) {

                //$subc .= ' por '.UserInformation::where('city_place_id', $request->place_id)->first()->city_name;

                $users->whereHas('information', function ($q) use ($input) {
                    $q->where('city_place_id', $input['place_id']);
                });
            }

            $users = $users->get();

            //dd($users);


            return view('web.search', compact('categories', 'users', 'subc'));
        } else {

            return view('web.search', compact('categories', 'users', 'subc'));
        }


        return view('web.search', compact('categories'));

        //dd(User::search($query)->with('services')->get());
    }

    public function search(Request $request)
    {
        if ($request->subcategory_id) {

            $users = User::whereHas('services', function ($q) use ($request) {
                $q->where('subcategory_id', $request->subcategory_id);
            })->whereHas('cv_information', function ($q) use ($request) {
                $q->where('subcategory_id', $request->subcategory_id);
            })->get();

            return view('web.search', compact('categories'));
        } else {

            return view('web.search');
        }

    }

    public function homeSearch(Request $request)
    {
        return response()->json(User::search($request->q)->with('cv_information.subcategory')->get());
    }

    public function subcategories($id)
    {
        $subcategories = SubCategories::where('category_id', $id)->get();

        return response()->json($subcategories);

    }
}
