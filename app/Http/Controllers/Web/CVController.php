<?php

namespace App\Http\Controllers\Web;

use App\CVAcademicInformation;
use App\CVInformation;
use App\CVSkills;
use App\CVLanguages;
use App\CVWorkExperience;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\User;
use App\UserDays;
use App\UserFiles;
use App\UserInformation;
use App\UserRatings;
use App\UserReviews;
use App\UserServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class CVController extends Controller
{
    public function store(Request $request)
    {
        //dd($request->all());
        try {
            DB::beginTransaction();
            $userinfo = UserInformation::where('user_id', Auth::id())->first();
            $userinfo->latitud          = $request->latitud;
            $userinfo->longitud         = $request->longitud;
            $userinfo->city_name        = $request->city_name;
            $userinfo->city_place_id    = $request->city_place_id;
            $userinfo->save();

            $cvinformation = CVInformation::firstOrNew(['user_id' => Auth::id()]);
            $cvinformation->about           = $request->about;
            $cvinformation->subcategory_id  = $request->subcategory_id_main;
            $cvinformation->work_status     = $request->work_status;
            $cvinformation->user_id         = Auth::id();
            $cvinformation->save();

            if (!$request->work_status){
                $workexperience = new CVWorkExperience();
                $workexperience->business       = $request->business;
                $workexperience->current_work   = $request->current_work ? 1 : 0;
                $workexperience->antiquity      = $request->antiquity;
                $workexperience->user_id        = Auth::id();
                $workexperience->save();
            }

            foreach ($request->service as $key => $service) {
                if ($service != null){
                    $services = new UserServices();
                    $services->subcategory_id   = $request->subcategory_id[$key];
                    $services->service          = $service;
                    $services->price_from       = $request->price_from[$key];
                    $services->price_to         = $request->price_to[$key];
                    $services->user_id          = Auth::id();;
                    $services->save();
                }
            }

            foreach ($request->institution as $key => $item) {
                if ($item != null){
                    $academic = new CVAcademicInformation();
                    $academic->institution  = $item;
                    $academic->from_year    = $request->from_year[$key];
                    $academic->to_year      = $request->to_year[$key];
                    $academic->title        = $request->title[$key];
                    $academic->user_id      = Auth::id();
                    $academic->save();
                }
            }

            foreach ($request->day as $key => $day) {
                if ($day != null) {
                    $userdays = new UserDays();
                    $userdays->user_id = Auth::id();
                    $userdays->day_id = $day;
                    $userdays->hour_from = $request->hour_from[$key];
                    $userdays->hour_to = $request->hour_to[$key];
                    $userdays->save();
                }
            }

            DB::commit();

            return redirect()->back()->with('notifications', Message::message('Correcto', 'Información guardada correctamente', 'success'));
        }catch (\Exception $e){
            DB::rollBack();
            Log::info('CV STORE MSG'.$e->getMessage());
            //dd('A');

            return redirect()->back()->with('notifications', Message::message('No se pudieron almacenar los datos', 'Por favor, intente más tarde', 'warning'));
        }

        //return redirect()->back()->with('success', 'Datos guardados correctamente');
    }

    public function addItem(Request $request)
    {

        switch ($request->data_from){
            case 'cva': // User Avatar
                //dd($request->file('user_avatar'));
                //dd($request->user_avatar);
                $user = User::find(Auth::id());

                if($request->hasFile('user_avatar')){
                    if (Storage::exists('public/users-avatar/'.$user->avatar)){
                        Storage::delete('public/users-avatar/'.$user->avatar);
                    }
                    if (Storage::exists('public/users-avatar/'.$user->avatar_thumb)){
                        Storage::delete('public/users-avatar/'.$user->avatar_thumb);
                    }

                    $file_name = Str::uuid().'.jpg';
                    //dd($file_name);

                    $request->user_avatar->storeAs('public/users-avatar',$file_name);

                    Image::make($request->file('user_avatar'))
                        ->resize('400','400')
                        ->save(public_path('storage').'/users-avatar/'.'thumb_'.$file_name);

                    $user->avatar = $file_name;
                    $user->avatar_thumb = 'thumb_'.$file_name;
                    $user->save();
                }

                break;
            case 'cvi': // User Information

                $user = User::find(Auth::id());
                if ($request->name){
                    $user->name = $request->name;
                }
                $user->save();

                $information = CVInformation::where('user_id', Auth::id())->first();

                if ($request->about){
                    $information->about             = $request->about;
                }

                if ($request->subcategory_id_main){
                    $information->subcategory_id    = $request->subcategory_id_main;
                }

                $information->user_id           = Auth::id();
                $information->save();

                break;
            case 'cvai': //CV Academic Information
                $acainformation = new CVAcademicInformation();
                $acainformation->title          = $request->title;
                $acainformation->institution    = $request->institution;
                $acainformation->from_year      = $request->from_year;
                $acainformation->to_year      = $request->to_year;
                //$acainformation->from_date      = $request->from_date;
                //$acainformation->to_date        = $request->to_date;
                $acainformation->status         = $request->status;
                $acainformation->user_id        = Auth::id();
                $acainformation->save();

                break;

            case 'cvwe': //CV Work Experience

                $workexp = new CVWorkExperience();
                //$workexp->position    = $request->position;
                $workexp->business      = $request->business;
                $workexp->antiquity     = $request->antiquity;
                $workexp->current_work  = $request->current_work == 'true' ? 1 : 0;
                //$workexp->from_date   = $request->from_date;
                //$workexp->to_date     = $request->to_date;
                $workexp->user_id       = Auth::id();
                $workexp->save();

                break;

            case 'cvs': //CV Skills
                $skill = new CVSkills();
                $skill->skill           = $request->skill;
                $skill->percent         = $request->percent;
                $skill->user_id         = Auth::id();
                $skill->save();

                break;

            case 'cvf': //CV Files

                //dd($request->all());
                //dd(basename($request->file('user_file')->store('public/user-files')));
                if($request->hasFile('user_file')){


                    $file = new UserFiles();
                    $fileName               = basename($request->file('user_file')->store('public/user-files'));
                    $file->file_path        = '/user-files/'.$fileName;
                    $file->file_name        = $request->file_name;
                    //$file->subcategory_id   = UserServices::find($request->service_id)->subcategory_id;
                    $file->user_id          = Auth::id();
                    $file->save();
                }
                //dd($request->user_file);

                break;

            case 'cvl': //CV Languages
                $language = explode(',', $request->language);
                $lang = new CVLanguages();
                $lang->lang_code        = $language[0];
                $lang->language         = $language[1];
                $lang->read             = $request->read ? 1 : 0;
                $lang->understand       = $request->understand ? 1 : 0;
                $lang->speak            = $request->speak ? 1 : 0;
                $lang->user_id          = Auth::id();
                $lang->save();

                break;

            case 'us': //User Services
                $us = new UserServices();
                $us->subcategory_id     = $request->subcategory_id;
                $us->service            = $request->service;
                $us->price_from         = str_replace(' ', '', $request->price_from);
                $us->price_to           = str_replace(' ', '', $request->price_to);
                $us->user_id            = Auth::id();
                $us->save();

                break;

            case 'ur': //User Reviews
                $ur = new UserReviews();
                $ur->review     = $request->review;
                $ur->service_id = $request->service_id;
                $ur->user_id    = Auth::id();
                $ur->save();

                break;

            case 'cvrr': //User Reviews Report
                /*if ($request->report){
                    echo 'A';
                }else{
                    echo 'B';

                }
                dd($request->report);*/
                $ur = UserReviews::find($request->comment);
                $ur->report     = $request->report ?: false;
                //$ur->service_id = $request->service_id;
                //$ur->user_id    = Auth::id();
                $ur->save();

                break;

            case 'cvsch': //User Schedule
                $sch = new UserDays();
                $sch->day_id    = $request->day;
                $sch->hour_from = $request->hour_from;
                $sch->hour_to   = $request->hour_to;
                $sch->user_id   = Auth::id();
                $sch->save();

                break;

            case 'cvcr': //User Comment Reports
                $sch = new UserDays();
                $sch->day_id      = $request->day;
                $sch->hour_from = $request->hour_from;
                $sch->hour_to   = $request->hour_to;
                $sch->user_id   = Auth::id();
                $sch->save();

                break;

            case 'urat': //User Rating
                $urat = UserRatings::firstOrNew(['bidder_id' => Auth::id()]);
                $urat->score     = $request->score;
                $urat->user_id   = $request->user_id;
                $urat->bidder_id = Auth::id();
                $urat->save();

                return response()->json(['msg' => 'Calificación guardada'], 200);
        }

        return redirect()->back()->with('notifications', Message::message('Correcto', 'Se ha guardado tu información', 'success'));

    }

    public function deleteItem($fromTable, $item)
    {

        switch ($fromTable){
            case 'cvai': //CV Academic Information
                $cvai = CVAcademicInformation::find($item);
                if (Storage::exists('public/user-files/'.$cvai->academic_file)){
                    Storage::delete('public/user-files/'.$cvai->academic_file);
                }
                $cvai->delete();

                break;

            case 'cvwe': //CV Work Experience
                $cvwe = CVWorkExperience::destroy($item);

                break;

            case 'cvs': //CV Skills
                $cvs = CVSkills::destroy($item);

                break;

            case 'cvl': //CV Languages

                $cvl = CVLanguages::destroy($item);

                break;

            case 'us': //User Services
                $us = UserServices::destroy($item);

                break;

            case 'ud': //User Services
                $ud = UserDays::destroy($item);

                break;

            case 'cvf': //User Files

                //dd('A');
                $file = UserFiles::find($item);
                //dd($file);
                if (Storage::exists('public'.$file->file_path)){
                    Storage::delete('public'.$file->file_path);
                }

                $file->delete();

                break;
        }
        //dd($fromTable);

        return redirect()->back()->with('notifications', Message::message('Correcto', 'Se ha eliminado el item', 'success'));

    }
}
