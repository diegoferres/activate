<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Sexs;
use App\User;
use App\UserInformation;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'      => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'sex_id'    => ['required'],
            'doc_number'=> ['required'],
            'cellphone' => ['required', 'min:12', 'max:14'],
            'born_date' => ['required'],
            'password'  => ['sometimes', 'required', 'string', 'min:8', 'confirmed'],
        ])->setAttributeNames([
            'name'      => 'nombre y apellido',
            'email'     => 'correo',
            'sex_id'    => 'sexo',
            'doc_number'=> 'documento',
            'cellphone' => 'celular',
            'born_date' => 'fecha nacimiento',
            'password'  => 'contraseña',
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //dd($data['role']);
        /*try {

        }catch ()*/
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'referred_code' => $data['referred_code'],
            'password' => isset($data['password']) ? Hash::make($data['password']) : null,
            'social' => isset(Session::get('socialData')['name']) ? [
                Session::get('socialData')['provider'] => [
                    'id' => Session::get('socialData')['id'],
                    'token' => Session::get('socialData')['token'],
                ],
            ] : null
        ]);

        if ($data['avatar']) {
            $file = $data['avatar'];

            $fileContents = file_get_contents($file);
            $fileName = Str::uuid().".jpg";
            File::put(storage_path('app/public/users-avatar/').$fileName, $fileContents);
            $user->avatar = $fileName;
            $user->save();
        }

        $user->assignRole($data['role']);

        $userInfo = UserInformation::create([
            'user_id' => $user->id,
            'doc_number' => preg_replace("/[^0-9]/", "", $data['doc_number']),
            'cellphone' => preg_replace("/[^0-9]/", "", $data['cellphone']),
            'born_date' => date('Y-m-d', strtotime($data['born_date'])),
            'latitud' => $data['latitud'],
            'longitud' => $data['longitud'],
            'city_name' => $data['city_name'],
            'city_place_id' => $data['city_place_id'],
            'profile_score' => 1,
            'sex_id' => $data['sex_id']
        ]);

        Session::forget('socialData');

        return $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showRegistrationForm($role = false)
    {
        //dd($role);
        if (!$role){
            return redirect()->route('web.index')->with('notifications', Message::message('Debes elegir una de las opciones', '', 'info'));
        }
        $sexs = Sexs::pluck('sex', 'id');

        return view('auth.register', compact('sexs', 'role'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect()->route('web.home')->with('notifications', Message::message('Revisa tu correo', 'Te hemos enviado el link de verificación', 'success'));;
    }
}
