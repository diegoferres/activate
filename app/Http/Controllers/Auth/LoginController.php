<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Sexs;
use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Nyholm\Psr7\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(\Illuminate\Http\Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ], ['email.required' => 'El campo correo es requerido'], ['password.required' => 'El campo contraseña es requerido']);
    }

    public function redirectToProvider(string $provider)
    {
        Session::put('register_role', isset($_GET['role']) ? $_GET['role'] : null);
        try {
            $scopes = config("services.$provider.scopes") ?? [];
            if (count($scopes) === 0) {
                return Socialite::driver($provider)->redirect();
            } else {
                return Socialite::driver($provider)->scopes($scopes)->redirect();
            }
        } catch (\Exception $e) {
            abort(404);
        }
    }
    public function handleProviderCallback(string $provider)
    {
        try {
            $data = Socialite::driver($provider)->user();

            return $this->handleSocialUser($provider, $data);
        } catch (\Exception $e) {
            return redirect('login')->withErrors(['authentication_deny' => 'Login with '.ucfirst($provider).' failed. Please try again.']);
        }
    }
    public function handleSocialUser(string $provider, object $data)
    {
        $user = User::where([
            "social->{$provider}->id" => $data->id,
        ])->first();
        if (!$user) {
            $user = User::where([
                'email' => $data->email,
            ])->first();
        }
        if (!$user) {

            if (!Session::get('register_role')){

                return redirect()->route('web.index')
                    ->with('notifications', Message::message('Aún no te has registrado', 'Elige una de las opciones', 'info'));
            }

            return $this->createUserWithSocialData($provider, $data);
        }
        $social = $user->social;
        $social[$provider] = [
            'id' => $data->id,
            'token' => $data->token
        ];
        $user->social = $social;
        $user->save();

        return $this->socialLogin($user);
    }
    public function createUserWithSocialData(string $provider, object $data)
    {
        $socialData = [
            'name'      => $data->name,
            'email'     => $data->email,
            'id'        => $data->id,
            'token'     => $data->token,
            'provider'  => $provider,
            'avatar'    => $data->avatar
        ];

        Session::put('socialData',$socialData);

        $role = Session::get('register_role');

        Session::forget('register_role');

        return redirect()->route('register', $role);

    }

    public function socialLogin(User $user)
    {
        \Log::info('TESTING');
        auth()->loginUsingId($user->id);
        return redirect($this->redirectTo);
    }
}
