<?php

namespace App\Helpers;

class Message
{
    public static function message(string $title, string $message, string $type)
    {
        return json_encode([
            "title" => $title,
            "message" => $message,
            "type" => $type,
        ]);
    }
}
