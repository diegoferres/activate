<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Days extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'day',
    ];

    public function users()
    {
        return $this->hasMany('App\UserDays', 'day_id');
    }
}
