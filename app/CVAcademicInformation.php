<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CVAcademicInformation extends Model
{
    protected $table = 'cv_academic_information';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'institution',
        'from_year',
        'to_year',
        'to_date',
        'academic_file',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
