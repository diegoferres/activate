@extends('admin.templates.layout')
@section('content')
<div class="row">
    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4>Horizontal Form</h4>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => 'register', 'method' => 'post']) !!}


                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="nombres">Nombres</label>
                        <input type="text" class="form-control is-invalid" id="nombres" name="nombres" placeholder="Nombres">
                        <div class="valid-feedback">
                            Good job!
                        </div>
                        <div class="invalid-feedback">
                            Bad job!
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="apellidos">Apellidos</label>
                        <input type="text" class="form-control" id="apellidos" name="" placeholder="Apellidos">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="ci">C.I.</label>
                        <input type="number" class="form-control" id="ci" name="ci" placeholder="# Cédula">
                    </div>
                </div>
                <div class="form-row">

                    <div class="form-group col-md-4">
                        <label for="email">Correo</label>
                        <input type="text" class="form-control" id="email" placeholder="micorreo@gmail.com">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="telefono">Teléfono</label>
                        <input type="tel" class="form-control" id="telefono" name="telefono"
                               placeholder="0981 998 888">
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <button class="btn btn-primary">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('script')
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>--}}
    <!-- Template JS File -->
    <script src="/admin/assets/js/scripts.js"></script>
    <!-- Custom JS File -->
    <script src="/admin/assets/js/custom.js"></script>

    <script>
        function submit() {
            console.log($('#usuariosForm'))
        }
    </script>
@endsection