@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                @isset($user)
                    {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'put']) !!}
                @else
                    {!! Form::open(['route' => 'admin.users.store', 'method' => 'post']) !!}
                @endisset
                <div class="card-header">
                    <h4>Datos del Usuario</h4>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Nombres y Apellidos</label>
                            {!! Form::text('name', old('name'), ['class' => ($errors->has('name')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Correo</label>
                            {!! Form::email('email', old('email'), ['class' => ($errors->has('email')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="born_date">Fecha Nac.</label>
                            {!! Form::date('information[born_date]', old('information.born_date'), ['class' => ($errors->has('information.born_date')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('information.born_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="ci">Nro. Documento</label>
                            {!! Form::number('information[doc_number]', old('information.doc_number'), ['class' => ($errors->has('information.doc_number')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('information.doc_number')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-2">
                            <label for="ci">Celular</label>
                            {!! Form::number('information[cellphone]', old('information.cellphone'), ['class' => ($errors->has('information.doc_number')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('information.cellphone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            {{--{{ dd($errors->all()) }}--}}
                        </div>
                        <div class="form-group col-md-2">
                            <label for="telefono">Sexo</label>
                            {!! Form::select('information[sex_id]', $sexs , old('information.sex_id'), ['class' => ($errors->has('information.sex_id')) ? 'form-control is-invalid select2' : 'form-control select2', 'placeholder' => 'Selecciona el sexo']) !!}
                            @error('information.sex_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="password">Contraseña</label>
                            {!! Form::password('password', ['class' => ($errors->has('password')) ? 'form-control is-invalid' : 'form-control',]) !!}
                            @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="password_confirmation">Confirmar Contraseña</label>
                            {!! Form::password('password_confirmation', ['class' => 'form-control',]) !!}
                        </div>
                        <div class="form-group col-md-2">
                            <label for="role_id">Roles</label>

                            {!! Form::select('role_id[]', $roles , isset($user->roles) ? $user->roles->pluck('id') : null, ['class' => ($errors->has('role_id')) ? 'form-control is-invalid select2' : 'form-control select2', 'multiple']) !!}
                            @error('role_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection