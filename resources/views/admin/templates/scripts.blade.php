<!-- General JS Scripts -->
<script src="/admin/assets/js/app.min.js"></script>
<!-- JS Libraies -->
<script src="/admin/assets/bundles/apexcharts/apexcharts.min.js"></script>
<script src="/admin/assets/bundles/izitoast/js/iziToast.min.js"></script>
<script src="/admin/assets/bundles/select2/dist/js/select2.full.min.js"></script>
<!-- Page Specific JS File -->
<script src="/admin/assets/js/page/index.js"></script>
{{--<script src="/assets/js/page/toastr.js"></script>--}}
<!-- Template JS File -->
<script src="/admin/assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="/admin/assets/js/custom.js"></script>
<script>

    function signOut() {
        document.getElementById('logout-form').submit();
    }

    $(document).ready(function () {
        //$("#toastr-2").click(function () {
            var notif_success = "{{ session('success') }}";
            var notif_error = "{{ session('error') }}";
            var notif_info = "{{ session('info') }}";

            if (notif_success){
                iziToast.success({
                    title: notif_success,
                    //message: 'This awesome plugin is made by iziToast',
                    position: 'topRight'
                });
            } else if (notif_error){
                iziToast.error({
                    title: notif_error,
                    //message: 'This awesome plugin is made by iziToast',
                    position: 'topRight'
                });
            } else if (notif_info){
                iziToast.info({
                    title: notif_info,
                    //message: 'This awesome plugin is made by iziToast',
                    position: 'topRight'
                });
            }
        //});
    })
</script>

{{--
    <!-- JS Libraies -->
    <script src="assets/bundles/izitoast/js/iziToast.min.js"></script>
    <!-- Page Specific JS File -->
    <script src="assets/js/page/toastr.js"></script>
--}}
