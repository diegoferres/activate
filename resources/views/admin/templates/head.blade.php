<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
<title>Actívate</title>
<!-- General CSS Files -->
<link rel="stylesheet" href="/admin/assets/css/app.min.css">
<link rel="stylesheet" href="/admin/assets/bundles/izitoast/css/iziToast.min.css">
<link rel="stylesheet" href="/admin/assets/bundles/select2/dist/css/select2.min.css">
<!-- Template CSS -->
<link rel="stylesheet" href="/admin/assets/css/style.css">
<link rel="stylesheet" href="/admin/assets/css/components.css">
<!-- Custom style CSS -->
<link rel="stylesheet" href="/admin/assets/css/custom.css">
<link rel='shortcut icon' type='image/x-icon' href='/admin/assets/img/favicon.ico'/>
