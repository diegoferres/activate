<!DOCTYPE html>
<html lang="en">
<!-- index.html  21 Nov 2019 03:44:50 GMT -->
<head>
    @include('admin.templates.head')
    @yield('head')
</head>
<body>
<div class="loader"></div>
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <!-- NAVBAR -->
        @include('admin.templates.nav')
        <!-- END NAVBAR -->
        <!-- SIDEBAR -->
        @include('admin.templates.sidebar')
        <!-- END SIDEBAR -->
        <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                @yield('content')
            </section>
            @include('admin.templates.settings_sidebar')
        </div>
        <footer class="main-footer">
            <div class="footer-left">
                <a href="/">EntraSinPapel</a>
            </div>
            <div class="footer-right">
            </div>
        </footer>
    </div>
</div>
@include('admin.templates.scripts')
@yield('script')
</body>
<!-- index.html  21 Nov 2019 03:47:04 GMT -->
</html>