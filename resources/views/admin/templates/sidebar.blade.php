<?php

use Illuminate\Support\Facades\Route;

$method = Route::currentRouteName();
?>
<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="/"> <img alt="image" src="/admin/images/logo-mini.png" class="header-logo"/> <span
                        class="logo-name">Actívate</span>
            </a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Principal</li>
            <li class="dropdown {{ $method == 'admin.dashboard' ? 'active' : ''}}">
                <a href="{{ route('admin.dashboard') }}" class="nav-link"><i data-feather="monitor"></i><span>Tablero</span></a>
            </li>
            <li class="menu-header">Seguridad</li>
            <li class="{{ $method == 'admin.users.index' || $method == 'admin.users.create' || $method == 'admin.users.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.users.index') }}"><i data-feather="users"></i><span>Usuarios</span></a>

            <li class="menu-header">Administración</li>
            <li class="{{ $method == 'admin.events.index' || $method == 'admin.events.create' || $method == 'admin.events.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.events.index') }}"><i data-feather="users"></i><span>Eventos</span></a>

            <li class="{{ $method == 'admin.users.index' || $method == 'admin.users.create' || $method == 'admin.users.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.memberships.index') }}"><i data-feather="users"></i><span>Membresías</span></a>

            <li class="{{ $method == 'admin.plans.index' || $method == 'admin.plans.create' || $method == 'admin.plans.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.plans.index') }}"><i data-feather="users"></i><span>Planes</span></a>

            <li class="{{ $method == 'admin.users.index' || $method == 'admin.users.create' || $method == 'admin.users.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.users.index') }}"><i data-feather="users"></i><span>Profesionales</span></a>

            <li class="{{ $method == 'admin.users.index' || $method == 'admin.users.create' || $method == 'admin.users.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.users.index') }}"><i data-feather="users"></i><span>Ofertantes</span></a>

            <li class="{{ $method == 'admin.reviews.index' || $method == 'admin.reviews.create' || $method == 'admin.reviews.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.reviews.index') }}"><i data-feather="users"></i><span>Comentarios</span></a>

            <li class="menu-header">Configuración</li>
            <li class="{{ $method == 'admin.categories.index' || $method == 'admin.categories.create' || $method == 'admin.categories.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.categories.index') }}"><i data-feather="users"></i><span>Categorías</span></a>
            <li class="{{ $method == 'admin.subcategories.index' || $method == 'admin.subcategories.create' || $method == 'admin.subcategories.edit' ? 'active' : '' }}">
                <a class="nav-link active" href="{{ route('admin.subcategories.index') }}"><i data-feather="users"></i><span>SubCategorías</span></a>
        </ul>
    </aside>
</div>
