@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Categorias</h4>
                    <div class="card-header-form">
                        <form>
                            <div class="input-group">
                                {{--<div class="input-group-btn">
                                    <a href="--}}{{--{{ route('admin.users.create') }}--}}{{--" class="btn btn-primary"><i
                                            class="fas fa-plus"> Crear</i>
                                    </a>
                                </div>--}}
                                &nbsp;
                                <input type="text" class="form-control" placeholder="Sección">
                                {!! Form::select('name', $sectionTypes, null , ['class' => 'form-control']) !!}
                                {{--<input type="text" class="form-control" placeholder="Sección">--}}
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary">Agregar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body p-3">
                    <div class="table-responsive">
                        <table class="table table-striped data-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Sección</th>
                                <th>Activo</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($sections as $section)
                                    <tr>
                                        <td class="p-0 text-center">{{ $section->id }}</td>
                                        <td>{{ $section->section }}</td>
                                        <td>{{ $section->active }}</td>
                                        <td><a href="{{ route('admin.users.edit', [$section->id]) }}" class="btn btn-primary">Editar</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
