@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                @isset($category)
                    {!! Form::model($category, ['route' => ['admin.categories.update', $category->id], 'method' => 'put']) !!}
                    <div class="card-header">
                        <h4>Editar categoría</h4>
                    </div>

                @else
                    {!! Form::open(['route' => 'admin.categories.store', 'method' => 'post']) !!}
                    <div class="card-header">
                        <h4>Crear categoría</h4>
                    </div>
                @endisset

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Categoría</label>
                            {!! Form::text('category', old('category'), ['class' => ($errors->has('category')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('category')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="name">Imagen/Icono</label>
                            {!! Form::text('img_icon', old('img_icon'), ['class' => ($errors->has('img_icon')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('img_icon')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a href="{{ route('admin.categories.index') }}" class="btn btn-secondary">Volver</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
