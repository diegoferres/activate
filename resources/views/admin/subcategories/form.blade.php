@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                @isset($subcategory)
                    {!! Form::model($subcategory, ['route' => ['admin.subcategories.update', $subcategory->id], 'method' => 'put']) !!}
                    <div class="card-header">
                        <h4>Editar subcategoría</h4>
                    </div>
                @else
                    {!! Form::open(['route' => 'admin.subcategories.store', 'method' => 'post']) !!}
                    <div class="card-header">
                        <h4>Crear subcategoría</h4>
                    </div>
                @endisset

                <div class="card-body">
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="name">Categoría</label>
                            {!! Form::select('category_id', $categories , old('category_id') , ['class' => 'form-control', 'placeholder' => 'Elige la categoría']) !!}
                            @error('category_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="name">SubCategoría</label>
                            {!! Form::text('subcategory', old('subcategory'), ['class' => ($errors->has('subcategory')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('subcategory')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a href="{{ route('admin.subcategories.index') }}" class="btn btn-secondary">Volver</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
