@extends('admin.templates.layout')
@section('head')
    {{-- DataTable --}}
    <link href="https://cdn.datatables.net/1.10.17/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Comentarios</h4>
                    <div class="card-header-form">
                        <form>
                            <div class="input-group">
                                 <div class="input-group-btn">
                                     <a href="{{ route('admin.reviews.create') }}" class="btn btn-primary"><i
                                                 class="fas fa-plus"> Crear</i>
                                     </a>
                                 </div>
                                {{--<input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary"><i class="fas fa-search"></i>
                                    </button>
                                </div>--}}
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body p-3">
                    <div class="table-responsive">
                        {{--<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span>--}}
                        <table class="table table-striped data-table">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Usuario</th>
                                <th>Servicio</th>
                                <th>Comentado por</th>
                                <th>Comentario</th>
                                <th>Reportado</th>
                                <th>Creado el</th>
                                <th width="15%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($reviews as $review)
                                <tr>
                                    <td>{{ $review->id }}</td>
                                    <td>{{ $review->service->user->name }}</td>
                                    <td>{{ $review->service->service }}</td>
                                    <td>{{ $review->user->name }}</td>
                                    <td>{{ $review->review }}</td>
                                    <td>{{ $review->report ? 'SI' : 'NO'  }}</td>
                                    <td>{{ $review->created_at }}</td>
                                    <td>
                                        <a href="{{ route('admin.reviews.destroy', $review->id) }}"
                                           class="edit btn btn-primary btn-sm">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- Page Specific JS File -->
    {{--<script src="/assets/bundles/apexcharts/apexcharts.min.js"></script>

        <!-- Template JS File -->
        <script src="/assets/js/scripts.js"></script>
        <!-- Custom JS File -->
        <script src="/assets/js/custom.js"></script>--}}
    <script src="/admin/assets/js/page/forms-advanced-forms.js"></script>

    {{--DataTable--}}


@endsection
