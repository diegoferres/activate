@extends('admin.templates.layout')
@section('head')
    <link rel="stylesheet" href="/admin/assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                @isset($plan)
                    {!! Form::model($plan, ['route' => ['admin.plans.update', $plan->id], 'method' => 'put']) !!}
                    <div class="card-header">
                        <h4>Editar plan</h4>
                    </div>
                @else
                    {!! Form::open(['route' => 'admin.plans.store', 'method' => 'post']) !!}

                    <div class="card-header">
                        <h4>Crear plan</h4>
                    </div>
                @endisset

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="name">Nombre</label>

                            {!! Form::text('name', old('name'), ['class' => ($errors->has('name')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Subtitulo</label>
                            {!! Form::text('subtitle', old('subtitle'), ['class' => ($errors->has('subtitle')) ? 'form-control is-invalid' : 'form-control' ]) !!}
                            @error('subtitle')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="born_date">Precio</label>
                            {!! Form::number('price', old('price'), ['class' => ($errors->has('price')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('price')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-2">
                            <label for="ci">Frecuencia</label>
                            {!! Form::select('frecuency_id', $frecuencies , isset($plan) ? $plan->frecuency_id : null, ['class' => ($errors->has('frecuency_id')) ? 'form-control is-invalid select2' : 'form-control select2', 'multiple']) !!}
                            @error('frecuency_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label class="custom-switch mt-2">
                                {!! Form::checkbox('active', '1', null,  ['id' => 'name', 'class' => 'custom-switch-input']) !!}
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Activo</span>
                            </label>
{{--                            <label class="custom-switch mt-2">
                                --}}{{--<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">--}}{{--


                            </label>--}}
                            {{--<label for="ci">Activo</label>

                            @error('frecuency_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror--}}
                        </div>
                    </div>
                    <label for="ci"><strong>Items del plan</strong></label>
                    <div class="form-row">
                        {!! Form::text('items', isset($plan->items) ? $plan->items : old('items'), ['class' => ($errors->has('items')) ? 'form-control is-invalid inputtags' : 'form-control inputtags', 'placeholder' => 'Separar por coma' ]) !!}
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="/admin/assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".inputtags").tagsinput({
                trimValue: true
            });
        })
    </script>
@endsection
