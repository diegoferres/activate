@extends('admin.templates.layout')
@section('content')
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                @isset($event)
                    {!! Form::model($event, ['route' => ['admin.events.update', $event->id], 'method' => 'put']) !!}
                    <div class="card-header">
                        <h4>Editar evento</h4>
                    </div>

                @else
                    {!! Form::open(['route' => 'admin.events.store', 'method' => 'post']) !!}
                    <div class="card-header">
                        <h4>Crear evento</h4>
                    </div>
                @endisset

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Evento</label>
                            {!! Form::text('event_title', old('event_title'), ['class' => ($errors->has('event')) ? 'form-control is-invalid' : 'form-control', 'required' ]) !!}
                            @error('event')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="name">SubCategorias</label>
                            {!! Form::select('subcategories[]', $subcategories , isset($event) ? $event->subcategories->pluck('id') : [] , ['class' => 'form-control select2', 'multiple', 'required']) !!}
                            {{--{!! Form::text('event', old('event'), ['class' => ($errors->has('event')) ? 'form-control is-invalid' : 'form-control' ]) !!}--}}
                            @error('event')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a href="{{ route('admin.events.index') }}" class="btn btn-secondary">Volver</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
