<div class="col-sm-6 d-none d-lg-block d-md-none"
     style="background: #4F86C4; padding: 2em 4em 0 4em; min-height: 39em; font-family: GalanoGrotesqueAlt;
                             ">
    <br><br>
    <div id="blogCarousel" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators" style="top:29em">
            <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#blogCarousel" data-slide-to="1"></li>
            <li data-target="#blogCarousel" data-slide-to="2"></li>
        </ol>
        <!-- Carousel items -->
        <div class="carousel-inner"
             style="background-color: transparent!important; box-shadow: none !important;">
            <div class="carousel-item active">
                <div class="row">
                    <div class="col" data-aos="fade-up" data-aos-duration="4000">
                        <div class="team-player">
                            <img src="/web/gif/lg_rg1.gif" width="50%">
                            <div class="slider_register">
                                <h4 class=" text-white">Una plataforma online que revoluciona el
                                    futuro del mercado de trabajo y promueve el empleo en un
                                    ecosistema digital que accede directamente al servicio que
                                    necesitas!</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!--.row-->
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col" data-aos="fade-up" data-aos-duration="4000">
                        <div class="team-player">
                            <img src="/web/gif/lg_rg2.gif" width="50%" alt="">
                            <div class="slider_register">
                                <h4 class="text-white">Sabemos que buscar trabajo es, mucho
                                    trabajo, te tomaria muchas horas de la semana para encontrar
                                    una oportunidad, con ACTÍVATE tienes eso resuelto, y ya no
                                    tendrias que pasar tanto tiempo buscando, porque ya estarias
                                    disponible y con oportunidades!</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!--.row-->
            </div>
            <!--.item-->
            <div class="carousel-item">
                <div class="row">
                    <div class="col" data-aos="fade-up" data-aos-duration="4000">
                        <div class="team-player">
                            <img src="/web/gif/lg_rg3.gif" width="50%" alt="">
                            <div class="slider_register">
                                <h4 class="text-white">Nos hemos colocado en el Camino para
                                    crear el mejor mercado de trabajo con los mejores
                                    Profesionales y Trabajadores, en Paraguay. Que estas
                                    Esperando? Registrate!</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!--.row-->
            </div>
            <!--.item-->
        </div>
        <!--.carousel-inner-->
    </div>
</div>
