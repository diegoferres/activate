@extends('web.layouts.app')
@section('title')
    Actívate - Registrarse
@endsection
@section('style')
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">

    <style>
        .slider_register {
            top: 0rem;
            position: relative;
        }

        .text-white {
            font-family: GalanoGrotesqueAlt;
        }

        input {
            font-size: 18.2px !important;
        }

        select {
            font-size: 18.2px !important;
            color: #004897 !important;

        }

        input::placeholder {
            color: #004897 !important;
            font-size: 18.2px;
        }

        .btn.btn-icon, .navbar .navbar-nav > a.btn.btn-icon {
            height: 3.375rem !important;
            min-width: 3.375rem !important;
            width: 3.375rem !important;
            padding: 0;
            font-size: 1.5rem !important;
            overflow: hidden;
            position: relative;
            line-height: normal;
        }

        .btn-fa-facebook {
            background-color: #004897;
            color: #fff;
        }

        .btn-fa-google {
            background-color: #dd3939;
            color: #fff;
        }


    </style>
@endsection
@section('content')
    <div class="wrapper">
        <div class="page-header" style="max-height: 100% !important;
        ">

            <div class="content" style="margin-top:7%">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 ml-auto mr-auto" style="background-color:white;">
                            <div class="card card-login card-plain">
                                {!! Form::open(['route' => 'register', 'method' => 'post']) !!}
                                @csrf
                                <br>
                                <h3 style="color:#004897">Registrarse</h3>
                                <hr>


                                <input type="hidden" name="referred_code" value="{{ \Str::random(12) }}">
                                <input type="hidden" name="role" value="{{ $role }}">
                                <input type="hidden" name="avatar"
                                       value="{{ Session::get('socialData') ? Session::get('socialData')['avatar'] : null }}">
                                <div class="text-center">
                                    <a class="btn btn-icon btn-round btn-fa-facebook"
                                       href="{{ route('social.login', ['provider' => 'facebook', 'role' => $role]) }}"><i
                                            class="fab fa-facebook-f"></i></a>
                                    <a class="btn btn-icon btn-round btn-fa-google"
                                       href="{{ route('social.login', ['provider' => 'google', 'role' => $role]) }}"><i
                                            class="fab fa-google"></i></a>
                                </div>
                                <div class="card-body">
                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons users_circle-08"></i>
                                                </span>
                                        </div>
                                        <input name="name" type="text"
                                               class="form-control @error('name') is-invalid @enderror"
                                               placeholder="Nombre y Apellido"
                                               value="{{ Session::get('socialData') ? Session::get('socialData')['name'] : old('name') }}">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons ui-1_email-85"></i>
                                                </span>
                                        </div>
                                        <input name="email" type="text"
                                               class="form-control @error('email') is-invalid @enderror"
                                               placeholder="Correo"
                                               value="{{ Session::get('socialData') ? Session::get('socialData')['email'] : old('email') }}">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="input-group no-border input-lg">
                                        {{--<div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons "></i>
                                                </span>
                                        </div>--}}
                                        {!! Form::select('sex_id', $sexs , null , ['class' => $errors->has('sex_id') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Elige el sexo']) !!}
                                        @error('sex_id')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>
                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons business_badge"></i>
                                                </span>
                                        </div>


                                        {!! Form::text('doc_number', null , ['class' => $errors->has('doc_number') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Nro. Documento', 'maxlength' => '10']) !!}
                                        @error('doc_number')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>


                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons tech_mobile"></i>
                                                </span>
                                        </div>


                                        {!! Form::text('cellphone', old('cellphone'), ['class' => $errors->has('cellphone') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Celular']) !!}
                                        @error('cellphone')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons ui-1_calendar-60"></i>
                                                </span>
                                        </div>


                                        {!! Form::text('born_date', old('born_date') , ['class' => $errors->has('born_date') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Fecha de Nacimiento']) !!}
                                        @error('born_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    @if(!Session::exists('socialData'))
                                        <div class="input-group no-border input-lg">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                      <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                                    </span>
                                            </div>
                                            <input name="password" type="password" placeholder="Contraseña"
                                                   class="form-control @error('password') is-invalid @enderror"/>
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="input-group no-border input-lg">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                      <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                                    </span>
                                            </div>
                                            <input name="password_confirmation" type="password"
                                                   placeholder="Confirmar contraseña" class="form-control"/>
                                        </div>
                                    @endif
                                    <h5 style="color:#004897" data-toggle="collapse" data-target="#mapa">Selecciona
                                        tu ubicación <i data-toggle="collapse" data-target="#mapa"
                                                        class="fas fa-map-marker-alt"></i></h5>
                                    {{-- <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                         Link with href
                                     </a>--}}

                                    <div id="mapa" class="collapse">
                                        <a href="javascript:void(0)" class="btn btn-primary" id="btn-location"><i
                                                class="fa fa-map-marker-alt"></i> Ir a mi ubicación</a>
                                        <div id="map" style="height: 350px;"></div>
                                    </div>
                                </div>

                                <input type="hidden" name="latitud" id="latitud">
                                <input type="hidden" name="longitud" id="longitud">
                                <input type="hidden" name="city_name" id="city_name">
                                <input type="hidden" name="city_place_id" id="city_place_id">

                                <div class="card-footer text-center">
                                    <button type="submit" style="font-size:1.2rem"
                                            class="btn btn-primary btn-round btn-lg btn-block">
                                        Comenzar
                                    </button>
                                    <br>
                                    <div class="text-center">
                                        <h5 style="color: #4F86C4;">

                                            ¿Ya tienes una cuenta?
                                            <a href="{{ route('login') }}" class="link" style="color:#004897">Iniciar
                                                sesión</a>
                                        </h5>
                                    </div>


                                    {!! Form::close() !!}


                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>


        @include('web.layouts.footer')
        @include('web.layouts.scripts')
        <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>

        {{-- MAPA WIZARD --}}

        <script type="text/javascript">
            // Note: This example requires that you consent to location sharing when
            // prompted by your browser. If you see the error "The Geolocation service
            // failed.", it means you probably did not give permission for the browser to
            // locate you.
            var map, infoWindow, marker;
            var geocoder;
            var btnLocation = document.getElementById("btn-location");
            var myLocation = {
                lat: -25.2819,
                lng: -57.635
            };

            function initMap() {

                geocoder = new google.maps.Geocoder();
                map = new google.maps.Map(document.getElementById('map'), {
                    center: myLocation,
                    zoom: 10
                });

                marker = new google.maps.Marker({
                    position: myLocation,
                    map: map,
                    draggable: true,
                    title: 'Tu posición'
                });

                infoWindow = new google.maps.InfoWindow;
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {

                        $('#latitud').val(position.coords.latitude);
                        $('#longitud').val(position.coords.latitude);

                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        myLocation.lat = position.coords.latitude;
                        myLocation.lng = position.coords.longitude;

                        /*marker = new google.maps.Marker({
                            position: myLocation,
                            map: map,
                            draggable: true,
                            title: 'Tu posición'
                        });*/

                        /*infoWindow.setPosition(pos);
                        infoWindow.setContent('Your position');
                        marker.addListener('click', function() {
                          infoWindow.open(map, marker);
                        });
                        infoWindow.open(map, marker);*/

                        marker.setPosition(myLocation);

                        map.setCenter(myLocation);


                        updateMarkerPosition(marker.getPosition());
                        geocodePosition(pos);


                    }, function () {
                        //handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    //handleLocationError(false, infoWindow, map.getCenter());
                }

                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function () {
                    updateMarkerAddress('Dragging...');
                });

                google.maps.event.addListener(marker, 'drag', function () {
                    updateMarkerStatus('Dragging...');
                    updateMarkerPosition(marker.getPosition());
                });

                google.maps.event.addListener(marker, 'dragend', function () {
                    console.log(marker.getPosition())
                    updateMarkerStatus('Drag ended');
                    geocodePosition(marker.getPosition());

                    map.panTo(marker.getPosition());
                });

                google.maps.event.addListener(map, 'click', function (e) {
                    updateMarkerPosition(e.latLng);
                    geocodePosition(marker.getPosition());
                    marker.setPosition(e.latLng);
                    map.panTo(marker.getPosition());
                });

                btnLocation.addEventListener('click', function () {
                    goToMyLocation();
                });
            }


            function goToMyLocation() {
                map.setCenter(myLocation);
                marker.setPosition(myLocation);
                geocodePosition(myLocation);
            }

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos,
                }, function (responses) {
                    if (responses && responses.length > 0) {

                        $.each(responses, function (i, address_component) {
                            if (address_component.types[0] == "locality") {
                                console.log("City: ", address_component.address_components[0].long_name);
                                console.log("City ID: ", address_component.place_id);
                                document.getElementById('city_name').value = address_component.address_components[0].long_name;
                                document.getElementById('city_place_id').value = address_component.place_id;
                            }
                        })

                        geoData = responses;
                        console.log('RESPONSE', responses)
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                console.log('markerStatus', str)
                //document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                document.getElementById('latitud').value = latLng.lat();
                document.getElementById('longitud').value = latLng.lng();
                /*document.getElementById('info').innerHTML = [
                    latLng.lat(),
                    latLng.lng()
                ].join(', ');*/
            }

            function updateMarkerAddress(str) {
                console.log('STR', str)
                //document.getElementById('address').innerHTML = str;
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation service failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxIR5t5wcMIWW-63l53acB1ygeVyvFakg&callback=initMap">
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/cleave.min.js"
                integrity="sha512-KaIyHb30iXTXfGyI9cyKFUIRSSuekJt6/vqXtyQKhQP6ozZEGY8nOtRS6fExqE4+RbYHus2yGyYg1BrqxzV6YA=="
                crossorigin="anonymous"></script>
        <script>
            $(document).ready(function () {
                var born_date = new Cleave('[name="born_date"]', {
                    date: true,
                    delimiter: '-',
                    datePattern: ['d', 'm', 'Y']
                    //onCreditCardTypeChanged: function (type) {
                    // update UI ...
                    //}
                });

                var cellphone = new Cleave('[name="cellphone"]', {
                    //phone: true,
                    prefix: '09',
                    numericOnly: true,
                    delimiters: [" ", " ", " "],
                    blocks: [4, 3, 3],
                });

                var doc_number = new Cleave('[name="doc_number"]', {
                    //numericOnly: true,
                    numeral: true,
                    delimiter: ' ',
                    //blocks: [0, 3, 3],
                });
            })
        </script>
@endsection
