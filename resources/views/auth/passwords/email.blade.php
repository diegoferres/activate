@extends('web.layouts.app')
@section('title')
    Actívate - Iniciar sesión
@endsection
@section('style')
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">

    <style>
        .slider_register {
            top: 0rem;
            position: relative;
        }

        .text-white {
            font-family: GalanoGrotesqueAlt;
        }

        input::placeholder {
            color: #004897 !important;

        }

        .btn.btn-icon, .navbar .navbar-nav > a.btn.btn-icon {
            height: 3.375rem !important;
            min-width: 3.375rem !important;
            width: 3.375rem !important;
            padding: 0;
            font-size: 1.5rem !important;
            overflow: hidden;
            position: relative;
            line-height: normal;
        }

        .btn-fa-facebook {
            background-color: #004897;
            color: #fff;
        }

        .btn-fa-google {
            background-color: #dd3939;
            color: #fff;
        }
    </style>
@endsection
@section('content')
    <div class="wrapper">
        <div class="page-header" style="max-height: 100% !important;
        ">

            <div class="content" style="margin-top:7%">
                <div class="container">
                    <div class="row" style="background-color:white;">
                        @include('auth.auth_slider')

                        <div class="col-sm-6 ml-auto mr-auto">
                            <div class="card card-login card-plain">
                                <form method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    <br>
                                    <h3 style="color:#004897">Restablecer contraseña</h3>
                                    <hr>
                                    <div class="card-body">
                                        <div class="input-group no-border input-lg">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons users_circle-08"></i>
                                                </span>
                                            </div>
                                            <input id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email" placeholder="Ingresar correo" value="{{ old('email') }}"
                                                   required autocomplete="email" autofocus>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        <button type="submit" style="font-size:1.2rem"
                                                class="btn btn-primary btn-round btn-lg btn-block">
                                            {{ __('Send Password Reset Link') }}
                                        </button>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('web.layouts.footer')
@endsection
