@extends('web.layouts.app')
@section('title')
    Actívate - Iniciar sesión
@endsection
@section('style')
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">

    <style>
        .slider_register {
            top: 0rem;
            position: relative;
        }

        .text-white {
            font-family: GalanoGrotesqueAlt;
        }

        input::placeholder {
            color: #004897 !important;

        }

        .btn.btn-icon, .navbar .navbar-nav > a.btn.btn-icon {
            height: 3.375rem !important;
            min-width: 3.375rem !important;
            width: 3.375rem !important;
            padding: 0;
            font-size: 1.5rem !important;
            overflow: hidden;
            position: relative;
            line-height: normal;
        }

        .btn-fa-facebook {
            background-color: #004897;
            color: #fff;
        }

        .btn-fa-google {
            background-color: #dd3939;
            color: #fff;
        }
    </style>
@endsection
@section('content')
    <div class="wrapper">
        <div class="page-header" style="max-height: 100% !important;
        ">
            <div class="content" style="margin-top:7%">
                <div class="container">
                    <div class="row" style="background-color:white;margin-top: 5.2rem">
                        <div class="col-sm-6 ml-auto mr-auto">
                            <div class="card card-login card-plain">
                                <br>
                                <h3 style="color:#004897">{{ __('Verify Your Email Address') }}</h3>
                                <hr>
                                <div class="card-body" style="color: black">
                                        {{ __('Before proceeding, please check your email for a verification link.') }}
                                        {{ __('If you did not receive the email') }},
                                </div>
                                <div class="card-footer text-center">
                                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                        @csrf
                                        <button style="font-size:1.2rem" type="submit" class="btn btn-primary btn-round btn-lg btn-block">{{ __('click here to request another') }}</button>.
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('web.layouts.footer')
@endsection


