@extends('web.layouts.app')
@section('title')
    Actívate - Iniciar sesión
@endsection
@section('style')
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">

    <style>
        .slider_register {
            top: 0rem;
            position: relative;
        }

        .text-white {
            font-family: GalanoGrotesqueAlt;
        }

        input{
    font-size: 18.2px !important;
}
        input::placeholder {
            color: #004897 !important;
         font-size: 18.2px;
        }
        .btn.btn-icon, .navbar .navbar-nav > a.btn.btn-icon {
            height: 3.375rem !important;
            min-width: 3.375rem !important;
            width: 3.375rem !important;
            padding: 0;
            font-size: 1.5rem !important;
            overflow: hidden;
            position: relative;
            line-height: normal;
        }

        .btn-fa-facebook {
            background-color: #004897;
            color: #fff;
        }

        .btn-fa-google {
            background-color: #dd3939;
            color: #fff;
        }
    </style>
@endsection
@section('content')
    <div class="wrapper">
        <div class="page-header" style="max-height: 100% !important;
        ">

            <div class="content" style="margin-top:7%">
                <div class="container">
                    <div class="row" >


                        <div class="col-sm-6 ml-auto mr-auto" style="background-color:white;">
                            <div class="card card-login card-plain">
                                {!! Form::open(['route' => 'login', 'method' => 'post']) !!}
                                @csrf
                                <br>
                                <h3 style="color:#004897">Iniciar sesión</h3>
                                <hr>

                                <div class="text-center">
                                    <a class="btn btn-icon btn-round btn-fa-facebook"
                                       href="{{ route('social.login', ['provider' => 'facebook']) }}"><i
                                            class="fab fa-facebook-f"></i></a>
                                    <a class="btn btn-icon btn-round btn-fa-google"
                                       href="{{ route('social.login', ['provider' => 'google']) }}"><i
                                            class="fab fa-google"></i></a>
                                </div>
                                <div class="card-body">
                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons ui-1_email-85"></i>
                                                </span>
                                        </div>
                                        <input name="email" type="text"
                                               class="form-control @error('email') is-invalid @enderror"
                                               placeholder="Correo"
                                               value="{{ Session::get('socialData') ? Session::get('socialData')['email'] : old('email') }}">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                                </span>
                                        </div>
                                        <input name="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               placeholder="Contraseña"

                                    </div>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>

                                <input type="hidden" name="latitud" id="latitud">
                                <input type="hidden" name="longitud" id="longitud">

                                <div class="card-footer text-center">
                                    <button type="submit" style="font-size:1.2rem"
                                            class="btn btn-primary btn-round btn-lg btn-block">
                                        Iniciar sesión
                                    </button>
                                    <br>
                                    <div class="text-center">
                                        <h5 style="color: #4F86C4;">

                                            ¿Aún no tienes una cuenta?
                                            <a href="{{ route('register') }}" class="link" style="color:#004897">Registrate</a>
                                        </h5>
                                    </div>
                                    <div class="text-center">
                                        <h5>
                                            @if (Route::has('password.request'))
                                                <a href="{{ route('password.request') }}" class="link"
                                                   style="color:#4F86C4">Olvide mi contraseña</a>
                                            @endif
                                        </h5>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('web.layouts.footer')
    @include('web.layouts.scripts')

@endsection


