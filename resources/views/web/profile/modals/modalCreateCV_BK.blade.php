<div class="modal" id="crearCV">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3>Conectate con otros usuarios</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <!--div class="col-md-4">
<img src="/web/assets/img/createProfile.png" class="responsive" style="padding-top:47px"
alt="">
</div-->
                    <div class="col-md-12">

                        {{--{!! Form::open(['route' => 'web.profile.cv.store', 'method' => 'post', 'class' => 'login-box'])
                        !!}--}}
                        {{--<form role="form" action="{{ route('web.profile.cv.store') }}" method="POST"
                        class="login-box">--}}


                        <div class="tab-content" id="main_form">
                            {{--<livewire:web.profile.create-c-v-post :subcategories="$subcategories">--}}

                            {{--@livewire('web.profile.wizard-cv', ['subcategories' => $subcategories])--}}
                            <livewire:web.profile.wizard-cv :subcategories="$subcategories">
                            {{--<div class="tab-pane active" role="tabpanel" id="paso1">
                                <form wire:submit.prevent="submit">
                                <h3 class="">1. Cuéntanos, ¿A qué te dedicas?</h3>
                                <div class="form-group">
                                    <label for="about">Dinos sobre tí</label>
                                    <textarea name="about" id="about" maxlength="300" class="form-control" cols="30"
                                              rows="10"></textarea>
                                </div>
                                    @error('about')
                                    <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror

                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="submit" class="btn btn-round  next-step">Continuar
                                        </button>
                                    </li>
                                </ul>
                                </form>
                            </div>--}}
                            {{--<div class="tab-pane" role="tabpanel" id="paso2">
                                <h3>2. Agrega una subcategoría principal</h3>
                                <div class="form-group subcategoria">
                                    <label>Selecciona tu actividad principal</label>
                                {!! Form::select('subcategory_id_main', $subcategories , null , ['class' =>
                                'custom-select select-picker', 'placeholder' => 'Selecciona la subcategoría']) !!}

                                <!--aca muestra el resuldao-->
                                    <div id="subcategoria"></div>

                                    <!-- aca llamo a la funcion-->
                                    --}}{{--<input type="button" id="addProfession" class="btn btn-info btn-round"
                                           onClick="addProfesion()" value="+ Agregar otra profesión"/>--}}{{--
                                </div>

                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" class="btn btn-round prev-step">Atras</button>
                                    </li>
                                    <li>
                                        <button type="button" class="btn btn-round next-step">Siguiente
                                        </button>
                                    </li>
                                </ul>
                            </div>--}}
                            {{--<div class="tab-pane" role="tabpanel" id="paso3">
                                <h3>3. Lista de servicios y rango de precios</h3>
                                <div id="subcategories">
                                    <div id="subcategory_slot">
                                        <label>Lista de servicios y precios</label>
                                        <div id="services_id" class="form-inline">
                                            <div class="form-group  mx-sm-3 mb-2">
                                                <label for="inputServicio" class="sr-only">SubCategoría</label>
                                                {!! Form::select('subcategory_id[]', $subcategories , null , ['class' => 'custom-select
                                        select-picker', 'placeholder' => 'Selecciona la subcategoría']) !!}
                                            </div>
                                            <div class="form-group  mx-sm-3 mb-2">
                                                <label for="inputServicio" class="sr-only">Servicios</label>
                                                <input type="text" class="form-control" name="service[]"
                                                       id="inputServicio"
                                                       placeholder="Servicio">
                                            </div>
                                            <div class="form-group mx-sm-3 mb-2">
                                                <label for="inputPrecioDesde" class="sr-only">Precios desde</label>
                                                <input type="number" class="form-control" name="price_from[]"
                                                       id="inputPrecioDesde"
                                                       placeholder="Precios desde">
                                            </div>
                                            <div class="form-group mx-sm-3 mb-2">
                                                <label for="inputPrecioHasta" class="sr-only">Precios hasta</label>
                                                <input type="number" class="form-control" name="price_to[]"
                                                       id="inputPrecioHasta"
                                                       placeholder="Precios hasta">
                                            </div>
                                            <div class="mx-sm-3 mb-2">
                                                <a data-toggle="tooltip" onclick="addService(this); return false;"
                                                   href="javascript:void(0)" data-placement="right"
                                                   title="Agregar otro servicio" id="addService"><i
                                                        class="fas fa-plus"></i></a>
                                                &nbsp;
                                                <a data-toggle="tooltip" onclick="deleteService(this); return false;"
                                                   href="javascript:void(0)" data-placement="right"
                                                   title="Eliminar servicio" id="deleteService" style="display: none"><i
                                                        class="fas fa-minus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--aca muestra el resuldao-->
                                <div id="listSubcategoria"></div>

                                <!-- aca llamo a la funcion-->
                                <input type="button" id="addProfession" class="btn btn-info btn-round"
                                       onclick="addSubcategory()" value="+ Agregar otra profesión"/>
                                <input type="button" id="deleteProfession" class="btn btn-warning btn-round"
                                       onclick="deleteSubcategory()" style="display: none"
                                       value="- Eliminar profesión"/>


                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" class="btn btn-round prev-step">Atras</button>
                                    </li>
                                    <li>
                                        <button type="button" class="btn btn-round next-step">Siguiente
                                        </button>
                                    </li>
                                </ul>
                            </div>--}}
                            {{--<div class="tab-pane" role="tabpanel" id="paso4">
                                <h3>4.¿Has trabajando en una empresa anteriormente?</h3>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customSi" name="pregunta" onClick="formularCampos()"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="customSi">SI</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customNo" name="pregunta" onClick="formularCampos()"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="customNo">NO</label>
                                </div>
                                <div id=optionWorking></div>

                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" class="btn btn-round prev-step">Atras</button>
                                    </li>
                                    <li>
                                        <button type="button" class="btn btn-round next-step">Siguiente
                                        </button>
                                    </li>
                                </ul>

                            </div>
                            <div class="tab-pane" role="tabpanel" id="paso5">
                                <h3>5. ¿En qué horario te gustaria trabajar</h3>
                                <h4>Selecciona el día y horario</h4>
                                <div id="schedules">

                                    <div id="schedules_id" class="form-inline">
                                        <div class="form-group  mx-sm-3 mb-2">
                                            <label for="inputServicio" class="sr-only">Día</label>
                                            {!! Form::select('day[]', $days , null , ['class' => 'form-control', 'placeholder' => 'Elige el día']) !!}
                                        </div>
                                        <div class="form-group mx-sm-3 mb-2">
                                            <label for="inputPrecioDesde" class="sr-only">Desde</label>
                                            <input type="time" class="form-control" name="hour_from[]"
                                                   placeholder="Desde">
                                        </div>
                                        <div class="form-group mx-sm-3 mb-2">
                                            <label for="inputPrecioHasta" class="sr-only">Hasta</label>
                                            <input type="time" class="form-control" name="hour_to[]"
                                                   placeholder="Hasta">
                                        </div>
                                        <div class="mx-sm-3 mb-2">
                                            <a data-toggle="tooltip" onclick="addSchedule(this); return false;"
                                               href="javascript:void(0)" data-placement="right"
                                               title="Agregar horario" id="addSchedule"><i
                                                    class="fas fa-plus"></i></a>
                                            &nbsp;
                                            <a data-toggle="tooltip" onclick="deleteSchedule(this); return false;"
                                               href="javascript:void(0)" data-placement="right"
                                               title="Eliminar horario" id="deleteSchedule" style="display: none"><i
                                                    class="fas fa-minus"></i></a>
                                        </div>
                                    </div>
                                </div>

                                --}}{{--<div class="form-check-inline">
                                    <label class="form-check-label" data-toggle="tooltip" data-placement="right"
                                           title="06:00 a 12:00">
                                        <input type="checkbox" class="form-check-input" value="">Mañana
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label" data-toggle="tooltip" data-placement="right"
                                           title="12:00 a 19:00">
                                        <input type="checkbox" class="form-check-input" value="">Tarde
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label" data-toggle="tooltip" data-placement="right"
                                           title="19:00 a 22:00">
                                        <input type="checkbox" class="form-check-input" value="">Noche
                                    </label>
                                </div>

                                <h4>Rango de horas</h4><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="four" name="radiohoras" onClick="formularCampos()"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="four">Más de 4 horas</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="seven" name="radiohoras" onClick="formularCampos()"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="seven">Más de 7 horas</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="teen" name="radiohoras" onClick="formularCampos()"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="teen">Más de 10 horas</label>
                                </div>--}}{{--


                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" class="btn btn-round prev-step">Atras</button>
                                    </li>
                                    <li>
                                        <button type="button" class="btn btn-round next-step">Siguiente
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="paso6">
                                <h3>6. Formación Académica</h3>
                                <h4>Estudios secundarios</h4>
                                <div>
                                    <div id="colegio" class="form-inline">
                                        <div class="form-group  mx-sm-1 mb-2">
                                            <label for="inputcolegio" class="sr-only">Colegio</label>
                                            <input type="text" class="form-control" name="institution[]"
                                                   placeholder="Colegio">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputAñoInicial" class="sr-only">Año Inicial</label>
                                            <input type="number" min="1940" max="{{ date('Y') }}" class="form-control" name="from_year[]"
                                                   id="inputAñoInicial"
                                                   placeholder="Año inicial">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputAñoFinal" class="sr-only">Año final</label>
                                            <input type="number" min="1940" max="{{ date('Y') }}" class="form-control" name="to_year[]"
                                                   id="inputAñoFinal"
                                                   placeholder="Año final">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputTituloColegio" class="sr-only">Título</label>
                                            <input type="text" name="title[]" class="form-control"
                                                   placeholder="Título obtenido">
                                        </div>
                                        <div class="mx-sm-1 mb-2">
                                            <a data-toggle="tooltip" onclick="addStudiesA(this); return false;"
                                               href="javascript:void(0)" data-placement="right"
                                               title="Agregar horario" id="addStudiesA"><i
                                                    class="fas fa-plus"></i></a>
                                            &nbsp;
                                            <a data-toggle="tooltip" onclick="deleteStudiesA(this); return false;"
                                               href="javascript:void(0)" data-placement="right"
                                               title="Eliminar horario" id="deleteStudiesA" style="display: none"><i
                                                    class="fas fa-minus"></i></a>

                                        </div>
                                    </div>
                                </div>
                                <h4>Estudios Universitarios</h4>
                                <div>
                                    <div id="colegio" class="form-inline">
                                        <div class="form-group  mx-sm-1 mb-2">
                                            <label for="inputcolegio" class="sr-only">Universidad</label>
                                            <input type="text" class="form-control" name="institution[]"
                                                   placeholder="Universidad">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputAñoInicial" class="sr-only">Año Inicial</label>
                                            <input type="number" min="1940" max="{{ date('Y') }}" class="form-control" name="from_year[]"
                                                   id="inputAñoInicial"
                                                   placeholder="Año inicial">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputAñoFinal" class="sr-only">Año final</label>
                                            <input type="number" min="1940" max="{{ date('Y') }}" class="form-control" name="to_year[]"
                                                   id="inputAñoFinal" name=""
                                                   placeholder="Año final">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputTituloColegio" class="sr-only">Título</label>
                                            <input type="text" name="title[]" class="form-control"
                                                   placeholder="Título obtenido">
                                        </div>
                                        <div class="mx-sm-1 mb-2">
                                            <a data-toggle="tooltip" onclick="addStudiesA(this); return false;"
                                               href="javascript:void(0)" data-placement="right"
                                               title="Agregar horario" id="addStudiesA"><i
                                                    class="fas fa-plus"></i></a>
                                            &nbsp;
                                            <a data-toggle="tooltip" onclick="deleteStudiesA(this); return false;"
                                               href="javascript:void(0)" data-placement="right"
                                               title="Eliminar horario" id="deleteStudiesA" style="display: none"><i
                                                    class="fas fa-minus"></i></a>

                                        </div>
                                    </div>
                                </div>
                                <h4>Estudios Terciarios</h4>
                                <div>
                                    <div id="colegio" class="form-inline">
                                        <div class="form-group  mx-sm-1 mb-2">
                                            <label for="inputcolegio" class="sr-only">Institución</label>
                                            <input type="text" class="form-control" name="institution[]"
                                                   placeholder="Institución">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputAñoInicial" class="sr-only">Año Inicial</label>
                                            <input type="number" min="1940" max="{{ date('Y') }}" class="form-control" name="from_year[]"
                                                   id="inputAñoInicial"
                                                   placeholder="Año inicial">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputAñoFinal" class="sr-only">Año final</label>
                                            <input type="number" min="1940" max="{{ date('Y') }}" class="form-control" name="to_year[]"
                                                   id="inputAñoFinal"
                                                   placeholder="Año final">
                                        </div>
                                        <div class="form-group mx-sm-1 mb-2">
                                            <label for="inputTituloColegio" class="sr-only">Título</label>
                                            <input type="text" name="title[]" class="form-control"
                                                   placeholder="Título obtenido">
                                        </div>
                                        <div class="mx-sm-1 mb-2">
                                            <a data-toggle="tooltip" onclick="addStudiesA(this); return false;"
                                               href="javascript:void(0)" data-placement="right"
                                               title="Agregar horario" id="addStudiesA"><i
                                                    class="fas fa-plus"></i></a>
                                            &nbsp;
                                            <a data-toggle="tooltip" onclick="deleteStudiesA(this); return false;"
                                               href="javascript:void(0)" data-placement="right"
                                               title="Eliminar horario" id="deleteStudiesA" style="display: none"><i
                                                    class="fas fa-minus"></i></a>

                                        </div>
                                    </div>
                                </div>
                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" class="btn btn-round prev-step">Atras</button>
                                    </li>
                                    <li>
                                        <button type="button" class="btn btn-round next-step">Siguiente
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="paso7">

                                <h3>7. Selecciona tu ubicación</h3>
                                <input type="hidden" name="latitud" id="latitud">
                                <input type="hidden" name="longitud" id="longitud">
                                <input type="hidden" name="city_name" id="city_name">
                                <input type="hidden" name="city_place_id" id="city_place_id">
                                <div class="all-info-container">
                                    <div style="height: 200px" id="map"></div>
                                </div>
                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" class="btn btn-round prev-step">Volver
                                        </button>
                                    </li>
                                    <li>
                                        <button type="submit" class="btn btn-round next-step">Guardar
                                            Datos
                                        </button>
                                    </li>
                                </ul>

                            </div>--}}
                            <div class="clearfix"></div>
                        </div>
                        {{--{!! Form::close() !!}--}}


                    </div>
                    {{--<div class="wizard">
                        <div class="wizard-inner">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#paso1" data-toggle="tab" aria-controls="paso1" role="tab"
                                       aria-expanded="true"></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#paso2" data-toggle="tab" aria-controls="paso2" role="tab"
                                       aria-expanded="false"></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#paso3" data-toggle="tab" aria-controls="paso3" role="tab"></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#paso4" data-toggle="tab" aria-controls="paso4" role="tab"></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#paso5" data-toggle="tab" aria-controls="paso5" role="tab"></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#paso6" data-toggle="tab" aria-controls="paso6" role="tab"></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#paso7" data-toggle="tab" aria-controls="paso7" role="tab"></a>
                                </li>
                            </ul>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
