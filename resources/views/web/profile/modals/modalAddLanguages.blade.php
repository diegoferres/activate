<div class="modal" id="addLanguages">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-md-8 offset-2">
                        {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
                        <input type="hidden" name="data_from" value="cvl">
                        <div class="form-group">
                            <label for="email">Selecione el idioma</label>
                            <select name="language" class="custom-select">
                                <option selected>General</option>
                                <option value="ES,Español"><img
                                        src="/web/assets/img/flags/ES.png">Español
                                </option>
                                <option value="US,Inglés"><img
                                        src="/web/assets/img/flags/US.png">Ingles
                                </option>
                                <option value="BR,Portugués"><img
                                        src="/web/assets/img/flags/BR.png">Portugues
                                </option>
                                <option value="DE,Alemán"><img
                                        src="/web/assets/img/flags/DE.png">Aleman
                                </option>
                                <option value="CN,Chino"><img
                                        src="/web/assets/img/flags/CN.png">Chino
                                </option>
                                <option value="PY,Guarani"><img
                                        src="/web/assets/img/flags/PY.png">Guaraní
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Nivel de lenguaje</label>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input"
                                       id="read" name="read">
                                <label class="custom-control-label"
                                       for="read">Lee</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input"
                                       id="understand" name="understand">
                                <label class="custom-control-label" for="understand">Comprende</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input"
                                       id="speak" name="speak">
                                <label class="custom-control-label"
                                       for="speak">Habla</label>
                            </div>
                        </div>
                        <button type="submit"
                                class="btn btn-info float-right btn-round">Guardar datos
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

