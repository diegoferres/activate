<div class="modal" id="addService">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                   
                    <div class="col-md-8 offset-2">
                        {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
                        <input type="hidden" name="data_from" value="us">
                        <div class="form-group">
                            <label for="email">Selecciona la subcategoría</label>
                            {!! Form::select('subcategory_id', $subcategories , null , ['class' => 'custom-select select-picker', 'placeholder' => 'Selecciona la subcategoría']) !!}
                        </div>

                        <div class="form-group">
                            <label for="service">Servicio</label>
                            <input type="text" id="service" name="service"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="price">Precio desde</label>
                            <input type="text" id="price_from" name="price_from"
                                   class="form-control isnumber">
                        </div>
                        <div class="form-group">
                            <label for="price">Precio hasta</label>
                            <input type="text" id="price_to" name="price_to"
                                   class="form-control isnumber">
                        </div>
                        <button type="submit"
                                class="btn btn-info float-right btn-round">Guardar datos
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

