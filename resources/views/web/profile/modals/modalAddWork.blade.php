<div class="modal" id="addWork">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-md-8 offset-2">
                        {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
                        <input type="hidden" name="data_from" value="cvwe">
                        {{--<div class="form-group">
                            <label for="position">Cargo</label>
                            <input type="text" id="position" name="position"
                                   class="form-control">
                        </div>--}}
                        <div class="form-group">
                            <label for="business">Empresa</label>
                            <input type="text" id="business" name="business"
                                   class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="antiquity">Antigüedad (meses)</label>
                            <input type="number" id="antiquity" name="antiquity"
                                   class="form-control" required>
                        </div>
                        <label>¿Sigues trabajando en la empresa?</label>
                        <br>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input wire:model="current_work" type="radio"
                                       class="form-check-input @error('current_work')is-invalid @enderror"
                                       name="current_work" value="true" required>
                                Si
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input wire:model="current_work" type="radio"
                                       class="form-check-input @error('current_work')is-invalid @enderror"
                                       name="current_work" value="false" required>
                                No
                            </label>
                        </div>
                        <br>
                        <button type="submit"
                                class="btn btn-info float-right btn-round">Guardar datos
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

