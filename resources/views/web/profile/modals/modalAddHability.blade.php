<div class="modal" id="addHability">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-md-8 offset-2">
                        {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
                        <input type="hidden" name="data_from" value="cvs">
                        <div class="form-group">
                            <label for="email">Habilidad/ Conocimiento</label>
                            <input type="text" name="skill" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Nivel</label><br>
                            <div
                                class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input"
                                       id="basico" name="percent" value="25">
                                <label class="custom-control-label"
                                       for="basico">Básico</label>
                            </div>
                            <div
                                class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input"
                                       id="intermedio" name="percent" value="50">
                                <label class="custom-control-label" for="intermedio">Intermedio</label>
                            </div>
                            <div
                                class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input"
                                       id="avanzado" name="percent" value="75">
                                <label class="custom-control-label"
                                       for="avanzado">Avanzado</label>
                            </div>
                            <div
                                class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input"
                                       id="experto" name="percent" value="100">
                                <label class="custom-control-label"
                                       for="experto">Experto</label>
                            </div>
                        </div>
                        <button type="submit"
                                class="btn btn-info float-right btn-round">Guardar datos
                        </button>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

