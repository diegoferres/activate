<div class="modal" id="addStudies">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                   
                    <div class="col-md-8 offset-2">
                        {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
                        <input type="hidden" name="data_from" value="cvai">
                        <div class="form-group">
                            <label for="email">Título</label>
                            <input type="text" name="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Institución</label>
                            <input type="text" name="institution" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Desde año</label>
                                    <input type="number" min="1940" max="{{ date('Y') }}" class="form-control" name="from_year"
                                           id="inputAñoFinal"
                                           placeholder="Año final">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Hasta año</label>
                                    <input type="number" min="1940" max="{{ date('Y') }}" class="form-control" name="to_year"
                                           id="inputAñoFinal"
                                           placeholder="Año final">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Documento</label>
                            <input type="file" name="institution" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-info float-right btn-round">
                            Guardar datos
                        </button>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
