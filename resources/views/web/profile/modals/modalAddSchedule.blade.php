<div class="modal" id="newSchedule">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
                <input type="hidden" name="data_from" value="cvsch">
                <div id="schedules_id" class="form-inline">
                    <div class="form-group  mx-sm-3 mb-2">
                        <label for="inputServicio" class="sr-only">Día</label>
                        {!! Form::select('day', $days , null , ['class' => 'form-control', 'placeholder' => 'Elige el día']) !!}
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPrecioDesde" class="sr-only">Desde</label>
                        <input type="time" class="form-control" name="hour_from"
                               placeholder="Desde">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPrecioHasta" class="sr-only">Hasta</label>
                        <input type="time" class="form-control" name="hour_to"
                               placeholder="Hasta">
                    </div>
                </div>
                <button class="btn btn-info btn-lg btn-round btn-block">
                    Guardar Datos
                </button>
                {!! Form::close() !!}
            </div>


        </div>
    </div>
</div>

