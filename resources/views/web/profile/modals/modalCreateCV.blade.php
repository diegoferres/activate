<div class="modal" id="crearCV">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-12">
                        <div class="tab-content" id="main_form">
                            <livewire:web.profile.wizard-cv :subcategories="$subcategories">
                                <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
