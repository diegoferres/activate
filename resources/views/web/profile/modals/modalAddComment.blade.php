<div class="modal" id="comentariosModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <video autoplay width="100%"
                               style="-webkit-mask-box-image: url('https://freepik.cdnpk.net/img/video/karma/mask.svg'); mask-image: url('');">
                            <source src="https://freepik.cdnpk.net/img/video/karma/video.mp4"
                                    type="video/mp4">
                        </video>
                    </div>
                    <div class="col-sm-6">
                        <h2>Recuerda que cada comentario nos ayuda a mejorar.</h2>
                        {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
                        <input type="hidden" name="data_from" value="ur">
                        <div class="form-group">
                            <label for="email">Selecciona el servicio</label>
                            {!! Form::select('service_id', $user->services->pluck('service', 'id') , null , ['class' => 'custom-select', 'placeholder' => 'Elige un servicio', 'required' => true]) !!}
                        </div>
                        <div class="form-group">
                            <label for="pwd">Escribe brevemente el comentario</label>
                            {!! Form::textarea('review', '', ['class' => 'form-control', 'row' => 5, 'placeholder' => 'Comentario', 'required' => true]) !!}
                        </div>
                        <button type="submit" class="btn btn-info float-right btn-round">
                            Comentar
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

