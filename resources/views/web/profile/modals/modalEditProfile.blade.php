<div class="modal" id="editPerfil">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
            <input type="hidden" name="data_from" value="cvi">
            <!-- Modal body -->
            <div class="modal-body">
                <!--FORMULARIO-->
                <div class="form-group">
                    <label for="usr">Nombres/ Apellidos</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label for="usr">SubCategoría principal</label>
                    {!! Form::select('subcategory_id_main', $subcategories , null , ['class' =>'custom-select', 'placeholder' => 'Selecciona la subcategoría']) !!}
                </div>
                <div class="form-group">
                    <label for="comment">Breve reseña</label>
                    <textarea class="form-control" rows="5" id="comment" name="about"></textarea>
                </div>

                <button class="btn btn-info btn-lg btn-round btn-block">
                    Guardar Datos
                </button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

