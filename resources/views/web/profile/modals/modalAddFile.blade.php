<div class="modal" id="addFile">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-8 offset-2">
                        {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post', 'files' => true]) !!}
                        <input type="hidden" name="data_from" value="cvf">
                        <div class="form-group">
                            <label for="email">Selecciona el servicio</label>
                            {!! Form::select('service_id', $user->services->pluck('service', 'id') , null , [
                                'class' => 'custom-select',
                                'placeholder' => 'Elige un servicio']) !!}
                        </div>

                        <input type="file" multiple class="form-control" name="user_file" accept="image/x-png,image/jpg,image/jpeg">
                        <br>
                        <button type="submit" class="btn btn-info float-right btn-round">
                            Guardar datos
                        </button>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

