@extends('web.layouts.app')
@section('title')
    Actívate - Perfil
@endsection
@section('style')
    <link rel="stylesheet" href="/web/assets/css/starrr.css">
    <link rel="stylesheet" href="/web/assets/css/progress.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.css" rel="stylesheet">
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">

    <style>
        .btn-info {
            font-size: 1.2rem !important;
        }

        .circle canvas {
            vertical-align: top;
            width: 200px !important;
            height: 200px !important;
        }

        .custom-input-file {
            background-color: #004897;
            color: #fff;
            cursor: pointer;
            font-size: 1.2rem;
            border-radius: 35px;
            margin: 0 auto 0;
            min-height: 15px;
            overflow: hidden;
            padding: 10px;
            position: relative;
            text-align: center;
            width: 400px;
        }

        .custom-input-file .input-file {
            border: 10000px solid transparent;
            cursor: pointer;
            font-size: 10000px;
            margin: 0;
            opacity: 0;
            outline: 0 none;
            padding: 0;
            position: absolute;
            right: -1000px;
            top: -1000px;
        }

        .parent_camera {
            position: absolute;
            left: 12rem;
            top: 10rem;
            COLOR: LIGHTGREY;
        }

        .child {

            background-size: cover;
            background-repeat: no-repeat;
            -webkit-transition: all .5s;
            -moz-transition: all .5s;
            -o-transition: all .5s;
            transition: all .5s;
        }

        select {
            margin: 4px !important;
        }

        .parent:hover .child, .parent:focus .child {
            -ms-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -webkit-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
        }

        .parent:hover .child:before, .parent:focus .child:before {
            display: block;
        }

        .parent:hover a, .parent:focus a {
            display: block;
        }

        .child:before {
            content: "";
            display: none;
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background-color: rgba(66, 75, 83, 0.644);
        }

        /* Media Queries */
        @media screen and (max-width: 960px) {
            .parent {
                width: 100%;
                margin: 20px 0px
            }

            .wrapper {
                padding: 20px 20px;
            }
        }

        .profile-page .photo-container {
            width: 200px;
            height: 200px;
            border-radius: 50%;
            overflow: hidden;

            box-shadow: 0px 10px 25px 0px rgba(0, 0, 0, 0.3);
        }

        .page-header .container {
            height: 100%;
            z-index: 1;
            text-align: initial;
            position: relative;
        }

        .subcategorias {
            font-size: 1rem;
            font-weight: 800;
        }

        body {
            background-color: #eeeeee;
        }

        .file-upload {
            background-color: #ffffff;

            margin: 0 auto;
            padding: 20px;
        }

        input, textarea {
            font-size: 18.2px !important;
        }

        select {
            font-size: 18.2px !important;
            color: #004897 !important;

        }

        input::placeholder {
            color: #004897 !important;
            font-size: 18.2px;
        }

        label {
            font-size: 18.2px !important;
        }


        .file-upload-content {
            display: none;
            text-align: center;
        }

        .btn {
            font-size: 1.2rem !important;
        }

        .file-upload-input {
            position: absolute;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            outline: none;
            opacity: 0;
            cursor: pointer;
        }

        .image-upload-wrap {
            margin-top: 20px;
            border: 2px dashed #004897;
            position: relative;
            border-radius: 15px;
        }


        .image-title-wrap {
            padding: 0 15px 15px 15px;
            color: #222;
        }

        .drag-text {
            text-align: center;
        }

        .drag-text p {
            font-weight: 100;
            text-transform: uppercase;
            color: #004897;

        }

        .file-upload-image {
            max-height: 200px;
            max-width: 200px;
            margin: auto;
            padding: 20px;
        }

        .remove-image {
            width: 200px;
            margin: 0;
            color: #fff;
            background: #cd4535;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #b02818;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .btn-link:hover, .btn-link:active {
            color: #004897 !important;
        }

        .remove-image:hover {
            background: #c13b2a;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .remove-image:active {
            border: 0;
            transition: all .2s ease;
        }

        .principal {
            font-size: 1.5rem;
            font-weight: 800;
        }


        .port-image {
            width: 100%;
        }

        .gallery_product {
            margin-bottom: 30px;
        }

    </style>
    <style>
        .cropit-preview {
            background-color: #f8f8f8;
            background-size: cover;
            border: 5px solid #ccc;
            border-radius: 3px;
            margin-top: 7px;
            width: 250px;
            height: 250px;
        }

        .cropit-preview-image-container {
            cursor: move;
        }

        .cropit-preview-background {
            opacity: .2;
            cursor: auto;
        }

        .image-size-label {
            margin-top: 10px;
        }

        input, .export {
            /* Use relative position to prevent from being covered by image background */
            position: relative;
            z-index: 10;
            display: block;
        }

        button {
            margin-top: 10px;
        }

        .btn-delete-img {
            background-color: #c13b2a;
            color: #fff;
            font-size: 1.5rem;
            padding: 5px 14px;
            width: 50px;
            height: 50px;
            position: relative;
            top: -330px;
            left: 280px;
        }
    </style>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css"
          rel="stylesheet"/>
    @livewireStyles
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-user"></i> Perfil</a>
    @if (Auth::id() == $user->id)
        <a class="dropdown-item" href="#crearCV" data-toggle="modal" data-target="#crearCV"><i
                class="far fa-address-card"></i> Crear CV</a>
    @endif
    <a class="dropdown-item" href="{{ route('chat') }}"><i class="far fa-comments"></i> Chat</a>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item"><i class="fas fa-undo-alt"></i> Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
@section('body_open')
    profile-page sidebar-collapse
@endsection
@section('content')
    <div class="wrapper">
        <!-- MODAL MAPA USUARIO -->
        <div class="modal" id="mapaUsuario">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div style="width: 100%; height: 450px;box-shadow: 2px 2px 22px -6px;" id="mapUser"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL CAMBIAR FOTO PERFIL-->
        <div class="modal fade" id="imagenEdit" tabindex="-1" role="dialog" aria-labelledby="imagenEditLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <img src="/web/gif/imageEdit.gif" alt="">
                            </div>
                            <div class="col-sm-8">

                                {{-- CROPIT --}}
                                {{--<div class="image-editor">
                                    <input type="file" class="cropit-image-input">
                                    <div class="cropit-preview"></div>
                                    <div class="image-size-label">
                                        Resize image
                                    </div>
                                    <input type="range" class="cropit-image-zoom-input">
                                    <button class="rotate-ccw">Rotate counterclockwise</button>
                                    <button class="rotate-cw">Rotate clockwise</button>

                                    <button class="export">Export</button>
                                </div>--}}
                                {{-- END CROPIT --}}


                                {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post', 'files' => true]) !!}
                                <input type="hidden" name="data_from" value="cva">
                                <div class="file-upload">
                                    <button class="btn btn-round btn-primary btn-lg btn-block" type="button"
                                            onclick="$('.file-upload-input').trigger( 'click' )">Insertar imagen
                                    </button>

                                    <div class="image-upload-wrap">
                                        <input class="file-upload-input" type='file' onchange="readURL(this);"
                                               accept="image/*" name="user_avatar"/>
                                        <div class="drag-text">
                                            <br>
                                            <h1><i class="fas fa-file-import"></i></h1>
                                            <p>Arrastra y suelta aqui la imagen ó<br>Selecciona una haciendo clic aquí
                                            </p>
                                        </div>
                                    </div>
                                    <div class="file-upload-content">
                                        <img class="file-upload-image" src="#" alt="your image"/>
                                        <div class="image-title-wrap">
                                            <button type="button" onclick="removeUpload()" class="remove-image">Remover
                                                <span class="image-title">Imagen cargada</span></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">

                                    <button type="submit" class="btn btn-info btn-lg btn-round">Guardar cambios</button>

                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="page-header clear-filter page-header-small" filter-color="orange">
            <div class="page-header-image" data-parallax="true"
                 style="background-image:url('/web/assets/img/bg5.png');">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="photo-container text-center parent">

                            <div
                                style="width:200px; height:200px; background-position: center; background-size: contain; background-repeat: no-repeat; background-image: url({{!empty($user->avatar) ? asset('storage/users-avatar/'.$user->avatar) : asset('storage/users-avatar/avatar.png')}});"></div>

                            <h2 class="parent_camera" data-toggle="tooltip" data-placement="right"
                                title="Cambiar foto de perfil"><i class="fas fa-camera" data-toggle="modal"
                                                                  data-target="#imagenEdit"></i></h2>
                        </div>
                        <br>
                        <div class="text-center">
                            <div class='starrr' id='star1'></div>
                            <h5><a href="" data-toggle="modal" data-target="#mapaUsuario"
                                   style="color: white !important"><i class="fas fa-map-marker-alt"></i>
                                    {{ isset($user->information->city_name) ? $user->information->city_name : 'Cargar ubicación' }}
                                </a></h5>
                                <h5>
                                    <i class="far fa-eye"></i> 1000 Visualizaciones
                                </h5>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <br>

                        {{-- <span class="badge badge-light">{{ isset($user->cv_information) ? $user->cv_information->subcategory->subcategory : 'Sin SubCategoría'}}</span>--}}
                        <span
                            class="principal">{{ isset($user->cv_information->subcategory->subcategory) ? $user->cv_information->subcategory->subcategory : 'Sin dato' }}</span>
                        <br>
                        @isset($user->cv_information->subcategory_id)
                            @foreach($user->services()->whereNotIn('subcategory_id', [$user->cv_information->subcategory_id])->get()->groupBy('subcategory_id') as $service)
                                <span class="subcategorias">{{ $service[0]->subcategory->subcategory }}</span>
                            @endforeach
                        @else
                            <span class="subcategorias">Sin subcategorías</span>
                        @endisset
                        {{--@forelse($user->services()->whereNotIn('subcategory_id', [$user->cv_information->subcategory_id])->get()->groupBy('subcategory_id') as $service)

                        @endforeach--}}
                        <h2>{{ $user->name }}</h2>
                        <a target="_blank" href="{{ route('chat') }}" class="btn btn-primary btn-round btn-lg"> <i
                                class="far fa-comment-dots"></i>
                            Contactar</a>
                        <a target="_blank"
                           href="https://wa.me/595{{ isset($user->information) ? $user->information->cellphone : '#' }}"
                           class="btn  btn-round  btn-icon btn-lg" style="background: #009688;"> <i
                                class="fab fa-whatsapp"></i>
                        </a>

                    </div>
                </div>

                <br>

                <div>
          <span class='your-choice-was' style='display: none;'>
            Puntuación <span class='choice'></span>.
          </span>
                </div>
            </div>
        </div>
        <!--IMPORTANTE NO BORRAR | Seccion para usuario normal-->


    {{--<div class="section">
        <div class="text-center">
            <img src="/web/gif/nofound.gif" alt="">
            <h2>Ofrece tus servicios</h2>
        <!--Este boton tiene que redireccionar a planes target #navPlanes-->
            <a href="" class="btn-round btn-info btn btnl-lg" style="font-size: 1.2rem; color: #fff ;">Comenzar</a>

        </div>
    </div>--}}


    <!--IMPORTANTE NO BORRAR |  Fin seccion-->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto">
                        <h2 style="font-weight: bold;" class="title text-center">Curriculum</h2>
                        <div class="nav-align-center">
                            <ul class="nav nav-pills nav-pills-primary nav-pills-just-icons" role="tablist">
                                <li class="nav-item">
                                    <a href="#item0" class="nav-link active" data-toggle="tab" role="tablist"
                                       rel="tooltip" title="Sobre mí">
                                        <i class="fas fa-user"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#item1" role="tablist"
                                       rel="tooltip" title="Experiencias">
                                        <i class="fas fa-university"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#item2" role="tablist" rel="tooltip"
                                       title="Habilidades" onclick="animateElements()">
                                        <i class="fas fa-award"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Tab panes -->
                <div class="tab-content gallery">
                    <div class="tab-pane active" id="item0" role="tabpanel">
                        <div class="large-container">
                            <div class="sec-title">
                                <span class="title">Perfil</span>
                                @if (Auth::id() == $user->id)
                                    <span
                                        class="float-right btn btn-info btn-round" data-toggle="modal"
                                        data-target="#editPerfil"> <i class="far fa-edit"></i> Editar Perfil</span>
                                @endif
                                <h2>¿Quién soy?</h2>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-6">

                                <h5 class="description">

                                    {{ isset($user->cv_information) ? $user->cv_information->about : null }}
                                </h5>
                            </div>
                        </div>
                        <hr>
                        <div class="large-container">
                            <div class="sec-title">
                                <span class="title">Horarios</span>
                                @if (Auth::id() == $user->id)
                                    <span
                                        class="float-right btn btn-info btn-round" data-toggle="modal"
                                        data-target="#newSchedule"> <i class="fas fa-plus"></i> Agregar Horarios</span>
                                @endif
                                <h2>Atención al clientes</h2>
                            </div>


                            <br>
                            <table class="table table-borderless" style="font-size: 1.3em!important">
                                <thead>
                                <tr>
                                    <th>Días</th>

                                    <th>Horario</th>

                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($user->days as $day)
                                    <tr>
                                        <td>{{ $day->day->day }}</td>
                                        <td>
                                            {{ date('H:s', strtotime($day->hour_from)) }} a
                                            {{ date('H:s', strtotime($day->hour_to)) }}
                                        </td>
                                        <td class="text-center">
                                            @if (Auth::id() == $user->id)
                                                <a href="{{ route('web.profile.cv.deleteitem', ['ud', $day->id]) }}">
                                                    <i class="fas fa-times"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        <hr>
                        <div class="large-container">
                            <div class="sec-title">
                                <span class="title">Servicios & tarifas</span>
                                @if (Auth::id() == $user->id)
                                    <span
                                        class="float-right btn btn-info btn-round" data-toggle="modal"
                                        data-target="#addService"> <i class="fas fa-plus"></i> Agregar Servicios</span>
                                @endif
                                <h2>¿Qué hago?</h2>
                            </div>


                            <br>
                            <table class="table table-borderless" style="font-size: 1.3em!important">
                                <thead>
                                <tr>
                                    <th>Categoria</th>
                                    <th>Servicios</th>
                                    <th>Desde</th>
                                    <th>Hasta</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->services as $service)
                                    <tr>
                                        <td>{{ $service->subcategory->subcategory  }}</td>
                                        <td>{{ $service->service  }}</td>
                                        <td>{{ number_format($service->price_from,0,',','.')  }}</td>
                                        <td>{{ number_format($service->price_to,0,',','.')  }}</td>
                                        <td class="text-center">
                                            @if (Auth::id() == $user->id)
                                                <a href="{{ route('web.profile.cv.deleteitem', ['us', $service->id]) }}">
                                                    <i class="fas fa-times"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        <hr>
                        <div class="large-container">
                            <div class="sec-title">
                                <span class="title">Comentarios</span>
                                @if (Auth::id() != $user->id)
                                    <span class="float-right btn btn-info btn-round" data-toggle="modal"
                                          data-target="#comentariosModal"> <i
                                            class="far fa-comment"></i> Dejar un comentario</span>
                                @endif

                                <h2>Lo que dicen de mi</h2>
                            </div>
                            @foreach($user->reviews as $review)
                                <div class="media p-3" style="border-radius: 15px; background-color: #eaedf0;">
                                    <img
                                        src="{{ !empty($review->user->avatar) ? asset('storage/users-avatar/'.$review->user->avatar) : asset('storage/users-avatar/avatar.png') }}"
                                        alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                                    <div class="media-body">
                                        @php(setlocale(LC_TIME,"es_ES.UTF-8"))

                                        <h4>{{ $review->user->name }} <small><i>Comentado
                                                    el {{ strftime('%d, %B', strtotime($review->created_at)) }}</i></small>
                                            @if (Auth::id() == $user->id)
                                                {!! Form::open(['route' => 'web.profile.cv.additem', 'method' => 'post']) !!}
                                                <input type="hidden" name="data_from" value="cvrr">
                                                <input type="hidden" name="comment" value="{{ $review->id }}">
                                            @if ($review->report == 1)
                                                    <input type="hidden" name="report" value="0">
                                                    <button type="submit"
                                                            class="btn btn-outline-primary btn-round float-right">
                                                        <i class="fas fa-ban"></i> Cancelar reporte
                                                    </button>
                                            @elseif ($review->report == 0)
                                                    <input type="hidden" name="report" value="1">
                                                    <button type="submit"
                                                            class="btn btn-outline-primary btn-round float-right">
                                                        <i class="fas fa-ban"></i> Reportar comentario
                                                    </button>
                                            @endif

                                                {{--<a href="" class="btn btn-outline-primary btn-round float-right"></a>--}}
                                                {!! Form::close() !!}

                                            @endif

                                        </h4>
                                        <p>{{ $review->review }}</p>

                                        {{--<div class="media p-3" style="border-radius: 15px; background-color: #004897; color: #fff;">
                                            <img src="{{ !empty($review->user->avatar) ? asset('storage/users-avatar/'.$review->user->avatar) : asset('storage/users-avatar/avatar.png') }}" alt="Jane Doe" class="mr-3 mt-3 rounded-circle" style="width:45px;">
                                            <div class="media-body">
                                                <h4>Jane Doe <small><i>Posted on February 20 2016</i></small></h4>
                                                <p>Lorem ipsum...</p>
                                            </div>
                                        </div>--}}
                                    </div>
                                </div>
                            @endforeach
                            {{--<div class="media p-3" style="border-radius: 15px; background-color: #eaedf0;">
                              <img src="/web/assets/img/ryan.jpg" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                              <div class="media-body">
                                <h4>John Doe <small><i>Posted on February 19, 2016</i></small></h4>
                                <p>Lorem ipsum...</p>
                                <div class="media p-3" style="border-radius: 15px; background-color: #004897; color: #fff;">
                                  <img src="/web/assets/img/ryan.jpg" alt="Jane Doe" class="mr-3 mt-3 rounded-circle" style="width:45px;">
                                  <div class="media-body">
                                    <h4>Jane Doe <small><i>Posted on February 20 2016</i></small></h4>
                                    <p>Lorem ipsum...</p>
                                  </div>
                                </div>
                              </div>
                            </div>--}}
                        </div>
                    </div>
                    <div class="tab-pane" id="item1" role="tabpanel">
                        <!--experiencia academica-->
                        <div class="large-container">
                            <br>
                            @if (Auth::id() == $user->id)
                                <span class="float-right btn-round btn-info" data-target="#addStudies"
                                      data-toggle="modal"><i class="fas fa-plus"></i> Agregar estudios</span>
                            @endif
                            <h2>1. Formación academica.</h2>


                            <br>
                            <br>
                            <div class="row collections">
                                <table class="table table-borderless" style="font-size: 1.3em!important">
                                    <thead>
                                    <tr>
                                        <th>Titulo</th>
                                        <th>Institución</th>
                                        <th>Desde</th>
                                        <th>Hasta</th>
                                        <th>Documento</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user->cv_academic_information as $acainfo)
                                        <tr>
                                            <td>{{ $acainfo->title }}</td>
                                            <td>{{ $acainfo->institution }}</td>
                                            <td>{{ $acainfo->from_year }}</td>
                                            <td>{{ $acainfo->to_year }}</td>
                                            <td align="center" style="font-size: 25px">
                                                @if ($acainfo->academic_file)

                                                    <a disabled target="_blank"
                                                       href="{{ asset('storage/user-files/'.$acainfo->academic_file) }}"><i
                                                            class="far fa-file-alt"></i></a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if (Auth::id() == $user->id)
                                                    <a href="{{ route('web.profile.cv.deleteitem', ['cvai', $acainfo->id]) }}">
                                                        <i class="fas fa-times"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <br>
                            <hr>
                            <br>
                            @if (Auth::id() == $user->id)
                                <span class="float-right btn-round btn-info" data-toggle="modal" data-target="#addWork"><i
                                        class="fas fa-plus"></i> Agregar experiencia laboral</span>
                            @endif
                            <h2>2. Experiencia Laboral.</h2>
                            <br>
                            <br>
                            <div class="row collections">
                                <table class="table table-borderless" style="font-size: 1.3em!important">
                                    <thead>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>Antigüedad</th>
                                        <th>Actual</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user->cv_work_experience as $workexp)
                                        <tr>
                                            <td>{{ $workexp->business }}</td>
                                            <td>{{ $workexp->antiquity }}</td>
                                            <td>{{ $workexp->current_work == 1 ? 'SI' : 'NO' }}</td>
                                            <td class="text-center">
                                                @if (Auth::id() == $user->id)

                                                    <a href="{{ route('web.profile.cv.deleteitem', ['cvwe', $workexp->id]) }}">
                                                        <i class="fas fa-times"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <br>
                            <hr>
                            <br>
                            @if (Auth::id() == $user->id)
                                <span class="float-right btn-round btn-info" data-toggle="modal" data-target="#addFile"><i
                                        class="fas fa-plus"></i> Agregar archivo</span>
                            @endif
                            <h2>3. Galería</h2>
                            <br>
                            <br>
                            <!-- MENU PARA GALERIA -->
                            <div class="text-center">

                                <button class="btn btn-link filter-button" data-filter="all">Todos</button>

                                @foreach ($userSubcategories as $subcategory)
                                    <button class="btn btn-link filter-button"
                                            data-filter="{{ str_replace(' ','-', $subcategory->subcategory->subcategory) }}">{{ $subcategory->subcategory->subcategory }}</button>
                                @endforeach

                                {{--<button class="btn btn-link filter-button" data-filter="sprinkle">Sprinkle Pipes
                                </button>
                                <button class="btn btn-link filter-button" data-filter="spray">Spray Nozzle</button>
                                <button class="btn btn-link filter-button" data-filter="irrigation">Irrigation Pipes
                                </button>--}}
                            </div>
                            <!-- CONTENEDOR DE GALERIA-->
                            <div class="row">
                                {{--<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                    <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                                    <!--BOTON PARA ELIMINAR FOTO-->
                                    <div class="btn-delete-img">
                                        <i class=" hvr-buzz-out fas fa-trash-alt"></i></div>
                                </div>
                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                    <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                                    <!--BOTON PARA ELIMINAR FOTO-->
                                    <div class="btn-delete-img">
                                        <i class=" hvr-buzz-out fas fa-trash-alt"></i></div>
                                </div>
                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                    <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                                    <!--BOTON PARA ELIMINAR FOTO-->
                                    <div class="btn-delete-img">
                                        <i class=" hvr-buzz-out fas fa-trash-alt"></i></div>
                                </div>
                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                    <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                                    <!--BOTON PARA ELIMINAR FOTO-->
                                    <div class="btn-delete-img">
                                        <i class=" hvr-buzz-out fas fa-trash-alt"></i></div>
                                </div>--}}
                                @foreach($user->files as $file)


                                    <div
                                        class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter">
                                        <img src="{{ asset('storage/'.$file->file_path) }}" class="img-responsive">
                                        <!--BOTON PARA ELIMINAR FOTO-->
                                        <a href="{{ route('web.profile.cv.deleteitem', ['cvf', $file->id]) }}">
                                            <div class="btn-delete-img">
                                                <i class=" hvr-buzz-out fas fa-trash-alt"></i>
                                            </div>
                                        </a>
                                    </div>

                                    {{--<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter {{ str_replace(' ','-', $file->subcategory->subcategory) }}">
                                        <img src="{{ asset('storage/'.$file->file_path) }}" class="img-responsive">
                                    </div>--}}
                                @endforeach

                            </div>

                            {{--<<<<<<< HEAD--}}
                            {{-- @if (Auth::id() == $user->id)
                                 <span class="float-right btn-round btn-info" data-toggle="modal" data-target="#addFile"><i
                                         class="fas fa-plus"></i> Agregar archivo</span>
                             @endif--}}
                            {{--=======

                            >>>>>>> design--}}
                        </div>
                    </div>

                    <div class="tab-pane" id="item2" role="tabpanel">
                        <div class="large-container">

                            <br>
                            @if (Auth::id() == $user->id)
                                <span class="float-right btn-round btn-info" data-toggle="modal"
                                      data-target="#addHability"><i class="fas fa-plus"></i> Agregar habilidades</span>
                            @endif
                            <h2>4. Habilidades & conocimientos</h2>
                            <br>

                            <div class="row collections">
                                <!--Progress bar-->

                                    @foreach($user->cv_skills as $skill)
                                        <div class="col-sm-3">
                                            <div class="progressbar">
                                                <div class="second circle" data-percent="{{ $skill->percent }}">
                                                    <strong></strong>
                                                    <span>{{ $skill->skill }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                            </div>

                            <br>
                            <hr>
                            <br>
                            @if (Auth::id() == $user->id)
                                <span class="float-right btn-round btn-info" data-toggle="modal"
                                      data-target="#addLanguages"><i class="fas fa-plus"></i> Agregar idiomas</span>
                            @endif
                            <h2>5. Idiomas.</h2>
                            <br>
                            <br>
                            <div class="row collections">
                                <table class="table table-borderless" style="font-size: 1.3em!important">
                                    <thead>
                                    <tr>
                                        <th>Idioma</th>
                                        <th>Lee</th>
                                        <th>Comprende</th>
                                        <th>Habla</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user->cv_languages as $lang)
                                        <tr>
                                            <td><img src="/web/assets/img/flags/{{ $lang->lang_code }}.png"
                                                     rel="tooltip" title="{{ $lang->language }}"
                                                     width="20%" class="img-responsive" alt=""></td>
                                            <td rel="tooltip"
                                                title="{{ $lang->read ? 'Si' : 'No' }}">{{ $lang->read ? 'Si' : 'No' }}</td>
                                            <td rel="tooltip"
                                                title="{{ $lang->understand ? 'Si' : 'No' }}">{{ $lang->understand ? 'Si' : 'No' }}</td>
                                            <td rel="tooltip"
                                                title="{{ $lang->speak ? 'Si' : 'No' }}">{{ $lang->speak ? 'Si' : 'No' }}</td>
                                            <td class="text-center">
                                                @if (Auth::id() == $user->id)
                                                    <a href="{{ route('web.profile.cv.deleteitem', ['cvl', $lang->id]) }}">
                                                        <i class="fas fa-times"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="section">
            <div class="container">

                <div class="sec-title">
                    <h2>Perfiles similares</h2>
                </div>
                <section class="customer-logos slider">

                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Angel Chiguala
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Mathias Medina
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Rocio del Pilar
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>


                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Fatima Irala
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Griselda Acosta
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Marcelo Sosa
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>

                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Ambar Nuñez
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Gabriela Leguizamon
                            <p class="text-center" style="color: gray;">Developer</p>
                        </h5>

                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Julio Caceres
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>


                </section>
                <div class="text-center">
                    <a href="search.html" class="btn btn-round btn-outline-primary">Ver más similiares</a>
                </div>
            </div>
        </div>
    </div>
    @include('web.layouts.footer')
    @if (Auth::id() == $user->id)
        @include('web.profile.modals.modalAddHability')
        @include('web.profile.modals.modalAddLanguages')
        @include('web.profile.modals.modalAddServices')
        @include('web.profile.modals.modalAddStudies')
        @include('web.profile.modals.modalAddWork')
        @include('web.profile.modals.modalCreateCV')
        @include('web.profile.modals.modalEditProfile')
        @include('web.profile.modals.modalAddFile')
    @endif
    @include('web.profile.modals.modalAddComment')
    @include('web.profile.modals.modalAddSchedule')
@endsection
@section('scripts')
    <!--   Core JS Files   -->
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="/web/assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="/web/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="/web/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    {{--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>--}}
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
    <script src="/web/assets/js/progress.js" type="text/javascript"></script>
    <script src="https://rawgit.com/kottenator/jquery-circle-progress/1.2.2/dist/circle-progress.js"></script>
    <script src="/web/assets/js/starrr.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/formvalidation/0.6.2-dev/js/formValidation.min.js"
            integrity="sha512-DlXWqMPKer3hZZMFub5hMTfj9aMQTNDrf0P21WESBefJSwvJguz97HB007VuOEecCApSMf5SY7A7LkQwfGyVfg=="
            crossorigin="anonymous"></script>

    {{--GALERIA--}}
    <script type="text/javascript">
        $(document).ready(function () {

            $(".filter-button").click(function () {
                var value = $(this).attr('data-filter');

                if (value == "all") {
                    //$('.filter').removeClass('hidden');
                    $('.filter').show('1000');
                } else {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                    $(".filter").not('.' + value).hide('3000');
                    $('.filter').filter('.' + value).show('3000');

                }
            });

            if ($(".filter-button").removeClass("active")) {
                $(this).removeClass("active");
            }
            $(this).addClass("active");

        });
    </script>

    {{-- MAPA WIZARD --}}
    <script type="text/javascript">
        $(document).ready(function () {
            @if(!$user->cv_information)

            $('#crearCV').show()

            @endif
        })

        function addProfesion() {
            //selecciono el elemento a clonar y first para que sea solo uno, appendTo
            //para añadir al final dentro del elemento

            $('select[name="subcategory_id"]').first().clone().appendTo('div#subcategoria');

        }

        function addSubcategoria() {
            $('select[name="subcategory_id2"]').first().clone().appendTo('div#listSubcategoria');
        }

        function addStudiesA(e) {
            var element = $(e).parent().parent().clone().appendTo($(e).parent().parent().parent());

            if ($(e).parent().parent().parent().children().length > 1) {
                element.last().children().last().children().last().show()
            } else {
                $(element).siblings().hide()
            }
        }

        function deleteStudiesA(e) {
            $(e).parent().parent().remove();
        }

        function addSchedule(e) {
            var element = $(e).parent().parent().clone().appendTo($(e).parent().parent().parent());

            if ($(e).parent().parent().parent().find('div#schedules_id').length > 1) {
                element.last().children().last().children().last().show()
            } else {
                $(element).siblings().hide()
            }
        }

        function deleteSchedule(e) {
            $(e).parent().parent().remove();
        }

        function addService(e) {
            var element = $(e).parent().parent().clone().appendTo($(e).parent().parent().parent());

            if ($(e).parent().parent().parent().find('div#services_id').length > 1) {
                element.last().children().last().children().last().show()
            } else {
                $(element).siblings().hide()
            }
        }

        function deleteService(e) {
            $(e).parent().parent().remove();
        }

        function addSubcategory() {
            $('div#subcategory_slot').first().clone().appendTo($('div#subcategories')).find('div#services_id').nextAll().remove()

            if ($('div#subcategories').children().length > 1) {
                $('input#deleteProfession').show()
            } else {
                $('input#deleteProfession').hide()
            }
        }

        function deleteSubcategory() {
            $('div#subcategories').children().last().remove();

            if ($('div#subcategories').children().length > 1) {
                $('input#deleteProfession').show()
            } else {
                $('input#deleteProfession').hide()
            }
        }

        //funcion del step 3
        function formularCampos() {
            document.getElementById('optionWorking').innerHTML = '';

            if (document.getElementById('customSi').checked) {

                var div = document.createElement('div');
                div.setAttribute('class', 'form-group');
                div.innerHTML =
                    '<div>' +
                    '<div">' +
                    '<label>¿En donde?</label>' +
                    '</div>' +
                    '<input type="text" class="form-control" placeholder="Institución o empresa" name="business">' +
                    '</div>' +

                    '<div>' +
                    '<div>' +
                    '<label>¿Cuánto tiempo? (en meses)</label>' +
                    '</div>' +
                    '<input type="number" class="form-control" placeholder="Antigüedad" name="antique">' +
                    '</div>' +

                    '<div>' +
                    '<label>¿Sigues trabajando en la empresa?</label><br>' +
                    '<div class="form-check-inline">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" name="current_work" value="true">Si' +
                    '</label>' +
                    '</div>' +

                    '<div class="form-check-inline">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" name="current_work" value="false">No' +
                    '</label>' +
                    '</div>' +
                    '</div>';
                document.getElementById('optionWorking').appendChild(div);
                document.getElementById('optionWorking').appendChild(div);
            } else if (document.getElementById('customNo').checked) {

                var div = document.createElement('div');
                div.setAttribute('class', 'form-group');
                div.innerHTML =
                    '<div>' +
                    '<label>¿Selecciona una de las siguientes opciones?</label><br>' +
                    '<div class="form-check-inline">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" name="work_status" value="Trabajo independientemente">Trabajo independientemente' +
                    '</label>' +
                    '</div><br>' +

                    '<div class="form-check-inline">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" name="work_status" value="Estoy desempleado">Estoy desempleado' +
                    '</label>' +
                    '</div><br>' +

                    '<div class="form-check-inline">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" name="work_status" value="Estoy buscando ganar un extra">Estoy buscando ganar un extra' +
                    '</label>' +
                    '</div>' +
                    '</div>';
                document.getElementById('optionWorking').appendChild(div);
                document.getElementById('optionWorking').appendChild(div);
            }
        }


        // Note: This example requires that you consent to location sharing when
        // prompted by your browser. If you see the error "The Geolocation service
        // failed.", it means you probably did not give permission for the browser to
        // locate you.
        var map, map2, infoWindow, geoData;
        var geocoder;

        function initMap() {
            geocoder = new google.maps.Geocoder();
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -25.2819, lng: -57.635    },
                zoom: 10
            });

            map2 = new google.maps.Map(document.getElementById('map2'), {
                center: {lat: -25.2819, lng: -57.635},
                zoom: 10
            });

            var pos = {
                lat: -25.2819,
                lng: -57.635
            };

            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                draggable: true,
                title: 'Tu ubicación'
            });

            var marker2 = new google.maps.Marker({
                position: pos,
                map: map2,
                draggable: true,
                title: 'Tu ubicación'
            });

            updateMarkerPosition(marker.getPosition());
            geocodePosition(pos);

            map.setCenter(pos);

            infoWindow = new google.maps.InfoWindow;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    marker.setPosition(pos);

                    /*var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        draggable: true,
                        title: 'Tu ubicación'
                    });*/
                    /*infoWindow.setPosition(pos);
                    infoWindow.setContent('Your position');
                    marker.addListener('click', function() {
                      infoWindow.open(map, marker);
                    });
                    infoWindow.open(map, marker);*/
                    map.setCenter(pos);


                    updateMarkerPosition(marker.getPosition());
                    geocodePosition(pos);


                }, function () {
                    //handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                //handleLocationError(false, infoWindow, map.getCenter());
            }

            // Add dragging event listeners.
            google.maps.event.addListener(marker, 'dragstart', function () {
                updateMarkerAddress('Dragging...');
            });

            google.maps.event.addListener(marker, 'drag', function () {
                updateMarkerStatus('Dragging...');
                updateMarkerPosition(marker.getPosition());
            });

            google.maps.event.addListener(marker, 'dragend', function () {
                updateMarkerStatus('Drag ended');
                geocodePosition(marker.getPosition());
                map.panTo(marker.getPosition());
            });

            google.maps.event.addListener(map, 'click', function (e) {
                updateMarkerPosition(e.latLng);
                geocodePosition(marker.getPosition());
                marker.setPosition(e.latLng);
                map.panTo(marker.getPosition());
            });
        }

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos,
            }, function (responses) {
                if (responses && responses.length > 0) {

                    $.each(responses, function (i, address_component) {
                        if (address_component.types[0] == "locality") {
                            console.log("City: ", address_component.address_components[0].long_name);
                            console.log("City ID: ", address_component.place_id);

                            city_name = document.getElementById('city_name');
                            city_name.value = address_component.address_components[0].long_name

                            city_place_id = document.getElementById('city_place_id');
                            city_place_id.value = address_component.place_id

                            var ev = new InputEvent("input");
                            city_name.dispatchEvent(ev);
                            var ev = new InputEvent("input");
                            city_place_id.dispatchEvent(ev);


                            //document.getElementById("city_name").dispatchEvent(new Event('city_name'));
                            //document.getElementById('city_place_id').value = address_component.place_id;
                        }
                    })

                    geoData = responses;
                    console.log('RESPONSE', responses)
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerStatus(str) {
            console.log('MARKER STATUS: ', str)
            //document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {
            console.log('A')
            latitud = document.getElementById('latitud');
            latitud.value = latLng.lat();
            var ev = new InputEvent("input");
            latitud.dispatchEvent(ev);


            longitud = document.getElementById('longitud');
            longitud.value = latLng.lng();
            document.getElementById('longitud').value = latLng.lng();
            var ev = new InputEvent("input");
            longitud.dispatchEvent(ev);

            /*document.getElementById('info').innerHTML = [
                latLng.lat(),
                latLng.lng()
            ].join(', ');*/
        }

        function updateMarkerAddress(str) {
            console.log('STR', str)
            //document.getElementById('address').innerHTML = str;
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxIR5t5wcMIWW-63l53acB1ygeVyvFakg&callback=initMap">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/cleave.min.js"
            integrity="sha512-KaIyHb30iXTXfGyI9cyKFUIRSSuekJt6/vqXtyQKhQP6ozZEGY8nOtRS6fExqE4+RbYHus2yGyYg1BrqxzV6YA=="
            crossorigin="anonymous"></script>
    {{-- CROPIT --}}
    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="/js/cropit/jquery.cropit.js"></script>--}}
    {{-- END CROPIT --}}

    <script>
        /* CROPIT */
        /*$(function() {
            $('.image-editor').cropit({
                exportZoom: 1.25,
                imageBackground: true,
                imageBackgroundBorderWidth: 20,
                imageState: {
                    src: 'http://lorempixel.com/500/400/',
                },
            });

            $('.rotate-cw').click(function() {
                $('.image-editor').cropit('rotateCW');
            });
            $('.rotate-ccw').click(function() {
                $('.image-editor').cropit('rotateCCW');
            });

            $('.export').click(function() {
                var imageData = $('.image-editor').cropit('export');
                window.open(imageData);
            });
        });*/
        /* END CROPIT */

        $(document).ready(function () {
            /*var doc_number = new Cleave('input[type="number"]', {
                //numericOnly: true,
                numeral: true,
                delimiter: ' ',
                //blocks: [0, 3, 3],
            });*/
            $('.isnumber').toArray().forEach(function (field) {
                console.log('FIELD', field)
                new Cleave(field, {
                    /*numericOnly: true,
                    delimiter: ' ',
                    blocks: [5,4]*/
                    //numericOnly: true,
                    numeral: true,
                    delimiter: ' ',
                    //blocks: [0, 3, 3],
                });
            });
        });
        $('#star1').starrr({
            change: function (e, value) {
                if (value) {

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: '{{ route('web.profile.cv.additem') }}',
                        data: {score: value, user_id: {{ Auth::id() }}, data_from: 'urat'},
                        success: function () {
                            iziToast.success({
                                title: 'Calificación ' + value,
                                message: 'Se ha guardado su calificación'
                            });
                        }
                    });

                    $('.your-choice-was').show();
                    $('.choice').text(value);
                } else {
                    $('.your-choice-was').hide();
                }
            }
        });

        var $s2input = $('#star2_input');
        $('#star2').starrr({
            max: 10,
            rating: $s2input.val(),
            change: function (e, value) {
                $s2input.val(value).trigger('input');
            }
        });

        $(function () {
            $('.selectpicker').selectpicker();
        });

        /* input de foto de perfil*/
        function readURL(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-upload-wrap').hide();

                    $('.file-upload-image').attr('src', e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }

        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });

    </script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fd6f353d242424b"></script>

    @livewireScripts
@endsection
