@extends('web.layouts.app')
@section('title')
Actívate - Inicio
@endsection
@section('navbaroptions')
<a class="dropdown-header">Cuenta</a>
<a class="dropdown-item" href="#"><i class="fas fa-binoculars"></i>Inicio</a>
<a class="dropdown-item" href="#"><i class="far fa-paper-plane"></i>¿Por que elegirnos?</a>
<a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Planes disponibles</a>
<a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Testimonios</a>
<a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Contactanos</a>
<div class="dropdown-divider"></div>

{{--<a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"> <i class="far fa-eye"></i> Ir a mi
perfil</a>--}}
@endsection
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.css" rel="stylesheet">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<link rel="stylesheet" href="/web/galanoFont/stylesheet.css">
<link rel="stylesheet" href="/web/assets/css/hover.css">


<style>
    .page-header .page-header-image {

        position: absolute;
        background-size: cover;
        background-position: center center;
        width: 100%;
        height: 100%;
        z-index: auto !important;

    }



    .search-slt {
        display: block;
        width: 100%;
        font-size: 0.875rem;
        line-height: 1.5;
        color: #55595c;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        height: calc(3rem + 2px) !important;
        border-radius: 0;
    }

    .wrn-btn {
        width: 100%;
        font-size: 16px;
        font-weight: 400;
        text-transform: capitalize;
        height: calc(3rem + 2px) !important;
        border-radius: 0;
        position: relative;
        top: -10px;
    }

    .btn-spotify {
        background: #1ed760 !important;
    }



    .carousel-inner2 {
    box-shadow: none !important;
    border-radius: 0 !important;
}
    .carousel-inner2,
    .carousel,
    .carousel-item,
    .fill {
        height: 100%;
        min-height: 400px;
        width: 100%;
        background-position: center center;
        background-size: cover;
    }

    .slide-wrapper {
        display: inline;

    }




    /*------------------------------ vertical bootstrap slider----------------------------*/

    .carousel-inner2>.carousel-item.carousel-item-next,
    .carousel-inner2>.carousel-item.active.carousel-item-right {
        transform: translate3d(0, 100%, 0);
        -webkit-transform: translate3d(0, 100%, 0);
        -ms-transform: translate3d(0, 100%, 0);
        -moz-transform: translate3d(0, 100%, 0);
        -o-transform: translate3d(0, 100%, 0);
        top: 0;

    }

    .carousel-inner2>.carousel-item.carousel-item-prev,
    .carousel-inner2>.carousel-item.active.carousel-item-left {
        transform: translate3d(0, -100%, 0);
        -webkit-transform: translate3d(0, -100%, 0);
        -moz-transform: translate3d(0, -100%, 0);
        -ms-transform: translate3d(0, -100%, 0);
        -o-transform: translate3d(0, -100%, 0);
        top: 0;

    }

    .carousel-inner2>.carousel-item.next.carousel-item-left,
    .carousel-inner2>.carousel-item.carousel-item-prev.carousel-item-right,
    .carousel-inner2>.carousel-item.active {
        transform: translate3d(0, 0, 0);
        -webkit-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
        ;
        -moz-transform: translate3d(0, 0, 0);
        -o-transform: translate3d(0, 0, 0);
        top: 0;

    }

    /*------------------------------- vertical carousel indicators ------------------------------*/
    .carousel-indicators {
    position: absolute;
    top: 0;
    bottom: 0;
    /* margin: auto; */
    /* height: 69px; */
    right: 0;
    left: 485px;
    width: auto;
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
}

    .carousel-indicators li {
        display: block;
        margin-bottom: 5px;
        border: 1px solid #00a199;

    }

    .carousel-indicators li.active {
        margin-bottom: 5px;
        background: #00a199;

    }
</style>
@endsection
@section('content')
<div class="wrapper">
    <div class="page-header page-header-small" style="min-height: 100vh;">
        <div class="page-header-image" data-parallax="true">
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="/web/slider/slider1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="/web/slider/slider2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="/web/slider/slider3.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="/web/slider/slider4.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="/web/slider/slider5.jpg" class="d-block w-100" alt="...">
                    </div>
                    <!--https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg-->
                </div>

            </div>
        </div>
        <div class="content-center">

            <div class="row">
                <div class="col-md-8">

                    <img src="/web/assets/img/activate@2x.png" width="62%" alt="">

                    <br>
                    <br>
                    <h1 style="text-align: left; color:grey; font-family: GalanoGrotesqueAlt;">¡Un ejército de
                        trabajadores
                        disponibles en todo
                        momento!
                    </h1>
                    <p style="color:grey; font-family: GalanoGrotesqueAlt; font-weight: 600;">¡Regístrate y empieza
                        ahora!</p>
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{ route('register', 'ofertante') }}" style="height: 4rem;
font-size: 1.3rem;
font-family: GalanoGrotesqueAlt;" class="btn btn-block btn-lg  btn-primary btn-round btn-spotify hvr-bounce-to-left">

                                Ofrecer
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{ route('register', 'contratante') }}"
                                class="btn btn-block btn-lg btn-info btn-round hvr-bounce-to-right"
                                style="height: 4rem;font-size: 1.3rem;font-family: GalanoGrotesqueAlt;">
                                Contratar
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="text-center" style="font-size:1.4rem">
                        <span style="color:grey; font-family: GalanoGrotesqueAlt !important;">───── Ya tenés una cuenta?
                            ─────</span><br>
                        <br>
                        <a href="{{ route('login') }}" style="color:#004897; font-weight:600;">
                            Iniciar sesión</a>
                    </div>

                </div>

                <div class="col-md-4"></div>

            </div>
        </div>
    </div>
    <!--div class="container">
<div class="row">


<div class="col-md-12 ml-auto mr-auto">
<div class="slide-wrapper" style="border-radius:15px; padding: 15px; border: 11px solid;    min-height: 38rem;">
<div id="myCarouselTutorial" class="carousel slide" data-ride="carousel">
<div class="carousel-inner2" style="border-radius: 15px; font-family: GalanoGrotesqueAlt;">
<div class="carousel-item active">
<div class="container">
<h3 class="title text-center">¿Como funciona ACTÍVATE?</h3>
<p>
Imaginate una pagina, donde encontras a profesionales reconocidos y mejores talentos, en todo el pais por zona o categoria.
<br>Por la primera vez en Paraguay, formas parte de una comunidad de trabajo y oportunidades, tenes acceso a ilimitados trabajadores y contratantes de primera categoria como tu.
<br>Permite a los trabajadores crear un perfil profesional, y es garantizado, no necesitas preocuparte por asegurar tu posicion, estas siempre accesible a todas las oportunidades.
<br>Es negociable y tu pones los terminos, asegura que te paguen de una forma facil y a tiempo.
<br>Ayuda a los contratantes a encontrar buenos profesionales, a chatear en tiempo real y en un solo click puedes ver su certificaciones, su formacion y trabajos realizados. Encontra el mejor servicio para tu negocio y hogar.
<br>Actívate es abierto para todos, si tenes lo que se necesita y queres unirte, registrate ya!
</p>
</div>
</div>
<div class="carousel-item">
<div class="container">
<br>
<h3 class="title text-center">Queres contratar un servicio?</h3>
<br>
<br>
<br>
<br>
<div class="row">
<div class="col-sm-4 p-4" data-aos="fade-right" data-aos-duration="1000">
<div>
<a href="#" class="intro-banner-vdo-play-btn" target="_blank">
<img src="/web/gif/c1.gif" alt="">


</a>
</div>
<br>
<br>
<div class="text-center" style="font-family:GalanoGrotesqueAlt; font-weight: 600;">

<br>
<br>
<h3>Interactua y selecciona al mejor profesional</h3>
<p>Tu eliges con quien quieres trabajar, en que zona y el rango de precios que te conviene.</p>

</div>
</div>
<div class="col-sm-4 p-4" data-aos="fade-down" data-aos-duration="1000">
<div>
<a href="#" class="intro-banner-vdo-play-btn" target="_blank">
<img src="/web/gif/c2.gif" alt="">

</a>
</div>
<br>
<br>
<div class="text-center" style="font-family:GalanoGrotesqueAlt; font-weight: 600;">
<br>
<br>
<h3>Revisa los Perfiles</h3>
<p>Analiza los candidatos, sus habilidades y trabajos realizados para conocerlos a fondo.</p>

</div>
</div>
<div class="col-sm-4 p-4" data-aos="fade-left" data-aos-duration="1000">

<a href="#" class="intro-banner-vdo-play-btn" target="_blank">
<img src="/web/gif/c3.gif" alt="">
</a>
</div>
<br>
<br>
<div class="text-center" style="font-family:GalanoGrotesqueAlt; font-weight: 600;">
<br>
<br>
<h3>Chatea en tiempo real</h3>
<p>Contacta en minutos y paga por hora.</p>

</div>
</div>
</div>
<br>
</div>
</div>
<div class="carousel-item">
<div class="container">
<br>
+
</div>
</div>
</div>

</div>
</div>
<br>



</div>
</div>

</div-->
<br>
<br>
<div class="slide-wrapper">
        <div class="container">
            <div id="myCarouselTutorial" class="carousel slide" data-ride="carousel" style="border-radius:15px;
            padding: 15px;
            border: 25px solid;
            min-height: 38rem;">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarouselTutorial" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarouselTutorial" data-slide-to="1"></li>
                    <li data-target="#myCarouselTutorial" data-slide-to="2"></li>
                    <li data-target="#myCarouselTutorial" data-slide-to="3"></li>
                    <li data-target="#myCarouselTutorial" data-slide-to="4"></li>
                    <li data-target="#myCarouselTutorial" data-slide-to="5"></li>


                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner2 carousel-inner container">
                    <!--h3 class="title text-center">Queres contratar un servicio?</h3-->

                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Interactua y selecciona al mejor profesional</h2>
                                <h2>Tu eliges con quien quieres trabajar, en que zona y el rango de precios que te conviene.</h2>
                            </div>
                            <div class="col">
                                <img src="/web/gif/c1.gif" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col">
                                <img src="/web/gif/c2.gif" alt="">
                            </div>
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Revisa los Perfiles</h1>
                                <h2>Analiza los candidatos, sus habilidades y trabajos realizados para conocerlos a fondo.</h2>

                            </div>


                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Chatea en tiempo real</h2>
                                <h2>Contacta en minutos y paga por hora.</h2>
                            </div>
                            <div class="col">
                                <img src="/web/gif/c3.gif" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col">
                                <img src="/web/gif/o5.gif" alt="">
                            </div>
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">¿Quéres ofrecer un servicio?</h1>
                                <h2>texto descriptivo OPCIONAL.</h2>

                            </div>


                        </div>
                    </div>

                    <!-- <h3 class="text-center title">Queres ofrecer un servicio?</h3> -->
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Crea tu Perfil Profesional</h2>
                                <h2>Subi una buena foto de perfil, carga tus datos, documentos y habilitaciones.</h2>
                            </div>
                            <div class="col">
                                <img src="/web/gif/o1.gif" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col">
                                <img src="/web/gif/o5.gif" alt="">
                            </div>
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Compartí tu ubicación actual</h1>
                                <h2>Consegui trabajos estando en cualquier lugar.</h2>

                            </div>


                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Chatea en tiempo real</h1>
                                <h2>Utiliza el chat para ofrecer y elegir el trabajo que mas te convenga. negocia precios y
                                    horarios que mejor te queden.</h2>
                            </div>
                            <div class="col">
                                <img src="/web/gif/c3.gif" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col">
                                <img src="/web/gif/04.gif" alt="">
                            </div>
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Forma parte de una comunidad de profesionales</h1>
                                <h2>Donde te puedes destacar y compartir trabajos realizados</h2>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Recibe una buena puntuación por tus trabajos</h1>
                                <h2>Los contratantes dejan comentarios sobre tu servicio, dando credibilidad a tu perfil</h2>
                            </div>
                            <div class="col">
                                <img src="/web/gif/o3.gif" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col">
                                <img src="/web/gif/o6.gif" alt="">
                            </div>
                            <div class="col" style="margin: auto;">
                                <h1 style="font-weight: bold;">Ahorra tiempo</h1>
                                <h2>Sin necesidad de firmar contratos o tener horarios fijos. Cuanto más tiempo estes Online
                                    desde la comodidad de tu casa, más puedes conseguir empleo.</h2>

                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>


    @include('web.layouts.footer')


    @endsection
    @section('scripts')
    {{--<script type="text/javascript" src="https://demo.themesberg.com/pixel-pro/assets/js/pixel.js"></script>--}}
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="/web/assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    {{--<script src="/web/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>--}}
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="/web/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
    <script>
        AOS.init();

        $('#myCarouselTutorial').carousel({
            interval: 15000
        });

        //scroll slides on swipe for touch enabled devices

        $("#myCarouselTutorial").on("touchstart", function (event) {

            var yClick = event.originalEvent.touches[0].pageY;
            $(this).one("touchmove", function (event) {

                var yMove = event.originalEvent.touches[0].pageY;
                if (Math.floor(yClick - yMove) > 1) {
                    $(".carousel").carousel('next');
                }
                else if (Math.floor(yClick - yMove) < -1) {
                    $(".carousel").carousel('prev');
                }
            });
            $(".carousel").on("touchend", function () {
                $(this).off("touchmove");
            });
        });
    </script>
    @endsection
