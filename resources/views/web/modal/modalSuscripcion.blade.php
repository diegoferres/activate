<div class="modal" id="modalSuscripcion">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
        
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
            <div class="wizard">
                <div class="wizard-inner text-center">
                   
                       

                            <ul class="nav nav-tabs"  role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" aria-expanded="true"></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" aria-expanded="false"></a>
                                </li>
                                
                                
                            </ul>
                        </div>
                     
                    
                </div>

                <form role="form" action="index.html" class="login-box">
                    <div class="tab-content" id="main_form">
                        <div class="tab-pane active" role="tabpanel"  id="step1">
                            <h4 class="text-center">Selecciona una categoría</h4>
                            
                             
                                <div class="paymentCont">
                                  
                                    <div class="paymentWrap">
                                        <div class="btn-group paymentBtnGroup btn-group-justified" data-toggle="buttons">
                                            <div class="row text-center">
                                            <div class="col-sm-4">
                                            <label class="btn paymentMethod ">
                                                <div class="method">
                                                    <br>
                                                    <i style="font-size: 3rem;" class="fas fa-cogs"></i><br><br>
                                                    Servicio Técnico</div>
                                                <input style="opacity: 0;" type="radio" name="options" checked> 
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod">
                                                <div class="method">
                                                    <br>
                                                    <i style="font-size: 3rem;" class="fas fa-hard-hat"></i><br><br>
                                                    Construcción
                                                </div>
                                                <input style="opacity: 0;" type="radio" name="options"> 
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod">
                                                <div class="method ">
                                                    <br>
                                                    <i style="font-size: 3rem;" class="fas fa-spa"></i><br><br>                                                 Belleza
                                                </div>
                                                <input style="opacity: 0;" type="radio" name="options">
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod">
                                                <div class="method ">
                                                    <br>
                                                    <i style="font-size: 3rem;" class="fas fa-chalkboard-teacher"></i><br><br>
                                                    Educación
                                                </div>
                                                <input style="opacity: 0;" type="radio" name="options">
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod">
                                                <div class="method ">
                                                    <br>
                                                    <i style="font-size: 3rem;" class="fas fa-heartbeat"></i><br><br>
                                                    Bienestar
                                                </div>
                                                <input style="opacity: 0;" type="radio" name="options">
                                            </label>
                                        </div>
                                           
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod">
                                                <div class="method">
                                                    <br>
                                                    <i style="font-size:3rem" class="fas fa-dice"></i><br><br>
                                                    Entrenimiento
                                                </div>
                                                <input style="opacity: 0;" type="radio" name="options">
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod">
                                                <div class="method">
                                                    <br>
                                                    <i style="font-size:3rem;" class="fas fa-birthday-cake"></i><br><br>
                                                    Eventos
                                                </div>
                                                <input style="opacity: 0;" type="radio" name="options">
                                            </label>
                                        </div>
                                           
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod">
                                                <div class="method">
                                                    <br>
                                                    <i style="font-size: 3rem;" class="fas fa-laptop-house"></i><br><br>
                                                    Home Office
                                                </div>
                                                <input style="opacity: 0;" type="radio" name="options">
                                            </label>
                                        </div>
                                           
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod">
                                                <div class="method">
                                                    <br>
                                                    <i style="font-size: 3rem;" class="fas fa-drafting-compass"></i><br><br>
                                                    Moda
                                                </div>
                                                <input style="opacity: 0;" type="radio" name="options">
                                            </label>
                                        </div>
                                           
                                           
                                           
                                         
                                        </div>        
                                        <br>
                                         
                                    </div>
                                 
                                </div>
                          
                                
                                
                            </div>
                            <ul class="list-inline pull-right">
                                <li><button type="button" class=" btn btn-round  next-step">Continuar</button></li>
                            </ul>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <h4 class="text-center">¿Qué servicio estas buscando?</h4>
                            <div class="row">
                                <div class="container">
                                    <div class="form-group">
                                        <label>Servicios disponibles</label> 
                                        <select name="country" class="form-control" id="country">
                                            <option value="NG" selected="selected">Contador</option>
                                            <option value="NU">Personal Juridico</option>
                                            <option value="NF">Fotógrafo</option>
                                            <option value="KP">Diseñador</option>
                                            <option value="MP">Desarrollador web</option>
                                            <option value="NO">Ingeniero civil</option>
                                            <option value="NO">Arquitecto</option>

                                        </select>
                                    </div>
                                </div>
                              
                            
                            
                            
                            
                          
                           </div>
                            
                            
                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-round prev-step">Atras</button></li>
                                <li><a href="login-page.html" class="btn btn-round next-step">Registrate</a></li>
                            </ul>
                        </div>
                        
                        
                        <div class="clearfix"></div>
                    </div>
                    
                </form>
            </div>
        </div>
  
        <!-- Modal footer -->
        
  
      </div>
    </div>