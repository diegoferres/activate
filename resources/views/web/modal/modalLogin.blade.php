<div class="modal" id="modalLogin">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h3>Por favor inicia sesión</h3>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a href="#" class="btn btn-lg btn-info btn-block"><i class="fab fa-facebook-f"></i> Facebook</a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a href="#" class="btn btn-lg btn-outline-danger btn-block"><i class="fab fa-google"></i> Google</a>
                    </div>
                </div>
                <div class="login-or">
                    <hr class="hr-or">
                    <span class="span-or">ó</span>
                </div>

                <form role="form">
                    <div class="form-group">
                        <label for="inputUsernameEmail">Nombre de usuario</label>
                        <input type="text" class="form-control" id="inputUsernameEmail">
                    </div>
                    <div class="form-group">
                        <a class="pull-right" href="#">Olvidadaste la contraseña?</a>
                        <label for="inputPassword">Contraseña</label>
                        <input type="password" class="form-control" id="inputPassword">
                    </div>
                    <div class="checkbox pull-right">
                        <label>
                            <input type="checkbox">
                            Recordar datos </label>
                    </div>
                    <button type="submit" class="btn btn-lg btn-block btn-primary">
                        Iniciar sesión
                    </button>
                </form>

                <br>
                <div class="text-center">
                    <span>Aún no tengo una cuenta. <a href=""><strong
                                    style="color: #004897;">Registrarme</strong></a></span>
                </div>
            </div>
        </div>
    </div>
</div>