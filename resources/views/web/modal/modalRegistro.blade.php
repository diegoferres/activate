<div class="modal" id="modalRegistro">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <h3>Completa los campos solicitados</h3>
          <form role="form">
            <div class="form-group">

              <label for="inputName">Nombres/ Apellidos</label>
              <input type="text" class="form-control" id="inputName">
            </div>

            <div class="form-group">
              <label for="inputCi">Nro. de C.I.</label>
              <input type="text" class="form-control" id="inputCi">
            </div>
            <div class="form-group">

              <label for="inputEmail">Email</label>
              <input type="text" class="form-control" id="inputEmail">
            </div>
            <div class="form-group">

              <label for="inputPassword">Contraseña</label>
              <input type="password" class="form-control" id="inputPassword">
            </div>

            <button type="submit" class="btn btn-lg btn-block btn-primary">
              Crear cuenta
            </button>
          </form>
          <div class="login-or">
            <hr class="hr-or">
            <span class="span-or">ó</span>
          </div>
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
              <a href="#" class="btn btn-lg btn-info btn-block"><i class="fab fa-facebook-f"></i> Facebook</a>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
              <a href="#" class="btn btn-lg btn-outline-danger btn-block"><i class="fab fa-google"></i> Google</a>
            </div>
          </div>
          <br>
          <div class="text-center">
            <span>Ya tengo una cuenta. <a href=""><strong style="color: #004897;">Iniciar sesión</strong></a></span>
          </div>
        </div>
      </div>
    </div>
  </div>