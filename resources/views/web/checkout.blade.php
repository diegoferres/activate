@extends('web.layouts.app')
@section('title')
    Actívate - Suscripción
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-user"></i> Perfil</a>
    @if (Auth::id() == $user->id)
    <a class="dropdown-item" href="#crearCV" data-toggle="modal" data-target="#crearCV"><i class="far fa-address-card"></i> Crear CV</a>
    @endif
    <a class="dropdown-item" href="{{ route('chat') }}"><i class="far fa-comments"></i> Chat</a>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item"><i class="fas fa-undo-alt"></i> Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
@section('style')
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">

    <style>
        .navbar.navbar-transparent {
            background-color: #004897 !important;
            box-shadow: none;
            color: #FFFFFF;
            padding-top: 20px !important;
        }

        .card:hover {
            border: 1px solid lawngreen;


        }

        .active {
            border: 1px solid lightcoral;
        }

        .font-medium {
            font-size: 3rem;
        }
    </style>
@endsection
@section('navbaroptions')
    <a class="dropdown-header">Cuenta</a>
    <a class="dropdown-item" href="#"><i class="fas fa-binoculars"></i> Vision integral</a>
    <a class="dropdown-item" href="#"><i class="far fa-paper-plane"></i> Planes disponibles</a>
    <a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Crear CV</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
@endsection
@section('body_open')
    profile-page sidebar-collapse
@endsection
@section('content')
    <!--<div class="wrapper">
       <div class="page-header  page-header-small">
            <div class="page-header-image" data-parallax="true"
                 style="background-image:url('/web/assets/img/checkout.jpg');">
            </div>

            <br>
            <div class="container">

                <div class="row">
                    <div class="col-sm-6">
                        <h1 style="text-align: left;">Antes de continuar<br>hecha un vistaso,<br> a los detalles de<br>tu
                            suscripción.</h1>
                        <br>

                    </div>

                    <div class="col-sm-6">
                        <video autoplay width="57%"
                               style="-webkit-mask-box-image: url('https://freepik.cdnpk.net/img/video/deadline/mask.svg'); mask-image: url('');">
                            <source src="https://freepik.cdnpk.net/img/video/deadline/video.mp4" type="video/mp4">
                        </video>

                    </div>
                </div>
            </div>
        </div>

    </div>-->
    <div class="section" style="font-family:GalanoGrotesqueAlt !important;">
        <div class="container">
            <br>
            <br>
            <div class="large-container">
                <div class="sec-title">
                    <span class="title">Checkout</span>
                    <h2>Detalles del Plan</h2>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Plan seleccionado:</h3>
                        <div class="paymentCont">
                            <div class="paymentWrap" style="padding:0!important">
                                <div class="btn-group paymentBtnGroup btn-group-justified"
                                     data-toggle="buttons">
                                    <div class="row text-center">
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod @if($plan->id == 1) active @endif">
                                                <div class="method" style="font-size:large; padding:1rem">
                                                    <br>
                                                    Plan
                                                    <br>
                                                    3 meses<br>

                                                </div>
                                                <input style="opacity: 0;" value="1" type="radio" name="options">
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod @if($plan->id == 2) active @endif">
                                                <a href="{{ route('web.suscription.checkout', 2) }}">
                                                    <div class="method " style="font-size:large; padding:1rem">
                                                        <br>
                                                        Plan
                                                        <br>
                                                        6 meses
                                                        <br>
                                                    </div>
                                                    <input style="opacity: 0;" value="2" type="radio" name="options">
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="btn paymentMethod @if($plan->id == 3) active @endif">
                                                <div class="method " style="font-size:large; padding:1rem">
                                                    <br>
                                                    Plan
                                                    <br>
                                                    365 días
                                                    <br>

                                                </div>
                                                <input style="opacity: 0;" value="3" type="radio" name="options">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <div id="price_1" class="price" @if($plan->id != 1) style="display: none" @endif>
                            <h3>Precio mensual:<br><span style="font-weight: bold; color:#004897"> Gs.</span><span
                                    style="font-weight: 600; color: grey;"
                                    class="font-medium"> {{ number_format(120000, 0, ',','.') }}</span>
                                / <span style=" color:#1ed760 ">3 meses.</span></h3>
                        </div>
                        <div id="price_2" class="price" @if($plan->id != 2) style="display: none" @endif>
                            <h3>Precio mensual:<br><span style="font-weight: bold; color:#004897"> Gs.</span><span
                                    style="font-weight: 600; color: grey;"
                                    class="font-medium"> {{ number_format(165000, 0, ',','.') }}</span>
                                / <span style=" color:#1ed760 ">6 meses.</span></h3>
                        </div>
                        <div id="price_3" class="price" @if($plan->id != 3) style="display: none" @endif>
                            <h3>Precio mensual:<br><span style="font-weight: bold; color:#004897"> Gs.</span><span
                                    style="font-weight: 600; color: grey;"
                                    class="font-medium"> {{ number_format(220000, 0, ',','.') }}</span>
                                / <span style=" color:#1ed760 ">365 días.</span></h3>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h3><strong>Incluye:</strong></h3>
                        <div id="plan_1" class="card-body">
                            <ul class="list-group mb-4">
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Disfruta del uso sin anuncios
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Contacta las 24hs vía chat o whatsapp
                                    on-Time 24hs
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Recibi Puntuación y comentarios de tus Servicios
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Integra tu perfil a Eventos
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Comparti tu ubicación
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Promociona tu curso.
                                </li>
                            </ul>
                        </div>
                        <div id="plan_2" class="card-body" style="display: none">
                            <ul class="list-group mb-4">
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Disfruta del uso sin anuncios
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Contacta las 24hs vía chat o whatsapp
                                    on-Time 24hs
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Recibi Puntuación y comentarios de tus Servicios
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Integra tu perfil a Eventos
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Comparti tu ubicación
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Promociona tu curso.
                                </li>
                            </ul>
                        </div>
                        <div id="plan_3" class="card-body" style="display: none">
                            <ul class="list-group mb-4">
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Disfruta del uso sin anuncios
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Contacta las 24hs vía chat o whatsapp
                                    on-Time 24hs
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Recibi Puntuación y comentarios de tus Servicios
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Integra tu perfil a Eventos
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Comparti tu ubicación
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Promociona tu curso.
                                </li>
                                <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                    Foto profesional del perfil.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h3>Pagar con:</h3>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="pagopar" name="pagos" value="customEx"
                                   checked>
                            <label class="custom-control-label" for="pagopar"><img src="/web/assets/img/pagopar.png"
                                                                                   width="30%" alt=""></label>
                        </div>
                        <br>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{ route('web.suscription.finish', $plan->id) }}" data-toggle="modal"
                           data-target="#modalAlertPay" class="btn btn-lg btn-info btn-round">Continuar</a>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <!-- The Modal -->
        <div class="modal" id="modalAlertPay">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <!--div class="modal-header">
                      <h4 class="modal-title">Modal Heading</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div-->

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h1 style="font-weight: bold;">Ya estas Listo.</h1>
                                    <h3>Sumerguite en la web y conoce todos los beneficios </h3>
                                    <p style="color: gray;">
                                        Te puntuamos 1 estrella por haber completado el Perfil correctamente.
                                        <br>Chatea 24hs
                                        <br>Comparti tu perfil en Facebook Twitter Instagram y Whatsapp!
                                        <br>Subi fotos de trabajos que hiciste, habilitaciones de tu profesion y otros
                                        registros.
                                        <br>Mira lo que hacen otros trabajadores, para mejorar tu presentacion.
                                    </p>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <img src="/web/gif/ready.gif" alt="">
                                    <a href="{{ route('web.suscription.finish', $plan->id) }}"
                                       style="font-size: 1.2rem;" class="btn btn-lg btn-info btn-round">Comenzar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('web.layouts.footer')
@endsection
@section('scripts')
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            $('[name="options"]').change(function (e) {
                e.preventDefault()
                console.log('THIS', this.value)
                var id = this.value;
                $('.card-body').slideUp(function () {
                    $('#plan_' + id).slideDown()
                });

                $('.price').slideUp(function () {
                    $('#price_' + id).slideDown()
                });
            });


            $('#qty_input').prop('disabled', true);
            $('#plus-btn').click(function () {

                $('#qty_input').val(parseInt($('#qty_input').val()) + 1);
            });
            $('#minus-btn').click(function () {
                $('#qty_input').val(parseInt($('#qty_input').val()) - 1);
                if ($('#qty_input').val() == 0) {
                    $('#qty_input').val(1);
                }

            });
        });

        jQuery('.card').click(function () {
            jQuery('.card').removeClass('active');
            jQuery(this).addClass('active');
        });
    </script>
@endsection
