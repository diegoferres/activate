@extends('web.layouts.app')
@section('title')
    Actívate - Preguntas Frecuentes
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-user"></i> Perfil</a>
    @if (Auth::id() == $user->id)
    <a class="dropdown-item" href="#crearCV" data-toggle="modal" data-target="#crearCV"><i class="far fa-address-card"></i> Crear CV</a>
    @endif
    <a class="dropdown-item" href="{{ route('chat') }}"><i class="far fa-comments"></i> Chat</a>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item"><i class="fas fa-undo-alt"></i> Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
@section('style')
<link rel="stylesheet" href="/web/assets/css/hover.css">
<link rel="stylesheet" href="/web/galanoFont/stylesheet.css">


<style>
    #accordion-style-1 h1,
#accordion-style-1 a{
    color:#007b5e;
}
#accordion-style-1 .btn-link {
    font-weight: 400;
    color: #007b5e;
    background-color: transparent;
    text-decoration: none !important;
    font-size: 16px;
    font-weight: bold;
	padding-left: 25px;
}

#accordion-style-1 .card-body {
    border-top: 2px solid #007b5e;
}

#accordion-style-1 .card-header .btn.collapsed .fa.main{
	display:none;
}

#accordion-style-1 .card-header .btn .fa.main{
	background: #007b5e;
    padding: 13px 11px;
    color: #ffffff;
    width: 35px;
    height: 41px;
    position: absolute;
    left: -1px;
    top: 10px;
    border-top-right-radius: 7px;
    border-bottom-right-radius: 7px;
	display:block;
}
.faqs{
    font-size: 1.2rem;
}
.navbar.navbar-transparent {
    background-color: #004897 !important;
    box-shadow: none;
    color: #FFFFFF;
    padding-top: 20px !important;
}

</style>
@endsection
@section('navbaroptions')
    <a class="dropdown-header">Cuenta</a>
    <a class="dropdown-item" href="#"><i class="fas fa-binoculars"></i> Vision integral</a>
    <a class="dropdown-item" href="#"><i class="far fa-paper-plane"></i> Planes disponibles</a>
    <a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Crear CV</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
@endsection
@section('body_open')
    profile-page sidebar-collapse
@endsection
@section('content')

    <div class="section">
        <div class="container">
            <br>
            <br>
            <br>
            <div class="large-container">
                <div class="sec-title">
                    <span class="title">FAQS</span>
                    <h1>Preguntas Frecuentes</h1>
                </div>
                <div class="container">
                    <div class="dropdown accordion" id="accordionExample">
                        <!--LA PAGINA-->
                        <h3 class="dropdown-toggle" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">La página</h3>
                        <div id="collapseOne" class="collapse show fade" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="accordion" id="lapagina">
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#lapagina-one" aria-expanded="true" aria-controls="lapagina-one" href="#">¿Qué es Actívate?</a>
                        <p id="lapagina-one" class="collapse  fade" aria-labelledby="headingOne" data-parent="#lapagina">
                            ¿Sos trabajador independiente, ofreces servicios y no sabes cómo llegar directamente a clientes interesados en los servicios que ofreces?
Actívate, cambiará la experiencia del trabajo por cuenta propia.
Es la única plataforma donde te ofrecemos todas las herramientas para que llegues a más clientes que se encuentren cerca tuyo.

                        </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#lapagina-two" aria-expanded="true" aria-controls="lapagina-two" href="#">¿Por qué utilizar Activate?</a>
                        <p id="lapagina-two" class="collapse  fade" aria-labelledby="headingOne" data-parent="#lapagina">
                            Al usar Actívate, formas parte de una red de más de mil trabajadores que buscan trabajos igual que tú, al crear tu cuenta en esta activo en el mercado y en carrera contra tu competencia. Te conectamos con los clientes que necesitan tus servicios. El trato lo cierran directamente Ustedes, sin complicaciones y chatean en tiempo real.
Solo tienes que crear tu perfil interesante, publicar tu experiencia, tus trabajos realizados y toda la información posible para que los clientes vean tu capacidad y se contacten directamente contigo.

                        </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#lapagina-three" aria-expanded="true" aria-controls="lapagina-three" href="#">¿Qué servicios puedo ofrecer?</a>
                        <p id="lapagina-three" class="collapse  fade" aria-labelledby="headingOne" data-parent="#lapagina">
                            Podes ofrecer todos los servicios que realices, siempre y cuando haya una referencia o garantía de tu experiencia y tengas la disponibilidad para cumplir con los clientes.
                            En base a tus habilidades y conocimientos selecciona más de un cargo que quieras ocupar.                            
                        </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#lapagina-four" aria-expanded="true" aria-controls="lapagina-four" href="#">Tengo problemas de conexión cuando trato de iniciar sesión.</a>
                   <p id="lapagina-four" class="collapse fade" aria-labelledby="headingOne" data-parent="#lapagina">
                    Te recomendamos cerrar la sesión y volver a iniciar. Si el problema persiste, consulta a nuestro centro de ayuda. 
                    <a href="" class="btn btn-round btn-lg btn-info" style="font-size: 1.2rem;">Ayuda</a>
                   </p>
                            </div>    
                        </div>
                        <!--PERFIL-->
                        <h3 class="dropdown-toggle" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Perfil</h3>
                        <div id="collapseTwo" class="collapse show fade" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="accordion" id="perfil">
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-one" aria-expanded="true" aria-controls="perfil-one" href="#">¿Cómo funciona?</a>
                            <p id="perfil-one" class="collapse  fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Es fácil, siguiendo estos pasos:
                                Crea tu Perfil Profesional
                                Subí una buena foto de perfil, Carga tus datos, títulos, documentos y habilitaciones.
                                Compartí tu ubicación actual
                                Conseguí trabajos cerca de tu zona. Nada mejor que llegar a tiempo.
                                Chatea en tiempo real 
                                Utiliza el chat para ofrecer y elegir el trabajo que más te convenga. Negocia precios y horarios que mejor te queden.
                                Forma parte de una comunidad de profesionales 
                                Donde te puedes destacar y compartir trabajos realizados.
                                Recibe una buena puntuación por tus trabajos
                                Los contratantes dejan comentarios sobre tu servicio. Dando credibilidad a tu perfil.
                                
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-two" aria-expanded="true" aria-controls="perfil-two" href="#">¿Cómo creo mi perfil?</a>
                            <p id="perfil-two" class="collapse  fade" aria-labelledby="headingTwo" data-parent="#perfil">      
                                Crea una cuenta, ingresando al enlace; Crear tu cuenta.
                                Llena los campos; nombre, apellido, dirección, fecha de nacimiento, teléfono y dirección de mail.
                                Subí una buena foto de perfil.
                                Carga tu Curriculum, Licencias, certificados, fotos de trabajos realizados y deja una descripción de los servicios que ofreces, así también tu ubicación, rango de horario y precios.
                                
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-three" aria-expanded="true" aria-controls="perfil-three" href="#">¿Cómo edito mi perfil?</a>
                            <p id="perfil-three" class="collapse  fade" aria-labelledby="headingTwo" data-parent="#perfil">                         
                                Inicie Sesión.
                                Haga clic en la foto miniatura de su foto de perfil en la barra del menú principal.
                                Haga clic en el botón Editar perfil.
                                
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-four" aria-expanded="true" aria-controls="perfil-four" href="#">¿Puedo utilizar Actívate si soy empleado de una empresa?, pero tengo tiempo libre para trabajar.</a>
                            <p id="perfil-four" class="collapse fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Sí, puedes utilizar Actívate siempre y cuando te sea posible cumplir con los servicios que ofreces. También debes contar con factura legal por que el cliente te lo solicitará.

                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-five" aria-expanded="true" aria-controls="perfil-five" href="">¿Por qué tengo que subir mi Currículum?</a>
                            <p id="perfil-five" class="collapse fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Es muy importante que completes tu Curriculum, ya que los clientes necesitan tener toda la información posible sobre tu experiencia. 
                                De esto depende de que seas contactado. Un perfil incompleto hace que tus posibles clientes miren otros perfiles y te dejen de lado.
                                
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-six" aria-expanded="true" aria-controls="perfil-six" href="">¿Cómo obtengo mi foto profesional?</a>
                            <p id="perfil-six" class="collapse fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Lo obtienes con el Plan Premium y nosotros nos pondremos en contacto contigo para agendar una sesion de Foto. (BOTON PREMIUM)
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-seven" aria-expanded="true" aria-controls="perfil-seven" href="">¿Cómo ayudo a promover mi perfil?</a>
                            <p id="perfil-seven" class="collapse fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Un perfil más completo es siempre más interesante. Te recomendamos subir buenas fotos de tus trabajos, un Curriculum detallado, imágenes de registro, certificados de estudios o capacitaciones, etc. Con el Plan Premium accedes a una foto de perfil profesional y te mantiene activo sobre los demás perfiles. Los comentarios de tus clientes y sus valoraciones son tenidos en cuenta por los demás, hacer un trabajo excelente y que tus clientes dejen buenos comentarios te ayudara a tener un perfil más competitivo. (BOTON PREMIUM)
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-eigth" aria-expanded="true" aria-controls="perfil-eigth" href="">¿Por qué es importante compartir mis datos?</a>
                            <p id="perfil-eigth" class="collapse fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Los datos son importantes a la hora de que los clientes elijan trabajar contigo. Ellos necesitan saber, quien sos, tu edad, sexo, donde vives, a que te dedicas y por sobretodo dar seguridad de que tu perfil es real.
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-nine" aria-expanded="true" aria-controls="perfil-nine" href="">¿Cómo desactivo mi cuenta?</a>
                            <p id="perfil-nine" class="collapse fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Si decides volver, estamos listos para ayudarte a que sigas en la carrera laboral.
                                Ingresa en CONFIGURACIONES DE PERFIL, ahí encontrarás la opción DESACTIVAR CUENTA.
                                
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-teem" aria-expanded="true" aria-controls="perfil-teem" href="">¿Puedo vincular mi perfil de Facebook, Instagram o Twitter a mi cuenta Activate?</a>
                            <p id="perfil-teem" class="collapse fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Podes crear tu perfil por medio de tus datos de Facebook o Gmail, pero estas cuentas no se vinculan, lo que si puedes hacer es compartir tu perfil en tus redes sociales.
                            </p>
                        <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#perfil-eleven" aria-expanded="true" aria-controls="perfil-eleven" href="">¿Qué puedo hacer si no me contacta nadie?</a>
                            <p id="perfil-eleven" class="collapse fade" aria-labelledby="headingTwo" data-parent="#perfil">
                                Puedes promocionarte compartiendo a todas tus redes tu perfil de activate en FACEBOOK, INSTAGRAM y TWITTER. (BOTONES TWITTER FACEBOOK)
                            </p>
                            </div>    
                        </div>
                        <!--CUENTA-->
                        <h3 class="dropdown-toggle" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Cuenta</h3>
                        <div id="collapseThree" class="collapse show fade" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="accordion" id="cuenta">
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-one" aria-expanded="true" aria-controls="cuenta-one" href="">¿Cómo paso un presupuesto?</a>
                                <p id="cuenta-one" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Lo podes presentar en Word, Excel, PDF, Imagen, Correo o un simple mensaje via Chat Online o Whatsapp. En la manera que te sea más cómoda y rápida. 
                                    Te sugerimos que seas lo más claro y detallista posible en tu presupuesto para evitar malos entendidos a la hora de llevar a cabo tu servicio.
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-two" aria-expanded="true" aria-controls="cuenta-two" href="">¿Qué tasa de IVA debo indicar?</a>
                                <p id="cuenta-two" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    En el país se mantiene la estructura vigente con la tasa general del 10% de IVA.

                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-three" aria-expanded="true" aria-controls="cuenta-three" href="">¿Cómo puedo saber si mi cliente ha aceptado mi presupuesto?</a>
                                <p id="cuenta-three" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    La aceptación del presupuesto dependerá de la negociación que tuviste con el cliente. El cliente te confirmara directamente a tu chat Online o via Whatsapp.

                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-four" aria-expanded="true" aria-controls="cuenta-four" href="">¿Qué plazo de pago debo indicar?</a>
                                <p id="cuenta-four" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Tú defines tus términos con el cliente, la forma de pago, garantía, devolución o reembolso. 
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-five" aria-expanded="true" aria-controls="cuenta-five" href="">¿Cómo acepto el trabajo?</a>
                                <p id="cuenta-five" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Felicidades!! Tu propuesta fue aceptada!! La aceptación del trabajo lo tienes que comunicar directamente a tu cliente. Recuerda que Activate es el nexo que los une, lo demás ya corre a cargo tuyo.
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-six" aria-expanded="true" aria-controls="cuenta-six" href="">¿Cobran comisión por los trabajos que hice?</a>
                                <p id="cuenta-six" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    No. No cobramos comisiones sobre los trabajos que realizaste, es más, ni sabemos los acuerdos en que llegan el cliente con el trabajador.
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-seven" aria-expanded="true" aria-controls="cuenta-seven" href="">¿Cómo puedo denunciar un comentario?</a>
                                <p id="cuenta-seven" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Agregar Botón (Reportar Comentario)
                                    Porque quieres reportar este comentario?
                                    •	No he trabajado con esta persona.
                                    •	No quiero recibir comentarios de esta persona
                                    •	No me parece correcto su comentario
                                    •	Otros motivos
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-eigth" aria-expanded="true" aria-controls="cuenta-eigth" href="">¿Qué hago si no me puedo conectar al Whatsapp?</a>
                                <p id="cuenta-eigth" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    No te preocupes, en ese caso los mensajes te llegaran via chat online en la plataforma o por mensaje de texto.

                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-nine" aria-expanded="true" aria-controls="cuenta-nine" href="">¿Por qué recibo comentarios de personas con quien no he trabajado?</a>
                                <p id="cuenta-nine" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Si no estás de acuerdo con los comentarios que recibiste, podes denunciar y solicitar que se elimine. 
                                    Agregar Botón (Reportar comentario)
                                    Porque quieres reportar este comentario?
                                    •	No he trabajado con esta persona.
                                    •	No quiero recibir comentarios de esta persona
                                    •	No me parece correcto su comentario
                                    •	Otros motivos
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-teem" aria-expanded="true" aria-controls="cuenta-teem" href="">¿Puedo bloquear a personas que ya no quiero que me contacten?</a>
                                <p id="cuenta-teem" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Aquí puedes bloquear a otros Usuarios.
                                    Agregar Botón (Bloquear Usuario)
                                    Porque quieres bloquear a esta persona?
                                    •	Creo que es un perfil falso
                                    •	Ya no quiero trabajar con esta persona
                                    •	Otros motivos
                                    Describir motivos:…………………………………………………………………………………………………………………..
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-eleven" aria-expanded="true" aria-controls="cuenta-eleven" href="">¿Puedo desactivar mi cuenta temporalmente?</a>
                                <p id="cuenta-eleven" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    No se puede eliminar, una vez que este se elimina, ya se borran los datos de manera permanente.
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-twelve" aria-expanded="true" aria-controls="cuenta-twelve" href="">¿Se puede hacer un reclamo sobre un cliente?</a>
                                <p id="cuenta-twelve" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Actívate se desliga de toda responsabilidad sobre las negociaciones a las que hayan llegado el Cliente y Tu. Pero nos interesa conocer tu experiencia.
                                    Agregar Botón (Dejar un comentario)
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-thirteen" aria-expanded="true" aria-controls="cuenta-thirteen" href="">¿Cómo reporto los perfiles falsos que me contactaron?</a>
                                <p id="cuenta-thirteen" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Si te parece que es un perfil falso, déjanos tus comentarios para poder verificar si el Usuario de esta cuenta es real.
                                    Agregar un Botón (Reportar un Perfil).
                                    Creo que es un perfil falso
                                    Me contacta para otros motivos que no son laborales
                                    Otros motivos
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-fourteen" aria-expanded="true" aria-controls="cuenta-fourteen" href="">¿Cómo reporto que una cuenta que se haga pasar por mí?</a>
                                <p id="cuenta-fourteen" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Necesitamos verificar que este perfil es realmente tuyo.
                                    Agregar Botón (Verificación de Identidad)
                                    Nombre y Apellido
                                    Dirección
                                    Teléfono
                                    Correo
                                    Adjuntar documento de identidad.
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-fifteen" aria-expanded="true" aria-controls="cuenta-fifteen" href="">¿Pueden Hackear mi cuenta o usarla sin permiso?</a>
                                <p id="cuenta-fifteen" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Es importante que no compartas tu contraseña, ni datos de tu usuario. Si crees que alguien ha ingresado a tu cuenta sin tu autorizacion, te recomendamos que cierres la sesión en todos los dispositivos que utilizaste y cambies tu contraseña.
                                    Agregar Botón (Cambiar Contraseña) 
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-sixteen" aria-expanded="true" aria-controls="cuenta-sixteen" href="">Si no me encuentro conectado ¿Pierdo contacto con futuros clientes?</a>
                                <p id="cuenta-sixteen" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Si no tenes activado el Plan Premium, los mensajes quedan en tu buzón, donde lo podrás ver una vez que estes conectado. Si tenes el plan Premiun, los mensajes te llegaran directamente a tu whatsapp sin la necesidad de estar conectado a nuestra web.
                                    Cambiate a Premium!!
                                    Agregar Botón (Suscribirse a Premium)
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-seventeen" aria-expanded="true" aria-controls="cuenta-seventeen" href="">¿Cómo reporto que algo no funciona en Actívate?</a>
                                <p id="cuenta-seventeen" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Agregar Botón (Reportar un problema)

                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-eigthteen" aria-expanded="true" aria-controls="cuenta-eigthteen" href="">¿Cómo reporto que otro Ofertante esté usando fotos de mi trabajo?</a>
                                <p id="cuenta-eigthteen" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Agregar Botón (Denunciar cuenta)
                                    Creo que es un perfil falso
                                    Utiliza mis datos
                                    Su datos no existen
                                    Otros motivos
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-nineteen" aria-expanded="true" aria-controls="cuenta-nineteen" href="">¿Puedo crear varias cuentas en Actívate?</a>
                                <p id="cuenta-nineteen" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Para ofrecer servicios solo necesita una sola cuenta, ya que en la misma puedes ofrecer varios servicios, pero si ya tienes un perfil de Oferente y lo que quieres es contratar servicios de otras personas, debes de crear tu perfil de Contratante. Con otro Mail.
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-twenty" aria-expanded="true" aria-controls="cuenta-twenty" href="">¿Cómo recupero mi cuenta si no recuerdo mi contraseña?</a>
                                <p id="cuenta-twenty" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Si no recuerdas tu contraseña, te enviaremos un enlace a tu correo para que puedas modificarlo. BOTON RENOVAR CONTRASEÑA
                                    Agregar Botón (Recuperar mi contraseña)
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-twentyone" aria-expanded="true" aria-controls="cuenta-twentyone" href="">¿Puedo saber cuántas personas ingresaron a mi perfil?</a>
                                <p id="cuenta-twentyone" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    Nuestra Web te ofrece una herramienta de conteo de visitas para que visualices cuantas personas estuvieron visitando tu perfil.
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#cuenta-twentytwo" aria-expanded="true" aria-controls="cuenta-twentytwo" href="">¿Puedo desactivar el contacto via Whatsapp?</a>  
                                <p id="cuenta-twentytwo" class="collapse  fade" aria-labelledby="headingThree" data-parent="#cuenta">
                                    El contacto via Whatsapp es un beneficio del servicio Premium, debes aprovecharlo al máximo. Si no tienes Whastapp, tus clientes te podrán contactar via chat online las 24hs.
                                </p>
                            </div>
                        </div>
                        <!--PLANES-->
                        <h3 class="dropdown-toggle" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">Planes</h3>
                        <div id="collapseFour" class="collapse show fade" aria-labelledby="headingFour" data-parent="#accordionExample">
                            <div class="accordion" id="plan">
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-one" aria-expanded="true" aria-controls="plan-one" href="">¿Qué beneficios tengo al ser Premium?</a>
                                <p id="plan-one" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    Ser Premium te da la oportunidad de llegar a más clientes, tener siempre tu cuenta activa significa estar disponible a más oportunidades.
                                    Tenes 60 días de prueba Gratis! Invitando a 5 amigos o mas via Facebook, Whatsapp, Twitter o Instagram. Este enlace que compartes con tus amigos los invita a Registrarse por un periodo de 48hs. Se procesa el enlace y si tus amigos se han registrado ya tienes acceso gratis al Plan premium por 60 dias. O si deseas acceder al PLAN PREMIUM sin esperar las 48hs. Click Aquí!
                                    Disfruta del uso Sin anuncios.
                                    Contacta con los Contratantes las 24horas Via Chat.
                                    Contacta con los Contratantes Via Whatsapp.
                                    Recibe una Puntuación y Comentarios acerca de tu Servicio.
                                    Integra tu perfil a Eventos. 
                                    Promociona un Curso.
                                    Plantilla y Formato de CV Gratuito. 
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-two" aria-expanded="true" aria-controls="plan-two" href="">¿Cómo pago plan Premium?</a>
                                <p id="plan-two" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    Podes pagar con todas las tarjetas de crédito.
                                    En bocas de cobranza: Aquí Pago – Pago Express – Infonet – Practipago.
                                    Billeteras: Tigo Money – Zimple –Billetera Personal
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-three" aria-expanded="true" aria-controls="plan-three" href="">¿Cuánto tiempo demora PagoPar en realizar el cobro?</a>
                                <p id="plan-three" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    El tiempo que se demora el debito de un pago depende de las políticas de cada Red de pago.
                                    Aquí pago - Pago Express – Practipago procesan el pago en 24 horas los días hábiles.
                                    Billeteras: Tigo Money – Zimple –Billetera Personal procesan el pago de manera instantánea los días hábiles.
                                    Tarjetas de crédito: procesan los días máximo hasta 48 horas, también esta sujeto a días hábiles.
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-four" aria-expanded="true" aria-controls="plan-four" href="">¿Cómo hago para no recibir anuncios?</a>
                                <p id="plan-four" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    Suscribite al Plan Premium, dentro del plan no incluyen anuncios y además tenes más beneficios.
                                    Agregar el Boton Suscripcion Premium
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-five" aria-expanded="true" aria-controls="plan-five" href="">¿Si no tengo plan no puedo actualizar mi perfil?</a>
                                <p id="plan-five" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    No esperes más para suscribirte a uno de nuestros planes. Si no tenes ningún plan activo, estás perdiendo clientes ya que estos no pueden contactarse contigo y no puede actualizar tus datos.
                                    Agregar Botón Suscripción Premium
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-six" aria-expanded="true" aria-controls="plan-six" href="">¿Cómo puedo saber si se efectuó mi pago?</a>
                                <p id="plan-six" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    Los pagos de las Suscripciones de Activate, las gestionamos via PAGOPAR si tenes alguna consulta referente a los pagos realizado, te recomendamos comunicarte con ellos al  (021) 326 3966       
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-seven" aria-expanded="true" aria-controls="plan-seven" href="">¿Cómo me contactan si no tengo un plan?</a>
                                <p id="plan-seven" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    No esperes más para suscribirte a uno de nuestros planes. Si no tenes ningún plan activo, estás perdiendo clientes ya que estos no pueden contactarse contigo.
                                    Agregar Botón Suscripción Premium
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-eigth" aria-expanded="true" aria-controls="plan-eigth" href="">¿Si no pago a tiempo, desactivan mi cuenta?</a>
                                <p id="plan-eigth" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    No te preocupes, Actívate te enviara un mail de recordando, para evitar que esto pase.
                                    Tu cuenta se desactivara automáticamente luego del plazo establecido. Pero tus datos no se borraran, una vez abonado un PLAN PREMIUM, estarás visible y podran contactarte nuevamente. 
                                    
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-nine" aria-expanded="true" aria-controls="plan-nine" href="">¿Por qué tengo que hacerme Premium?</a>
                                <p id="plan-nine" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    Hacerte Premium te da la ventaja de llegar a más clientes, te permite estar disponible a más oportunidades, a presentar un perfil ordenado y a estar en contacto las 24hs al dia con el Cliente.
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-teen" aria-expanded="true" aria-controls="plan-teen" href="">¿Qué pasa cuando termina mi plan?</a>
                                <p id="plan-teen" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    Cuando termina tu plan, su perfil sigue en la plataforma pero tus clientes ya no podrán ver tu información, ni contactarte.           
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-eleven" aria-expanded="true" aria-controls="plan-eleven" href="">¿Cómo Actívate protege mis informaciones de pagos?</a>
                                <p id="plan-eleven" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    Los pagos  de las Suscripciones actívate se realizan por medio de PagoPar que utiliza el sistema de PAGO SEGURO BANCARD.
                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-twelve" aria-expanded="true" aria-controls="plan-twelve" href="">¿No encontraste respuestas tu pregunta?</a>
                                <p id="plan-twelve" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                    Estamos para ayudarte!
Agregar Botón (Ayuda – Dejanos tu comentario)

                                </p>
                                <a class="dropdown-item faqs hvr-forward" data-toggle="collapse" data-target="#plan-thirteen" aria-expanded="true" aria-controls="plan-thirteen" href="">¿De dónde retiro mi factura?</a>
                                <p id="plan-thirteen" class="collapse  fade" aria-labelledby="headingFour" data-parent="#plan">
                                
                                </p>
                            </div>
                        </div>
                    </div>
        <br>
                   
    <br>
                  
<br>
                   
                  
                </div>

            </div>




<br>
        </div>
    </div>

    @include('web.layouts.footer')
@endsection
@section('scripts')
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
@endsection
