<footer class="footer footer-default">
    <div class=" container ">
        <nav>
            <ul>
                <li>
                    <a href="#">
                        Inicio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Usuarios
                    </a>
                </li>
                <li>
                    <a href="#">
                        Contactos
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright" id="copyright">
            &copy;
            <script>
                document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>
            , Powered by
            <a href="https://www.nano.com.py" target="_blank">@NANODEVELOPERS</a>
        </div>
    </div>
</footer>