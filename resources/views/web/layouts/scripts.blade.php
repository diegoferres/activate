<!--   Core JS Files   -->
<script src="/web/assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="/web/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="/web/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
{{-- NOTIFICATIONS --}}
<script src="/web/assets/js/iziToast.js" type="text/javascript"></script>
<script>
     $('#blogCarousel').carousel({
            interval: 15000
        });

</script>

<script>
    $(document).ready(function () {

        var message = '{!! session('notifications') !!}';

        if (message.length > 0) {
            message = JSON.parse(JSON.stringify({!! session('notifications') !!}));

            switch (message.type) {
                case 'success':
                    iziToast.success({
                        title: message.title,
                        message: message.message,
                        position: 'topRight'
                    });

                    break;
                case 'error':
                    iziToast.error({
                        title: message.title,
                        message: message.message,
                        position: 'topRight'
                    });
                    break;
                case 'info':
                    iziToast.info({
                        title: message.title,
                        message: message.message,
                        position: 'topRight'
                    });
                    break;
                case 'warning':
                    iziToast.warning({
                        title: message.title,
                        message: message.message,
                        position: 'topRight'
                    });
                    break;
            }
        }
    });
</script>

@yield('scripts')
