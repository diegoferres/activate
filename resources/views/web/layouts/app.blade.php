<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/web/assets/img/">
    <link rel="icon" type="image/png" href="/web/assets/img/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @yield('title', 'Actívate')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--Fuente e iconos-->
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">

    <link rel="stylesheet" href="/web/assets/fontawesome5/css/all.css">
    <!--CSS-->
    <link href="/web/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/web/assets/css/now-ui-kit.css?v=1.3.0" rel="stylesheet"/>
    <link rel="stylesheet" href="/web/assets/css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" rel="stylesheet">
    {{-- NOTIFICATIONS --}}
    <link href="\web\assets\css\iziToast.css" rel="stylesheet"/>


    @yield('style')
    <style>
                 .a {
  cursor: url(/web/assets/img/cursor.png), auto;
}
                  .h1 {
  cursor: url(/web/assets/img/cursor.png), auto;
}
                  .h2 {
  cursor: url(/web/assets/img/cursor.png), auto;
}
                  .h3 {
  cursor: url(/web/assets/img/cursor.png), auto;
}
                  .h4 {
  cursor: url(/web/assets/img/cursor.png), auto;
}
                  .h5 {
  cursor: url(/web/assets/img/cursor.png), auto;
}
                  .h6 {
  cursor: url(/web/assets/img/cursor.png), auto;
}
                  .custom {
  cursor: url(/web/assets/img/cursor.png), auto;
}
        </style>
</head>

<body class="@yield('body_open', 'landing-page sidebar-collapse') custom">

    {{--<body class="profile-page sidebar-collapse">--}}

<!-- Navbar -->
@empty($navShow)
    @include('web.layouts.nav')
@endempty
<!-- End Navbar -->

@yield('content')

@include('web.layouts.scripts')
</body>

</html>
