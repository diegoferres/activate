<!-- Navbar -->
<style>
    .dropdown-toggle::after {

        content: none !important;
    }

    .dropdown-menu:before {

        left: 15px !important;

    }

    .dropdown-menu .dropdown-item, .bootstrap-select .dropdown-menu.inner li a {
        font-size: larger !important;

    }
</style>
@if (\Request::route()->getName() == 'web.search')

    <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " style="background-color:#004897">
        @else
            <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="5">

                @endif
                <div class="container">
                    <div class="navbar-translate">
                        <a class="navbar-brand" rel="tooltip" title="Conectando trabajos" data-placement="bottom"
                           href="{{ route('web.index') }}">
                            <span class="icon-logoactivate"></span>
                        </a>
                        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navigation"
                                aria-controls="navigation-index" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar top-bar"></span>
                            <span class="navbar-toggler-bar middle-bar"></span>
                            <span class="navbar-toggler-bar bottom-bar"></span>
                        </button>
                    </div>
                    <div style="    margin-top: 1rem;" class="collapse navbar-collapse justify-content-end"
                         id="navigation"
                         data-nav-image="/web/assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav">
                            @guest
                               {{-- <li class="nav-item">
                                    <a href="{{ route('login') }}" class="nav-link btn  btn-info btn-round btn-lg">Iniciar
                                        Sesión</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('web.index') }}"
                                       class="nav-link btn btn-spotify btn-lg btn-round">No seas Kaigue</a>
                                </li>--}}

                            @else

                                @if (\Request::route()->getName() == 'web.home')
                                    <li class="nav-item">
                                        <h5 style="color:#fff">Bienvenido {{ Auth::user()->name }}</h5>
                                    </li>
                                @endif

                                @if (\Request::route()->getName() == 'web.profile')
                                    <li class="nav-item">
                                        <h5 style="color:#fff">Hola {{ Auth::user()->name }}</h5>
                                    </li>
                                    @if (Auth::id() == $user->id)
                                            <li class="nav-item">
                                                <h5 style="padding-left:1rem; color:#fff" data-toggle="modal"
                                                    data-target="#crearCV">
                                                    Crear CV
                                                </h5>
                                            </li>
                                    @endif
                                @endif
                            @endguest
                            @auth()

                                <li class="nav-item dropdown">
                                    <h5 style="padding-left:1rem; padding-right:1rem; color:#fff"
                                        class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                        <i class="fas fa-bell"> <span class="badge badge-danger">0</span>
                                        </i>
                                    </h5>
                                    <div class="dropdown-menu" style="left:-240px">
                                        <div class="row">
                                            <div class="col-sm-6"></div>
                                            <div class="col-sm-6"></div>
                                        </div>
                                        <a class="dropdown-item" style="display: inline-block;" href="#">
                                            <p> Sin notificaciones</p><br>
                                        </a>
                                        {{--<a class="dropdown-item" style="display: inline-block;" href="#">
                                            <p><i class="far fa-bell"></i> Membresía 60 días</p><br>
                                            <span style="font-size:0.5rem"><i
                                                    class="far fa-clock"></i> 2 min</span>
                                        </a>
                                        <a class="dropdown-item" style="display: inline-block;" href="#">
                                            <p><i class="far fa-bell"></i> Actualizacion a Pro</p><br>
                                            <span style="font-size:0.5rem"><i
                                                    class="far fa-clock"></i> 3 min</span>
                                        </a>
                                        <a class="dropdown-item" style="display: inline-block;" href="#">
                                            <p><i class="far fa-bell"></i> Verificación de cuenta</p><br>
                                            <span style="font-size:0.5rem"><i
                                                    class="far fa-clock"></i> 5 min</span>
                                        </a>--}}

                                    </div>
                                </li>
                                <div class="dropdown button-dropdown">
                                    <a href="" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                                        <span class="button-bar"></span>
                                        <span class="button-bar"></span>
                                        <span class="button-bar"></span>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @yield('navbaroptions')
                                    </div>
                                </div>
                            @endauth


                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
