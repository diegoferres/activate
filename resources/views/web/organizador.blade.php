@extends('web.layouts.app')
@section('title')
    Actívate - Eventos
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-user"></i> Perfil</a>
    {{--@if (Auth::id() == $user->id)
    <a class="dropdown-item" href="#crearCV" data-toggle="modal" data-target="#crearCV"><i class="far fa-address-card"></i> Crear CV</a>
    @endif--}}
    <a class="dropdown-item" href="{{ route('chat') }}"><i class="far fa-comments"></i> Chat</a>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item"><i class="fas fa-undo-alt"></i> Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
@section('body_open')
    profile-page sidebar-collapse
@endsection
@section('style')
    <style>
    .navbar.navbar-transparent {
            background-color: #004897 !important;
            box-shadow: none;
            color: #FFFFFF;
            padding-top: 20px !important;
        }
        .divEventos {
            background-color: #e6e6e66b;
            border-radius: 1rem;
        }
    </style>
@endsection
@section('content')
    <!--vincular con el botom de ver todos de search-->



    <div class="section">
        <div class="container">

            <br>
            <br>
            <h1 class="title" style="text-align: left;">{{ $event->event_title }}</h1>

            <br>
            <div class="large-container">
                <div class="sec-title">
                    <span class="title">Organizador de</span>
                    <h2>{{ $event->event_title }}</h2>
                </div>
            </div>


            <br>

            @foreach($event->subcategories as $subcategories)


            <div class="text-center">
                <h2>{{ $subcategories->subcategory }}</h2>
            </div>
            <br>
            <div class="customer-logos slider text-center">

                            @foreach($subcategories->users as $user)

                            <div class="slide">
                                <img class="rounded-circle" src="{{ !empty($user->avatar) ? asset('storage/users-avatar/'.$user->avatar) : asset('storage/users-avatar/avatar.png') }}">
    <br>
    <h5 class="text-center" style="font-size: 14px;">
    <i class="fas fa-map-marker-alt"></i> {{ isset($user->information->city_name) ? $user->information->city_name : 'Sin dato' }}
                                            <br>{{ $user->user->name }}
    <p class="text-center" style="color: gray;">{{ isset($user->subcategory->subcategory) ? $user->subcategory->subcategory : 'Sin subcategoría' }}</p></h5>
   <div class="text-center">
    <a href="{{ route('web.profile', $user->user->slug) }}" class="btn btn-primary  btn-round" style="font-size:1.2rem">Ver perfil</a>
    </div>
</div>
                                
                            @endforeach
                            
    </div>
        @endforeach
            <!--fin de la primera fila -->


        </div>
    </div>
    @include('web.layouts.footer')
@endsection
@section('scripts')
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
@endsection
