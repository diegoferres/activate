@extends('web.layouts.app')
@section('title')
    Actívate - Eventos
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-user"></i> Perfil</a>
    {{--@if (Auth::id() == $user->id)
    <a class="dropdown-item" href="#crearCV" data-toggle="modal" data-target="#crearCV"><i class="far fa-address-card"></i> Crear CV</a>
    @endif--}}
    <a class="dropdown-item" href="{{ route('chat') }}"><i class="far fa-comments"></i> Chat</a>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item"><i class="fas fa-undo-alt"></i> Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
@section('body_open')
    profile-page sidebar-collapse
@endsection
@section('style')
    <style>
         .navbar.navbar-transparent {
            background-color: #004897 !important;
            box-shadow: none;
            color: #FFFFFF;
            padding-top: 20px !important;
        }

        .divEventos {
            background-color: #e6e6e66b;
            border-radius: 1rem;
        }
    </style>
@endsection
@section('content')







    <div class="section">
        <div class="container">
            <br>
            <br>
            <div class="large-container">
                <div class="sec-title">
                    <span class="title">Organizador de</span>
                    <h2>Eventos</h2>
                </div>
            </div>

            <div class="row">

                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Fiesta para niños </h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/eveniños.jpg" alt="">

                        </div>
                    </div>


                </div>
                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Fiesta para jóvenes</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evejovenes.jpg" alt="">

                        </div>
                    </div>


                </div>
                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Feliz día de la Madre</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evemama.jpg" alt="">

                        </div>
                    </div>


                </div>
            </div>
            <br>
            <div class="row">


                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Feliz día del Padre</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evepapa.jpg" alt="">

                        </div>
                    </div>


                </div>

                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Fiesta temática méxico</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evetemmexico.jpg" alt="">

                        </div>
                    </div>


                </div>
                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Fiesta temática del vino</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evetemvino.jpg" alt="">

                        </div>
                    </div>


                </div>

            </div>

            <br>
            <div class="row">

                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Despedida de solteras</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evetemsoltera.jpg" alt="">

                        </div>
                    </div>


                </div>

                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Fiesta para adultos</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evetemadulto.jpg" alt="">

                        </div>
                    </div>


                </div>
                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Fiesta para egresados</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evetemegresado.jpg" alt="">

                        </div>
                    </div>


                </div>
            </div>
            <br>
            <div class="row">


                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Fiesta para empresas</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evetemcorporativo.jpg" alt="">

                        </div>
                    </div>


                </div>
                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Evento Fútura novia</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evetembridetobe.jpg" alt="">

                        </div>
                    </div>


                </div>
                <div class="col" style="margin: 4px;">
                    <div class="row divEventos">
                        <div class="col-md-4">
                            <h4 style="text-align: left;">Sábado familiar deportivo</h4>

                            <a href="" class="btn btn-info btn-round btn-block">Organizar</a>

                        </div>
                        <div class="col-md-8">
                            <img class="img-fluid" style="border-radius: 15px;"
                                 src="/web/assets/img/categorias/evetemsabadodeportivo.jpg" alt="">

                        </div>
                    </div>


                </div>
            </div>
            <br>


            <br>

        </div>
    </div>
    @include('web.layouts.footer')
@endsection
@section('scripts')
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
@endsection
