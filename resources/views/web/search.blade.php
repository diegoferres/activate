@extends('web.layouts.app')
@section('title')
    Actívate - Buscar
@endsection
@section('style')
    <link rel="stylesheet" href="/web/assets/css/hover.css">
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">

    <style>
         .navbar.navbar-transparent {
            background-color: #004897 !important;
            box-shadow: none;
            color: #FFFFFF;
            padding-top: 20px !important;
        }
        .div_puntuacion {
            color: rgb(255, 230, 0);
            left: -2.2rem;
            position: relative;
            top: 3rem;

        }

        .puntuacion {
            color: rgb(0 0 0);
            position: absolute;
            left: 11px;
            text-align: center;
            font-size: 10px;
            top: 7px;
            font-weight: bold;
        }

        .media-body {
            position: absolute;
            left: 7rem;
        }

        #mapCanvas {
            width: 100%;
            height: 100%;
        }

        .media {
            transition: 0.5s !important;
        }

        .media:hover {
            background-color: lightgrey;
            border-radius: 15px;
        }

        .footer{

            position: absolute;
    bottom: 0;
    width: 100%;
        }

    </style>
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-binoculars"></i>Perfil</a>
    {{--<a class="dropdown-item" href="#" data-toggle="modal" data-target="#crearCV"><i class="far fa-address-card"></i>
        Crear CV</a>--}}

    <a class="dropdown-item" href="/chat"><i class="far fa-address-card"></i>Chat</a>


    {{--<a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>--}}

    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item">Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="far fa-eye"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>

@endsection
@section('body_open')
    profile-page sidebar-collapse
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-user"></i> Perfil</a>
    {{--@if (Auth::id() == $user->id)
    <a class="dropdown-item" href="#crearCV" data-toggle="modal" data-target="#crearCV"><i class="far fa-address-card"></i> Crear CV</a>
    @endif--}}
    <a class="dropdown-item" href="{{ route('chat') }}"><i class="far fa-comments"></i> Chat</a>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item"><i class="fas fa-undo-alt"></i> Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
@section('content')

        <div class="section" style="padding-top:10rem ;">
            <div class="container">
                <h1 style="text-align: left;">Buscando {{ $subc }}</h1>

                @if(!$users)
                    <div class="text-center">
                        <img src="/web/gif/nofound.gif" alt="">
                        <h3>NO SE ENCONTRARON<br>RESULTADOS</h3>
                        <a href="/" style="font-size: 1.2rem;" class="btn btn-round btn-primary btn-lg">Volver al
                            inicio</a>
                    </div>
                @else
                    <div class="sec-title">
                        <h2>Perfiles encontrados</h2>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            @foreach($users as $user)
                                <a href="{{ route('web.profile', $user->slug) }}">
                                <div class="media p-3">
                                    <img src="{{ !empty($user->avatar) ? asset('storage/users-avatar/'.$user->avatar) : asset('storage/users-avatar/avatar.png') }}" alt="{{ $user->name }}" class="mr-3 mt-3 rounded-circle"
                                         style="width:60px;">
                                    <div class="div_puntuacion">
                                        <i class="fas fa-star" style=" font-size:1.5rem;"><span class="puntuacion">1</span></i>
                                    </div>
                                    <div class="media-body">
                                        <h4>{{ $user->name }} <small>
                                                <span class="badge badge-primary">{{ isset($user->cv_information->subcategory->subcategory) ? $user->cv_information->subcategory->subcategory : 'Sin subcategoría' }}</span>
                                            </small>
                                        </h4>
                                    </div>
                                </div>
                                </a>
                            @endforeach
                            {{--<div class="media  p-3 ">
                                <img src="/web/assets/img/ryan.jpg" alt="John Doe" class="mr-3 mt-3 rounded-circle"
                                     style="width:60px;">
                                <div class="div_puntuacion">
                                    <i class="fas fa-star" style=" font-size:1.5rem;"><span class="puntuacion">1</span></i>
                                </div>
                                <div class="media-body">
                                    <h4>Angel Chiguala <small>
                                            <span class="badge badge-primary">Arquitecto</span>
                                        </small>
                                    </h4>
                                </div>
                            </div>
                            <div class="media  p-3">
                                <img src="/web/assets/img/ryan.jpg" alt="John Doe" class="mr-3 mt-3 rounded-circle"
                                     style="width:60px;">
                                <div class="div_puntuacion">
                                    <i class="fas fa-star" style=" font-size:1.5rem;"><span class="puntuacion">1</span></i>
                                </div>
                                <div class="media-body">
                                    <h4>Angel Chiguala <small>
                                            <span class="badge badge-primary">Arquitecto</span>
                                        </small>
                                    </h4>
                                </div>
                            </div>
                            <div class="media  p-3 ">
                                <img src="/web/assets/img/ryan.jpg" alt="John Doe" class="mr-3 mt-3 rounded-circle"
                                     style="width:60px;">
                                <div class="div_puntuacion">
                                    <i class="fas fa-star" style=" font-size:1.5rem;"><span class="puntuacion">1</span></i>
                                </div>
                                <div class="media-body">
                                    <h4>Angel Chiguala <small>
                                            <span class="badge badge-primary">Arquitecto</span>
                                        </small>
                                    </h4>

                                </div>
                            </div>--}}
                        </div>
                        <div class="col-sm-6">
                            <div id="mapCanvas"></div>
                        </div>


                    </div>
                @endif

                <br>

            </div>
        </div>
    @include('web.layouts.footer')
    @endsection
    @section('scripts')
        <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
            <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxIR5t5wcMIWW-63l53acB1ygeVyvFakg&callback=initMap">
            </script>
            <script>
                function initMap() {
                    var map;
                    var bounds = new google.maps.LatLngBounds();
                    var mapOptions = {
                        mapTypeId: 'roadmap'
                    };

                    // Display a map on the web page
                    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
                    map.setTilt(50);

                    // Multiple markers location, latitude, and longitude
                    var markers = [
                        @foreach($users as $user)
                        ['{{ $user->name }}', {{ $user->information->latitud }}, {{ $user->information->longitud }}],
                        @endforeach
                    ];

                    // Info window content
                    var infoWindowContent = [
                        @foreach($users as $user)
                        ['<div class="info_content">' +
                        '<h5>{{ $user->name}}</h5>' +
                        /*'<p>{{--{{ $user->cv_information->subcategory->subcategory }}--}}</p>' + */'</div>'],
                        @endforeach
                    ];

                    // Add multiple markers to map
                    var infoWindow = new google.maps.InfoWindow(), marker, i;

                    // Place each marker on the map
                    for (i = 0; i < markers.length; i++) {
                        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                        bounds.extend(position);
                        marker = new google.maps.Marker({
                            position: position,
                            map: map,
                            title: markers[i][0],
                            label: markers[i][0]
                        });

                        // Add info window to marker
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infoWindow.setContent(infoWindowContent[i][0]);
                                infoWindow.open(map, marker);
                            }
                        })(marker, i));

                        // Center the map to fit all markers on the screen
                        map.fitBounds(bounds);
                       // this.panToBounds(bounds);
                    }

                    // Set zoom level
                    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {

                        //this.setZoom(10);
                        google.maps.event.removeListener(boundsListener);
                    });
                    google.maps.event.addDomListener(window, 'load', initMap);
                }

                // Load initialize function

            </script>
            <script>
                $(document).ready(function () {
                    $('[name="category_id"]').change(function () {

                        $.get('/search/subcategories/' + this.value, function (data) {


                            $('#subcategory_id').find('option')
                                .remove();
                            $.each(data, function (index, value) {
                                $('#subcategory_id').append('<option value="' + value.id + '">' + value.subcategory + '</option>')
                            });

                        });
                    })
                })
            </script>
@endsection
