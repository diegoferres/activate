@extends('web.layouts.app')
@section('title')
    Actívate - Inicio
@endsection
@section('navbaroptions')
    <a class="dropdown-header">Cuenta</a>
    <a class="dropdown-item" href="#"><i class="fas fa-binoculars"></i>Inicio</a>
    <a class="dropdown-item" href="#"><i class="far fa-paper-plane"></i>¿Por que elegirnos?</a>
    <a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Planes disponibles</a>
    <a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Testimonios</a>
    <a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Contactanos</a>
    <div class="dropdown-divider"></div>

    {{--<a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"> <i class="far fa-eye"></i> Ir a mi perfil</a>--}}
@endsection
@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">
    <link rel="stylesheet" href="/web/assets/css/hover.css">


    <style>
        .page-header .page-header-image {

            position: absolute;
            background-size: cover;
            background-position: center center;
            width: 100%;
            height: 100%;
            z-index: auto !important;

        }



        .search-slt {
            display: block;
            width: 100%;
            font-size: 0.875rem;
            line-height: 1.5;
            color: #55595c;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            height: calc(3rem + 2px) !important;
            border-radius: 0;
        }

        .wrn-btn {
            width: 100%;
            font-size: 16px;
            font-weight: 400;
            text-transform: capitalize;
            height: calc(3rem + 2px) !important;
            border-radius: 0;
            position: relative;
            top: -10px;
        }
        .btn-spotify{
            background:#1ed760 !important;
        }
        .pinkBg {
            background-color: #cded18 !important;
            background-image: linear-gradient(90deg, #f2fd55, #cbfd55);
        }

        .bluekBg {
            background-color: #004897 !important;
            background-image: linear-gradient(90deg, #5566fd, #8755fd);
        }

        .intro-banner-vdo-play-btn {
            height: 60px;
            width: 60px;
            position: absolute;
            top: 0%;
            left: 50%;
            text-align: center;
            margin: -30px 0 0 -30px;
            border-radius: 100px;
            z-index: 1
        }

        .intro-banner-vdo-play-btn i {
            line-height: 56px;
            font-size: 30px
        }

        .intro-banner-vdo-play-btn .ripple {
            position: absolute;
            width: 160px;
            height: 160px;
            z-index: -1;
            left: 50%;
            top: 50%;
            opacity: 0;
            margin: -80px 0 0 -80px;
            border-radius: 100px;
            -webkit-animation: ripple 1.8s infinite;
            animation: ripple 1.8s infinite
        }

        @-webkit-keyframes ripple {
            0% {
                opacity: 1;
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @keyframes ripple {
            0% {
                opacity: 1;
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        .intro-banner-vdo-play-btn .ripple:nth-child(2) {
            animation-delay: .3s;
            -webkit-animation-delay: .3s
        }

        .intro-banner-vdo-play-btn .ripple:nth-child(3) {
            animation-delay: .6s;
            -webkit-animation-delay: .6s
        }


    </style>
@endsection
@section('content')
    <div class="wrapper">
        <div class="page-header page-header-small" style="min-height: 100vh;">
            <div class="page-header-image" data-parallax="true">
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner">
                    <div class="carousel-item active">
                            <img src="/web/slider/slider1.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/web/slider/slider2.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/web/slider/slider3.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/web/slider/slider4.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/web/slider/slider5.jpg" class="d-block w-100" alt="...">
                        </div>
                        <!--https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg-->
                    </div>

                </div>
            </div>
            <div class="content-center">

                <div class="row">
                    <div class="col-md-8">

                        <img src="/web/assets/img/activate@2x.png" width="62%" alt="">

                        <br>
                        <br>
                        <h1 style="text-align: left; color:grey; font-family: GalanoGrotesqueAlt;">¡Un ejército de trabajadores
                            disponibles en todo
                            momento!
                        </h1>
                        <p style="color:grey; font-family: GalanoGrotesqueAlt; font-weight: 600;">¡Regístrate y empieza ahora!</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{ route('register') }}" style="height: 4rem;
    font-size: 1.3rem;
    font-family: GalanoGrotesqueAlt;"
                                   class="btn btn-block btn-lg  btn-primary btn-round btn-spotify hvr-bounce-to-left">
                                    Ofrecer
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ route('register') }}"
                                   class="btn btn-block btn-lg btn-info btn-round hvr-bounce-to-right" style="height: 4rem;
    font-size: 1.3rem;
    font-family: GalanoGrotesqueAlt;">
                                    Contratar
                                </a>
                            </div>


                        </div>
                        <hr>
                        <div class="text-center" style="font-size:1.4rem" >
                            <span style="color:grey; font-family: GalanoGrotesqueAlt !important;">───── Ya tenés una cuenta? ─────</span><br>
                            <br>
                            <a href="{{ route('login') }}"
                               {{--data-toggle="modal"--}} style="color:#004897; font-weight:600;">
                                Iniciar sesión</a>
                        </div>

                    </div>

                    <div class="col-md-4"></div>

                </div>
            </div>
        </div>
        <div class="section section-about-us">
            <div class="container">
                <div class="row">


                    <div class="col-md-12 ml-auto mr-auto">
                        <div style="border-radius:15px; padding: 15px; border: 11px solid;    min-height: 38rem;">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" style="border-radius: 15px; font-family: GalanoGrotesqueAlt;">
                                    <div class="carousel-item active">
                                        <div class="container">
                                        <h3 class="title text-center">¿Como funciona ACTIVATE?</h3>
                                        <p>
                                            Imaginate una pagina, donde encontras a profesionales reconocidos y mejores talentos, en todo el pais por zona o categoria.
                                            <br>Por la primera vez en Paraguay, formas parte de una comunidad de trabajo y oportunidades, tenes acceso a ilimitados trabajadores y contratantes de primera categoria como tu.
                                            <br>Permite a los trabajadores crear un perfil profesional, y es garantizado, no necesitas preocuparte por asegurar tu posicion, estas siempre accesible a todas las oportunidades.
                                            <br>Es negociable y tu pones los terminos, asegura que te paguen de una forma facil y a tiempo.
                                            <br>Ayuda a los contratantes a encontrar buenos profesionales, a chatear en tiempo real y en un solo click puedes ver su certificaciones, su formacion y trabajos realizados. Encontra el mejor servicio para tu negocio y hogar.
                                            <br>Activate es abierto para todos, si tenes lo que se necesita y queres unirte, registrate ya!
                                        </p>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="container">
                                            <br>
                                            <h3 class="title text-center">Queres contratar un servicio?</h3>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="row">
                                          <div class="col-sm-4 p-4" data-aos="fade-right" data-aos-duration="1000">
                                              <div>
                                                  <a href="#" class="intro-banner-vdo-play-btn" target="_blank">
                                                      <img src="/web/assets/img/firststar.png" alt="">
                                                      <span class="ripple pinkBg"></span>
                                                      <span class="ripple pinkBg"></span>
                                                      <span class="ripple pinkBg"></span>

                                                  </a>
                                              </div>
                                              <br>
                                              <br>
                                              <div class="text-center" style="font-family:GalanoGrotesqueAlt; font-weight: 600;">

                                                  <br>
                                                  <br>
                                                  <h3>Interactua y selecciona al mejor profesional</h3>
                                                  <p>Tu eliges con quien quieres trabajar, en que zona y el rango de precios que te conviene.</p>

                                              </div>
                                          </div>
                                          <div class="col-sm-4 p-4" data-aos="fade-down" data-aos-duration="1000">
                                              <div>
                                                  <a href="#" class="intro-banner-vdo-play-btn" target="_blank">
                                                      <img src="/web/assets/img/secondstar.png" alt="">
                                                      <span class="ripple pinkBg"></span>
                                                      <span class="ripple pinkBg"></span>
                                                      <span class="ripple pinkBg"></span>
                                                  </a>
                                              </div>
                                              <br>
                                              <br>
                                              <div class="text-center" style="font-family:GalanoGrotesqueAlt; font-weight: 600;">
                                                  <br>
                                                  <br>
                                                  <h3>Revisa los Perfiles</h3>
                                                  <p>Analiza los candidatos, sus habilidades y trabajos realizados para conocerlos a fondo.</p>

                                              </div>
                                          </div>
                                          <div class="col-sm-4 p-4" data-aos="fade-left" data-aos-duration="1000">
                                              <div>
                                                  <a href="#" class="intro-banner-vdo-play-btn" target="_blank">
                                                      <img src="/web/assets/img/thirdstar.png" alt="">
                                                      <span class="ripple bluekBg"></span>
                                                      <span class="ripple bluekBg"></span>
                                                      <span class="ripple bluekBg"></span>

                                                  </a>
                                              </div>
                                              <br>
                                              <br>
                                              <div class="text-center" style="font-family:GalanoGrotesqueAlt; font-weight: 600;">
                                                  <br>
                                                  <br>
                                                  <h3>Chatea en tiempo real</h3>
                                            <p>Contacta en minutos y paga por hora.</p>

                                              </div>
                                          </div>
                                      </div>
                                        <br>
                                      </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="container">
                                            <br>
                                            <h3 class="text-center title">Queres ofrecer un servicio?</h3>
                                                          <div class="row text-center">
                                                              <div class="col">
                                                                  <h4>Crea tu Perfil Profesional</h4>
                                                                  <p>Subi una buena foto de perfil, carga tus datos, documentos y habilitaciones.</p>
                                                              </div>
                                                              <div class="col">
                                                                  <h4>Comparti tu ubicación actual</h4>
                                                                  <p>Consegui trabajos estando en cualquier lugar.</p>
                                                              </div>
                                                              <div class="col">
                                                                  <h4>Chatea en tiempo real</h4>
                                                              <p>Utiliza el chat para ofrecer y elegir el trabajo que mas te convenga. negocia precios y horarios que mejor te queden.</p>
                                                              </div>
                                                              <div class="col">
                                                                  <h4>Forma parte de una comunidad de profesionales</h4>
                                                                  <p>Donde te puedes destacar y compartir trabajos realizados</p>
                                                              </div>
                                                              <div class="col">
                                                                  <h4>Recibe una buena puntuacion por tus trabajos</h4>
                                                                  <p>los contratantes dejan comentarios sobre tu servicio. Dando credibilidad a tu perfil</p>
                                                              </div>
                                                              <div class="col">
                                                                  <h4>Ahorra tiempo</h4>
                                                                  <p>Sin necesidad de firmar contratos o tener horarios fijos. Cuanto mas tiempo estes Online desde la comodidad de tu casa, mas puedes conseguir empleo.</p>
                                                              </div>
                                                          </div>
                                                          <br>
                                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <br>


                    </div>
                </div>
                <div class="text-center">
                    <a href="{{route('web.home')}}" class="btn btn-info btn-round btn-lg"> Ir a la página
                        principal</a>
                </div>

            </div>

        </div>
        <div class="modal" id="mySearch">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="wizard">
                            <div class="wizard-inner text-center">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab"
                                           aria-expanded="true"></a>
                                    </li>
                                    <li role="presentation" class="disabled">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab"
                                           aria-expanded="false"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        {!! Form::open(['route' => ['web.search'], 'method' => 'get', 'class' => 'login-box']) !!}
                        <div class="tab-content" id="main_form">
                            <div class="tab-pane active" role="tabpanel" id="step1">
                                <h4 class="text-center">Selecciona una categoría</h4>
                                <div class="paymentCont">
                                    <div class="paymentWrap">
                                        <div class="btn-group paymentBtnGroup btn-group-justified"
                                             data-toggle="buttons">
                                            <div class="row text-center">
                                                @foreach($categories as $category)
                                                    <div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method">
                                                                <br>
                                                                <i style="font-size: 3rem;"
                                                                   class="{{ $category->img_icon }}"></i><br><br>
                                                                {{ $category->category }}
                                                            </div>
                                                            <input style="opacity: 0;" type="radio"
                                                                   name="category_id"
                                                                   value="{{ $category->id }}">
                                                        </label>
                                                    </div>
                                                @endforeach
                                                {{--<div class="col-sm-4">
                                                    <label class="btn paymentMethod">
                                                        <div class="method">
                                                            <br>
                                                            <i style="font-size: 3rem;" class="fas fa-hard-hat"></i><br><br>
                                                            Construcción
                                                        </div>
                                                        <input style="opacity: 0;" type="radio" name="options">
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="btn paymentMethod">
                                                        <div class="method ">
                                                            <br>
                                                            <i style="font-size: 3rem;"
                                                               class="fas fa-spa"></i><br><br>
                                                            Belleza
                                                        </div>
                                                        <input style="opacity: 0;" type="radio" name="options">
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="btn paymentMethod">
                                                        <div class="method ">
                                                            <br>
                                                            <i style="font-size: 3rem;"
                                                               class="fas fa-chalkboard-teacher"></i><br><br>
                                                            Educación
                                                        </div>
                                                        <input style="opacity: 0;" type="radio" name="options">
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="btn paymentMethod">
                                                        <div class="method ">
                                                            <br>
                                                            <i style="font-size: 3rem;"
                                                               class="fas fa-heartbeat"></i><br><br>
                                                            Bienestar
                                                        </div>
                                                        <input style="opacity: 0;" type="radio" name="options">
                                                    </label>
                                                </div>

                                                <div class="col-sm-4">
                                                    <label class="btn paymentMethod">
                                                        <div class="method">
                                                            <br>
                                                            <i style="font-size:3rem"
                                                               class="fas fa-dice"></i><br><br>
                                                            Entrenimiento
                                                        </div>
                                                        <input style="opacity: 0;" type="radio" name="options">
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="btn paymentMethod">
                                                        <div class="method">
                                                            <br>
                                                            <i style="font-size:3rem;"
                                                               class="fas fa-birthday-cake"></i><br><br>
                                                            Eventos
                                                        </div>
                                                        <input style="opacity: 0;" type="radio" name="options">
                                                    </label>
                                                </div>

                                                <div class="col-sm-4">
                                                    <label class="btn paymentMethod">
                                                        <div class="method">
                                                            <br>
                                                            <i style="font-size: 3rem;"
                                                               class="fas fa-laptop-house"></i><br><br>
                                                            Home Office
                                                        </div>
                                                        <input style="opacity: 0;" type="radio" name="options">
                                                    </label>
                                                </div>

                                                <div class="col-sm-4">
                                                    <label class="btn paymentMethod">
                                                        <div class="method">
                                                            <br>
                                                            <i style="font-size: 3rem;"
                                                               class="fas fa-drafting-compass"></i><br><br>
                                                            Moda
                                                        </div>
                                                        <input style="opacity: 0;" type="radio" name="options">
                                                    </label>
                                                </div>--}}
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" class=" btn btn-round  next-step">Continuar</button>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step2">
                                <h4 class="text-center">¿Qué servicio estas buscando?</h4>
                                <div class="row">
                                    <div class="container">
                                        <div class="form-group">
                                            <label>Servicios disponibles</label>
                                            <select name="subcategory_id" class="form-control" id="subcategory_id">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="button" class="btn btn-round prev-step">Atras</button>
                                    </li>
                                    <li>
                                        <button type="submit" class="btn btn-round next-step">Comenzar busqueda
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

                <!-- Modal footer -->


            </div>
        </div>
        @include('web.modal.modalLogin')
        @include('web.modal.modalRegistro')
        @include('web.modal.modalSuscripcion')
        @include('web.layouts.footer')
    </div>

@endsection
@section('scripts')
    {{--<script type="text/javascript" src="https://demo.themesberg.com/pixel-pro/assets/js/pixel.js"></script>--}}
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="/web/assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    {{--<script src="/web/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>--}}
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="/web/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
    <script>
        AOS.init();
    </script>
    <script>
        $(document).ready(function () {
            $('[name="category_id"]').change(function () {

                $.get('/search/subcategories/' + this.value, function (data) {


                    $('#subcategory_id').find('option')
                        .remove();
                    $.each(data, function (index, value) {
                        $('#subcategory_id').append('<option value="' + value.id + '">' + value.subcategory + '</option>')
                    });

                });
            })
        })
    </script>
@endsection
