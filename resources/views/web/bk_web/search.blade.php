@extends('web.layouts.app')
@section('title')
    Actívate - Buscar
@endsection
@section('navbaroptions')
    <a class="dropdown-header">Cuenta</a>
    <a class="dropdown-item" href="#"><i class="fas fa-binoculars"></i> Vision integral</a>
    <a class="dropdown-item" href="#"><i class="far fa-paper-plane"></i> Planes disponibles</a>
    <a class="dropdown-item" href="#"><i class="far fa-address-card"></i> Crear CV</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
@endsection
@section('body_open')
    profile-page sidebar-collapse
@endsection
@section('content')
    <div class="wrapper">
        <div class="page-header  page-header-small">
            <div class="page-header-image" data-parallax="true"
                 style="background-image:url('/web/assets/img/search.jpg');">
            </div>

            <br>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1 style="text-align: left;">Busca a tu
                            profesional<br>en menos de tres pasos</h1>
                        <br>
                        <div style="text-align: left;">
                            <button class="btn btn-round btn-info btn-lg" data-toggle="modal" data-target="#mySearch"
                                    style="height: 45px;">Seleciona las categorias
                            </button>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <video autoplay width="57%"
                               style="-webkit-mask-box-image: url('https://freepik.cdnpk.net/img/video/deadline/mask.svg'); mask-image: url('');">
                            <source src="https://freepik.cdnpk.net/img/video/deadline/video.mp4" type="video/mp4">
                        </video>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="crearCV">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">

                        <h3>Conectate con otros usuarios</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">

                        <div class="row d-flex justify-content-center">
                            <div class="col-md-4">

                                <img src="/web/assets/img/createProfile.png" class="responsive" style="padding-top:47px"
                                     alt="">


                            </div>
                            <div class="col-md-8">

                                <div class="wizard">
                                    <div class="wizard-inner">

                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#paso1" data-toggle="tab" aria-controls="paso1" role="tab"
                                                   aria-expanded="true"><span class="round-tab">1 </span></a>
                                            </li>
                                            <li role="presentation" class="disabled">
                                                <a href="#paso2" data-toggle="tab" aria-controls="paso2" role="tab"
                                                   aria-expanded="false"><span class="round-tab">2</span></a>
                                            </li>
                                            <li role="presentation" class="disabled">
                                                <a href="#paso3" data-toggle="tab" aria-controls="paso3"
                                                   role="tab"><span
                                                        class="round-tab">3</span></a>
                                            </li>
                                            <li role="presentation" class="disabled">
                                                <a href="#paso4" data-toggle="tab" aria-controls="paso4"
                                                   role="tab"><span
                                                        class="round-tab">4</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <form role="form" action="index.html" class="login-box">
                                    <div class="tab-content" id="main_form">
                                        <div class="tab-pane active" role="tabpanel" id="paso1">
                                            <h4 class="">Cuéntanos, ¿A qué te dedicas?</h4>


                                            <div class="form-group">
                                                <label>Actividad principal</label>
                                                <select name="country" class="form-control" id="country">
                                                    <option value="NG" selected="selected">Nigeria</option>
                                                    <option value="NU">Niue</option>
                                                    <option value="NF">Norfolk Island</option>
                                                    <option value="KP">North Korea</option>
                                                    <option value="MP">Northern Mariana Islands</option>
                                                    <option value="NO">Norway</option>
                                                </select>
                                            </div>


                                            <ul class="list-inline pull-right">
                                                <li>
                                                    <button type="button" class="btn btn-round  next-step">Continuar
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="paso2">
                                            <h4 class="text-center">¿Cuál fue tu último trabajo?</h4>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Cargo *</label>
                                                        <input class="form-control" type="text" name="name"
                                                               placeholder="">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Empresa*</label>
                                                        <input class="form-control" type="text" name="name"
                                                               placeholder="">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Desde *</label>
                                                        <input class="form-control" type="text" name="name"
                                                               placeholder="">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Hasta *</label>
                                                        <input class="form-control" type="text" name="name"
                                                               placeholder="">
                                                    </div>
                                                </div>


                                            </div>


                                            <ul class="list-inline pull-right">
                                                <li>
                                                    <button type="button" class="btn btn-round prev-step">Atras</button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-round next-step">Siguiente
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="paso3">
                                            <h4 class="text-center">¿En qué horario se puede contar contigo?</h4>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="lunes"
                                                               name="lunes">
                                                        <label class="custom-control-label" for="lunes">Lunes</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="martes"
                                                               name="martes">
                                                        <label class="custom-control-label" for="martes">Martes</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="miercoles"
                                                               name="miercoles">
                                                        <label class="custom-control-label"
                                                               for="miercoles">Miercoles</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="jueves"
                                                               name="jueves">
                                                        <label class="custom-control-label" for="jueves">Jueves</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="viernes"
                                                               name="viernes">
                                                        <label class="custom-control-label"
                                                               for="viernes">Viernes</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="sabado"
                                                               name="sabado">
                                                        <label class="custom-control-label" for="sabado">Sábado</label>
                                                    </div>
                                                </div>


                                                <!-- <div class="col-md-4">
                                                  <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                                                    <label class="custom-control-label" for="customCheck">Domingo</label>
                                                  </div>
                                                </div> -->


                                            </div>
                                            <hr>
                                            <div class="row">

                                            </div>
                                            <ul class="list-inline pull-right">
                                                <li>
                                                    <button type="button" class="btn btn-round prev-step">Atras</button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-round next-step">Continuar
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="paso4">
                                            <h4 class="text-center">Por último, comparte tu ubicación.</h4>
                                            <div class="all-info-container">
                                                <iframe
                                                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d115514.76564422595!2d-57.39287371338245!3d-25.208738215081798!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da9e0c97837fb%3A0x2c4f1cde5ecafc2!2sLas%20tacuaras!5e0!3m2!1ses!2spy!4v1598252954874!5m2!1ses!2spy"
                                                    width="100%" height="320" frameborder="0" style="border:0;"
                                                    allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                                            </div>

                                            <ul class="list-inline pull-right">
                                                <li>
                                                    <button type="button" class="btn btn-round prev-step">Volver
                                                    </button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-round next-step">Guardar Datos
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </form>


                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>


        <div class="modal" id="mySearch">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="wizard">
                            <div class="wizard-inner text-center">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab"
                                           aria-expanded="true"></a>
                                    </li>
                                    <li role="presentation" class="disabled">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab"
                                           aria-expanded="false"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        {!! Form::open(['route' => ['web.search'], 'method' => 'get', 'class' => 'login-box']) !!}
                            <div class="tab-content" id="main_form">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <h4 class="text-center">Selecciona una categoría</h4>
                                    <div class="paymentCont">
                                        <div class="paymentWrap">
                                            <div class="btn-group paymentBtnGroup btn-group-justified"
                                                 data-toggle="buttons">
                                                <div class="row text-center">
                                                    @foreach($categories as $category)
                                                        <div class="col-sm-4">
                                                            <label class="btn paymentMethod next-step ">
                                                                <div class="method">
                                                                    <br>
                                                                    <i style="font-size: 3rem;"
                                                                       class="{{ $category->img_icon }}"></i><br><br>
                                                                    {{ $category->category }}
                                                                </div>
                                                                <input style="opacity: 0;" type="radio"
                                                                       name="category_id"
                                                                       value="{{ $category->id }}">
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                    {{--<div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method">
                                                                <br>
                                                                <i style="font-size: 3rem;" class="fas fa-hard-hat"></i><br><br>
                                                                Construcción
                                                            </div>
                                                            <input style="opacity: 0;" type="radio" name="options">
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method ">
                                                                <br>
                                                                <i style="font-size: 3rem;"
                                                                   class="fas fa-spa"></i><br><br>
                                                                Belleza
                                                            </div>
                                                            <input style="opacity: 0;" type="radio" name="options">
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method ">
                                                                <br>
                                                                <i style="font-size: 3rem;"
                                                                   class="fas fa-chalkboard-teacher"></i><br><br>
                                                                Educación
                                                            </div>
                                                            <input style="opacity: 0;" type="radio" name="options">
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method ">
                                                                <br>
                                                                <i style="font-size: 3rem;"
                                                                   class="fas fa-heartbeat"></i><br><br>
                                                                Bienestar
                                                            </div>
                                                            <input style="opacity: 0;" type="radio" name="options">
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method">
                                                                <br>
                                                                <i style="font-size:3rem"
                                                                   class="fas fa-dice"></i><br><br>
                                                                Entrenimiento
                                                            </div>
                                                            <input style="opacity: 0;" type="radio" name="options">
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method">
                                                                <br>
                                                                <i style="font-size:3rem;"
                                                                   class="fas fa-birthday-cake"></i><br><br>
                                                                Eventos
                                                            </div>
                                                            <input style="opacity: 0;" type="radio" name="options">
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method">
                                                                <br>
                                                                <i style="font-size: 3rem;"
                                                                   class="fas fa-laptop-house"></i><br><br>
                                                                Home Office
                                                            </div>
                                                            <input style="opacity: 0;" type="radio" name="options">
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label class="btn paymentMethod">
                                                            <div class="method">
                                                                <br>
                                                                <i style="font-size: 3rem;"
                                                                   class="fas fa-drafting-compass"></i><br><br>
                                                                Moda
                                                            </div>
                                                            <input style="opacity: 0;" type="radio" name="options">
                                                        </label>
                                                    </div>--}}
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="list-inline pull-right">
                                        <li>
                                            <button type="button" class=" btn btn-round  next-step">Continuar</button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="step2">
                                    <h4 class="text-center">¿Qué servicio estas buscando?</h4>
                                    <div class="row">
                                        <div class="container">
                                            <div class="form-group">
                                                <label>Servicios disponibles</label>
                                                <select name="subcategory_id" class="form-control" id="subcategory_id">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="list-inline pull-right">
                                        <li>
                                            <button type="button" class="btn btn-round prev-step">Atras</button>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-round next-step">Comenzar busqueda
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>

                <!-- Modal footer -->


            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <br>
            <br>
            <div class="large-container">
                <div class="sec-title">
                    <span class="title">Tres ({{ $users->count() }})</span>
                    <h2>Perfiles encontrados</h2>
                </div>
            </div>
            <div class="row">
                @empty($users)
                    <h3>NO HAY RESULTADOS</h3>
                @else
                    @foreach ($users as $user)

                        <div class="col-sm-3">
                            <a href="{{ route('web.profile', $user->slug) }}">
                            <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                                <br>
                                <h5 class="text-center" style="font-size: 14px;">{{ $user->name }}
                                    <p class="text-center" style="color: gray;">{{ $user->cv_information->subcategory->subcategory }}</p></h5>
                                <div class="text-center" style="color: yellow;">
                                    @for ($i = 0; $i < $user->information->profile_score; $i++)
                                        <i class="fas fa-star"></i>
                                    @endfor
                                    {{--<i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>--}}
                                </div>
                            </div>
                            </a>
                        </div>
                    @endforeach
                @endempty


                {{--<div class="col-sm-3">
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Rocio del Pilar
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Rocio del Pilar
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Rocio del Pilar
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>--}}

            </div>
            <br>
            <div class=text-center>
                <a href="" class="btn btn-round btn-info btn-lg">Ver todos</a>
            </div>
        </div>
    </div>
    @include('web.layouts.footer')
@endsection
@section('scripts')
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('[name="category_id"]').change(function () {

                $.get('/search/subcategories/' + this.value, function (data) {


                    $('#subcategory_id').find('option')
                        .remove();
                    $.each(data, function (index, value) {
                        $('#subcategory_id').append('<option value="' + value.id + '">' + value.subcategory + '</option>')
                    });

                });
            })
        })
    </script>
@endsection
