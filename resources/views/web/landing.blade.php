@extends('web.layouts.app')
@section('title')
    Actívate - Inicio
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-user"></i> Perfil</a>
    {{--@if (Auth::id() == $user->id)
    <a class="dropdown-item" href="#crearCV" data-toggle="modal" data-target="#crearCV"><i class="far fa-address-card"></i> Crear CV</a>
    @endif--}}
    <a class="dropdown-item" href="{{ route('chat') }}"><i class="far fa-comments"></i> Chat</a>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item"><i class="fas fa-undo-alt"></i> Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="/web/galanoFont/stylesheet.css">
    <link rel="stylesheet" href="/web/assets/css/hover.css">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <style>
        .page-header .page-header-image {
            position: absolute;
            background-size: cover;
            background-position: center center;
            width: 100%;
            height: 100%;
            z-index: auto !important;

        }
        a:link, a:visited, a:active {
    text-decoration:none;
}

        .text-light {
            color: #1ed760 !important;
        }

        .align-middle {
            vertical-align: center;
            position: fixed;
            top: 42rem;
            padding: 0 3rem;
            width: -webkit-fill-available;
            left: 0rem;
        }

        .align-middle-plane {
            vertical-align: center;
            position: fixed;
            top: 30rem;
            padding: 0 3rem;
            width: -webkit-fill-available;
            left: 0rem;
            font-size: 1.2rem;
        }

        .buscador::placeholder {
            font-size: 1rem;
            font-weight: 600;
        }

        .ciu {
            border-top: none !important;
            border-bottom: none !important;
            border-right: none !important;
        }

        .cate {
            border: none !important;

        }


        @media only screen and (max-width: 768px) {

            .ciu {
                border: 1px;
            }

            .cate {
                border: 1px;
            }
        }

        .btn-fafacebook {
            background-color: #004897;
        }

        .btn-fawhatsapp {
            background-color: #25d366;
        }

        .btn-fatwitter {
            background-color: #55acee;
        }

        .btn-fagoogle {
            background-color: #dd3939;

        }

        .search-slt {
            display: block;
            width: 100%;
            font-size: 0.875rem;
            line-height: 1.5;
            color: #55595c;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            height: calc(3rem + 2px) !important;
            border-radius: 0;
        }

        .wrn-btn {
            width: 100%;
            font-size: 16px;
            font-weight: 400;
            text-transform: capitalize;
            height: calc(3rem + 2px) !important;
            border-radius: 0;
            position: relative;
            top: -10px;
        }

        .blog .carousel-indicators {
            left: 0;
            top: auto;
            bottom: -40px;

        }

        /* The colour of the indicators */
        .blog .carousel-indicators li {
            background: #a3a3a3;
            border-radius: 8px;
            width: 50px;
            height: 1px;
        }

        .blog .carousel-indicators .active {
            background: #707070;
        }

        .pinkBg {
            background-color: #cded18 !important;
            background-image: linear-gradient(90deg, #f2fd55, #cbfd55);
        }

        .bluekBg {
            background-color: #004897 !important;
            background-image: linear-gradient(90deg, #5566fd, #8755fd);
        }

        .intro-banner-vdo-play-btn {

            text-align: center;

        }

        .intro-banner-vdo-play-btn i {
            line-height: 56px;
            font-size: 30px
        }

        .card {
            min-height: 36rem;
        }

        .intro-banner-vdo-play-btn .ripple {
            position: absolute;
            width: 160px;
            height: 160px;
            z-index: -1;
            left: 50%;
            top: 50%;
            opacity: 0;
            margin: -80px 0 0 -80px;
            border-radius: 100px;
            -webkit-animation: ripple 1.8s infinite;
            animation: ripple 1.8s infinite
        }

        @-webkit-keyframes ripple {
            0% {
                opacity: 1;
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @keyframes ripple {
            0% {
                opacity: 1;
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                opacity: 0;
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        .intro-banner-vdo-play-btn .ripple:nth-child(2) {
            animation-delay: .3s;
            -webkit-animation-delay: .3s
        }

        .intro-banner-vdo-play-btn .ripple:nth-child(3) {
            animation-delay: .6s;
            -webkit-animation-delay: .6s
        }


        .gallery-item {

            float: left;
            margin-bottom: 2em;
            position: relative;
            height: 22rem;
            width: 100%;
            border-radius: 1px;
            background-position-x: center !important;
        }

        .gallery-item:hover {
            background-color: #55595c3a;
        }


        .gallery-item .img-title .img-titulo {
            opacity: 1;
        }

        .img-title {
            position: relative;
            top: 300px;
            height: 53px;
            width: 100%;
            text-align: left;
            /* margin-left: 1rem; */
            /* opacity: 0; */
            padding: 6px;
            display: none;
            background-color: #463c3c8c;
            /* border-radius: 15px; */
            /* transition: all 0.5s ease; */
            font-weight: 300;
            /* padding: 20%; */
            color: white;
        }

        .btn.btn-icon, .navbar .navbar-nav > a.btn.btn-icon {
            height: 3.375rem;
            min-width: 3.375rem;
            width: 3.375rem;
            padding: 0;
            font-size: 1.500rem;
            overflow: hidden;
            position: relative;
            line-height: normal;
        }

        .img-titulo {
            position: relative;
            top: -53px;
            color: white;
            font-weight: 300;
            width: 100%;
            /* text-align: left; */
            /* margin-left: 16px; */
            background-color: #463c3c8c;
            height: 53px;
            padding: 8px;

        }

        .img-title h5 {
            position: absolute;
            color: #fff;
            top: 33%;
            width: 100%;
            text-align: center;
        }

        .img-title:hover {
            text-shadow: 2px 1px 8px black !important;
        }

        .btn-spotify {
            background: #1ed760 !important;
        }

        .parent {
        }

        .child {

            background-size: cover;
            background-repeat: no-repeat;
            -webkit-transition: all .5s;
            -moz-transition: all .5s;
            -o-transition: all .5s;
            transition: all .5s;
        }

        .parent:hover .child, .parent:focus .child {
            -ms-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -webkit-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
        }

        .parent:hover .child:before, .parent:focus .child:before {
            display: block;
            background-color: #55595c46;
        }

        .parent:hover a, .parent:focus a {
            display: block;
        }

        .child:before {
            content: "";
            display: none;
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;


        }


        /* Media Queries */
        @media screen and (max-width: 960px) {
            .parent {
                width: 100%;
                margin: 20px 0px
            }

            .wrapper {
                padding: 20px 20px;
            }

        }


    </style>
@endsection
@section('content')
    {{--@dd(session('title'))--}}
    <div class="wrapper">
        <div class="page-header page-header-small" style="min-height: 100vh;">
            <div class="page-header-image" data-parallax="true">
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="/web/slider/slider1.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/web/slider/slider2.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/web/slider/slider3.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/web/slider/slider4.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/web/slider/slider5.jpg" class="d-block w-100" alt="...">
                        </div>
                        <!--https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg-->
                    </div>

                </div>
            </div>
            <div class="content-center">

                <div class="row">
                    <div class="col-md-10">

                        <br>
                        <br>
                        <h1 style="text-align: left; color: #004897; font-family: GalanoGrotesqueAlt; font-weight: 600; font-size: 6rem;">
                            Hace más<br>en menos tiempo
                        </h1>


                        {!! Form::open(['route' => ['web.search'], 'method' => 'get', 'class' => 'login-box']) !!}
                        <div class="row" style="background:white; padding:15px; border-radius:5px">

                            <div class="col-lg-5 col-md-5 col-sm-12 p-0">
                                <form class="typeahead" role="search">
                                    <input type="search" name="q"
                                           class="form-control search-slt cate buscador search-input"
                                           placeholder="¿Qué buscas?" autocomplete="off">
                                </form>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 p-0">
                                <input
                                    id="pac-input"
                                    class="form-control search-slt ciu buscador"
                                    type="text"
                                    placeholder="Ciudad donde deseas buscar"
                                />
                                <input type="hidden" name="place_id" id="place_id">
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                                <input type="submit" class="form-control search-slt"
                                       style="background-color: #cf2b83; color:#fff" value="Buscar">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="col-md-2"></div>

                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <h1 class="title" style="font-family: GalanoGrotesqueAlt; font-size: 3rem; font-weight: 600;">
                    Servicios</h1>
                <br>
                <div class="row">

                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 1]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/servicioTecnico.jpg);">
                                <figcaption>
                                    <h3 class="img-title">Servicio Técnico</h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 2]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/construccion.jpg);">
                                <figcaption>
                                    <h3 class="img-title">Construcción</h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 3]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/belleza.jpg);">
                                <figcaption>
                                    <h3 class="img-title">Belleza</h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 4]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/educacion.jpg);">
                                <figcaption>
                                    <h3 class="img-title">Educación</h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 5]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/bienestar.jpg);">
                                <figcaption>
                                    <h3 class="img-title">Bienestar</h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 6]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/entretenimiento.jpg);">
                                <figcaption>
                                    <h3 class="img-title">
                                        Entretenimiento
                                    </h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 7]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/eventos.jpg);">
                                <figcaption>
                                    <h3 class="img-title">
                                        Eventos
                                    </h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 8]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/hogar.jpg);">
                                <figcaption>
                                    <h3 class="img-title">
                                        Hogar
                                    </h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 9]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/homeOffice.jpg);">

                                <figcaption>
                                    <h3 class="img-title">Home Office</h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 10]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/moda.jpg);">

                                <figcaption>
                                    <h3 class="img-title">Moda</h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 11]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/ayuda.jpg);">

                                <figcaption>
                                    <h3 class="img-title">Ayuda</h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-sm-3 parent">
                        <a href="{{ route('web.search', ['category_id' => 12]) }}">
                            <figure class="gallery-item child"
                                    style="background: url(/web/assets/img/categorias/cursos.jpg);">
                                <figcaption>
                                    <h3 class="img-title">
                                        Cursos
                                    </h3>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                </div>
                <br>
                <h2 class="title"
                    style="font-family: GalanoGrotesqueAlt; font-size: 3rem; font-weight: 600; text-align: left;">
                    Servicios para eventos</h2>
                <br>
                <div id="eventoCarousel" class="carousel slide container-blog" data-ride="carousel">
                    <!-- Carousel items -->
                    <div class="carousel-inner" style="box-shadow:none">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="{{ route('web.events') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/eveceremonia.jpg" alt="">
                                        <h3 class="img-titulo">Organiza tu evento </h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'ninos') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/eveniños.jpg" alt="">
                                        <h3 class="img-titulo">Niños</h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'jovenes') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evejovenes.jpg" alt="">
                                        <h3 class="img-titulo">Jóvenes </h3>
                                    </a>
                                </div>
                            </div>
                            <!--.row-->
                        </div>
                        <div class="carousel-item ">
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'dia-de-la-madre') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evemama.jpg" alt="">
                                        <h3 class="img-titulo">Dí­a de la Madre</h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'dia-del-padre') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evepapa.jpg" alt="">
                                        <h3 class="img-titulo">Día del Padre</h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'fiesta-mexicana') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetemmexico.jpg" alt="">
                                        <h3 class="img-titulo">Fiesta Méxicana</h3>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item ">
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'fanaticos-del-vino') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetemvino.jpg" alt="">
                                        <h3 class="img-titulo">Fanáticos del Vino</h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'despedida-de-solteras') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetemsoltera.jpg" alt="">
                                        <h3 class="img-titulo">Despedida de Solteras</h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'adultos') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetemadulto.jpg" alt="">
                                        <h3 class="img-titulo">Adultos</h3>
                                    </a>
                                </div>
                                <!--.row-->
                            </div>
                        </div>
                        <div class="carousel-item ">
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'egresados') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetemegresado.jpg" alt="">
                                        <h3 class="img-titulo">Egresados</h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'empresas') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetemcorporativo.jpg" alt="">
                                        <h3 class="img-titulo">Empresas</h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'futura-novia') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetembridetobe.jpg" alt="">
                                        <h3 class="img-titulo">Fútura Novia</h2>

                                    </a>
                                </div>
                            </div>
                            <!--.row-->
                        </div>
                        <div class="carousel-item ">
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'sabado-familiar-deportivo') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetemsabadodeportivo.jpg" alt="">
                                        <h3 class="img-titulo">Sábado familiar deportivo</h3>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('web.events', 'fiesta-tematica-mexico') }}">
                                        <img class="img-fluid"
                                             src="/web/assets/img/categorias/evetemmexico.jpg" alt="">
                                        <h3 class="img-titulo">Fiesta temática México</h3>
                                    </a>
                                </div>
                            </div>
                            <!--.row-->
                        </div>
                        <!--.item-->

                    </div>
                    <a class="carousel-control-prev" style=" left: -110px !important;" href="#eventoCarousel"
                       role="button"
                       data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" style=" right: -110px !important;" href="#eventoCarousel"
                       role="button"
                       data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--.carousel-inner-->
                    <br>
                    <!-- <ol class="carousel-indicators" >
                    <li data-target="#eventoCarousel" data-slide-to="0"  class="active"></li>
                    <li data-target="#eventoCarousel" data-slide-to="1"></li>
                    </ol> -->
                </div>
            </div>
        </div>
        <div class="section section-team text-center" style="background: url(web/assets/img/ondas2.png); background-repeat: no-repeat;   background-size: contain;
        background-repeat: round;
        min-height: 46rem;">
            <div class="container">
                <h2 class="title"
                    style="color: #fff; font-family: GalanoGrotesqueAlt; font-size: 3rem; font-weight: 600;">
                    Profesionales destacados</h2>
                <br>
                <div class="row blog">
                    <div class="col-md-12">
                        <div id="blogCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Carousel items -->
                            <div class="carousel-inner"
                                 style="background-color: transparent!important; box-shadow: none !important;">
                                <div class="carousel-item active">
                                    <div class="row">
                                        @foreach($bidders as $bidder)
                                            <div class="col" data-aos="fade-left" data-aos-duration="1000">
                                                <a href="{{ route('web.profile', $bidder->slug) }}">
                                                <div class="">
                                                    <div class="text-center">
                                                        <div class="p-4">
                                                        <div style="width:150px; height:150px; border-radius: 100%; background-position: center; background-size: contain; background-repeat: no-repeat; background-image: url({{ !empty($bidder->avatar) ? asset('storage/users-avatar/'.$bidder->avatar) : asset('storage/users-avatar/avatar.png')}});"></div>
                                                    </div>
                                                        <h4 style=" font-size:1rem; color: #fff; margin-top:1rem; margin-bottom:0rem">
                                                            <i class="fas fa-map-marker-alt"></i> {{ isset($bidder->information->city_name) ? $bidder->information->city_name : 'Sin dato' }}
                                                            <br>{{ $bidder->name }}</h4>
                                                        <p class="category text-info">{{ isset($bidder->cv_information->subcategory->subcategory) ? $bidder->cv_information->subcategory->subcategory : 'Sin dato' }}</p>

                                                        <a href="{{ route('web.profile', $bidder->slug) }}"
                                                           style="font-size: 1.2rem;"
                                                           class="btn btn-primary btn-lg btn-round">Ver perfil</a>
                                                    </div>
                                                </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                    <!--.row-->
                                </div>
                                <!--.item-->
                            </div>
                            <!--.carousel-inner-->
                        </div>
                        <!--.Carousel-->
                    </div>
                </div>
            </div>
        </div>
        <div id="navPlanes" class="section section-team text-center">
            <div class="container">
                <h2 class="title" style="font-family: GalanoGrotesqueAlt; font-size: 3rem; font-weight: 600;"> Nuestros
                    planes</h2>

                <div class="row">
                    <div class="col-md-6 col-lg-4" data-aos="fade-down" data-aos-duration="1000">
                        <!-- Pricing Card -->
                        <div class="pricing-card hvr-grow">
                            <div class="card p-4">
                                <!-- Header -->
                                <header class="card-header border-bottom bg-white text-center">
                                  <span class="d-block">
                                    <span class="display-1 font-weight-bold">
                                      <span class="align-top font-medium">Gs.</span>120<span
                                            style="color: gray; font-size: 2rem;">Mil</span>
                                    </span>
                                    <span class="text-light font-small" style="font-size: large;">/ 3 meses</span>
                                  </span>
                                </header>
                                <!-- End Header -->
                                <!-- Content -->
                                <div class="card-body">

                                    <ul class="list-group mb-4 text-left">
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Disfruta del uso sin anuncios
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Contacta las 24hs vía chat o whatsapp
                                            on-Time 24hs
                                        </li>

                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Recibi Puntuación y comentarios de tus Servicios
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Integra tu perfil a Eventos
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Comparti tu ubicación
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Promociona tu curso.
                                        </li>
                                    </ul>
                                    <div class="align-middle-plane">
                                        <a href="{{ route('web.suscription.checkout', 1) }}"
                                           class="btn btn-spotify btn-lg btn-round btn-block"
                                           tabindex="0">Comenzar
                                        </a>
                                    </div>
                                </div>
                                <!-- End Content -->
                            </div>
                        </div>
                        <!-- End of Pricing Card -->
                    </div>
                    <div class="col-md-6 col-lg-4" data-aos="fade-down" data-aos-duration="2000">
                        <!-- Pricing Card -->
                        <div class="pricing-card hvr-grow">
                            <div class="card shadow-soft border-light p-4">
                                <!-- Header -->
                                <header class="card-header border-bottom bg-white text-center">
                                  <span class="d-block">
                                    <span class="display-1 font-weight-bold">
                                      <span class="align-top font-medium">Gs.</span>165<span
                                            style="color: gray; font-size: 2rem;">Mil</span>
                                    </span>
                                    <span class="text-light font-small" style="font-size: large;">/ 6 meses</span>
                                  </span>
                                </header>
                                <!-- End Header -->
                                <!-- Content -->
                                <div class="card-body">

                                    <ul class="list-group mb-4 text-left">
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Disfruta del uso sin anuncios
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Contacta las 24hs vía chat o whatsapp
                                            on-Time 24hs
                                        </li>

                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Recibi Puntuación y comentarios de tus Servicios
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Integra tu perfil a Eventos
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Comparti tu ubicación
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Promociona tu curso.
                                        </li>
                                    </ul>
                                    <div class="align-middle-plane">
                                        <a href="{{ route('web.suscription.checkout', 2) }}"
                                           class="btn btn-spotify btn-lg btn-round btn-block" tabindex="0">Comenzar
                                        </a>
                                    </div>
                                </div>
                                <!-- End Content -->
                            </div>
                        </div>
                        <!-- End of Pricing Card -->
                    </div>
                    <div class="col-md-6 col-lg-4" data-aos="fade-down" data-aos-duration="3000">
                        <!-- Pricing Card -->
                        <div class="pricing-card hvr-grow">
                            <div class="card shadow-soft border-light p-4">
                                <!-- Header -->
                                <header class="card-header border-bottom bg-white text-center">
                                  <span class="d-block">
                                    <span class="display-1 text-tertiary font-weight-bold">
                                      <span class="align-top font-medium">Gs.</span>220<span
                                            style="font-size: 2rem; color: gray;">Mil</span>
                                    </span>
                                    <span class="text-light" style="font-size: large;">/ 365 días</span>
                                  </span>
                                </header>
                                <!-- End Header -->
                                <!-- Content -->
                                <div class="card-body">

                                    <ul class="list-group mb-4 text-left">
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Disfruta del uso sin anuncios
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Contacta las 24hs vía chat o whatsapp
                                            on-Time 24hs
                                        </li>

                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Recibi puntuación y comentarios de tus servicios
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Integra tu perfil a eventos
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Comparti tu ubicación
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Promociona tu curso.
                                        </li>
                                        <li class="list-group-item"><strong><i class="fas fa-check"></i></strong>
                                            Foto profesional del perfil.
                                        </li>

                                    </ul>
                                    <div class="align-middle-plane">
                                        <a href="{{ route('web.suscription.checkout', 3) }}"
                                           class="btn btn-spotify btn-lg btn-round btn-block"
                                           tabindex="0">Comenzar
                                        </a>
                                    </div>
                                </div>
                                <!-- End Content -->
                            </div>
                        </div>
                        <!-- End of Pricing Card -->
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-about-us" style="height: 96rem;
    background-image: url(web/assets/img/ondas2.png);
    background-repeat: no-repeat;
    background-size: cover;">
            <div class="container" style="color: #fff !important;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto text-center">
                                <br>
                                <br>
                                <h2 class="title"
                                    style="font-family: GalanoGrotesqueAlt; font-size: 3rem; font-weight: 600;">¿Por qué
                                    nos eligen?</h2>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="aplicar-step-txt" data-aos="fade-right" data-aos-duration="1000">
                            <h2 style="font-weight: bold;">Transparencia</h2>
                            <p style="font-size: 1.5rem;">Fomentamos una comunidad de talentos personalizados, con datos
                                personales auténticos, comentarios y puntuaciones de contratantes existentes.</p>
                        </div>
                    </div>

                    <div class="col-sm-6">

                    </div>
                    <!------->
                    <div class="col-sm-6">

                    </div>

                    <div class="col-sm-6">
                        <div class=" text-right aplicar-step-txt" data-aos="fade-left" data-aos-duration="2000">
                            <h2 style="font-weight: bold;">Cooperación</h2>
                            <p style="font-size: 1.5rem;">Promovemos la generación de empleos, ingresos y oportunidades
                                de forma responsable y sustentable.</p>
                        </div>
                    </div>
                    <!-------->

                    <div class="col-sm-6">
                        <div class="aplicar-step-txt" data-aos="fade-right" data-aos-duration="3000">
                            <h2 style="font-weight: bold;">Innovación</h2>
                            <p style="font-size: 1.5rem;">Simplificamos contrataciones, procesos de búsqueda y
                                facilitamos la creación de vínculos profesionales.</p>
                        </div>
                    </div>
                    <div class="col-sm-6"></div>
                    <!------->
                    <div class="col-sm-6"></div>

                    <div class="col-sm-6">
                        <div class="text-right aplicar-step-txt" data-aos="fade-left" data-aos-duration="4000">

                            <h2 style="font-weight: bold;">Practicidad</h2>
                            <p style="font-size: 1.5rem;">Es 100% Online, el usuario personaliza su experiencia y
                                controla el servicio que quiere ofrecer o contratar. Acceso instantáneo sin necesidad de
                                descargar una app.</p>
                        </div>
                    </div>
                    <!------>
                    <div class="col-sm-6">
                        <div class="aplicar-step-txt" data-aos="fade-right" data-aos-duration="5000">

                            <h2 style="font-weight: bold;">Flexibilidad</h2>
                            <p style="font-size: 1.5rem;">Elige a tus Clientes como ellos te eligen a tí, maneja los
                                tiempos de trabajo y controlá el servicio que ofreces.</p>
                        </div>
                    </div>
                    <div class="col-sm-6"></div>
                </div>


            </div>


        </div>
        <div class="section">
            <div class="container">

                <div class="row">
                    <div class="col-sm-4 p-4" data-aos="fade-right" data-aos-duration="1000">
                        <div>
                            <a href="#" class="intro-banner-vdo-play-btn" target="_blank">
                                <img src="/web/gif/first.gif" alt="">
                            </a>
                        </div>
                        <div class="text-center"
                             style="color: #004897; font-family:GalanoGrotesqueAlt;">
                            <h3 style="font-weight: bold;">
                                Te consentimos con Reconocimiento. Desde que eres miembro y tienes
                                un CV Completo, te damos una estrella de puntuación.
                            </h3>
                        </div>
                        <div class="text-center align-middle">
                            <a href="@if (Auth::check()){{ route('web.profile', Auth::id()) }}@else#@endif"
                               class=" btn btn-primary btn-lg btn-round btn-block" style="font-size: 1.2rem;">
                                Completar perfil
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4 p-4" data-aos="fade-down" data-aos-duration="1000">
                        <div>
                            <a href="#" class="intro-banner-vdo-play-btn" target="_blank">
                                <img src="/web/gif/second.gif" alt="">
                            </a>
                        </div>
                        <div class="text-center"
                             style="color: #004897; font-family:GalanoGrotesqueAlt; font-weight: 600;">
                            <h3 style="font-weight: bold;">
                                Te premiamos con una Estrella más. Comparti este link a 10 amigos o
                                más, para unirse envialo via Whatsapp Facebook Twitter. Cuantas mas
                                estrellas acumules, mejor auspiciado sera tu perfil.
                            </h3>
                        </div>
                        <div class="text-center align-middle">
                            <a href="" style="font-size: 1.2rem;" class="  btn btn-primary btn-lg btn-round btn-block">
                                Invitar amigos
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4 p-4" data-aos="fade-left" data-aos-duration="1000">
                        <div>
                            <a href="#" class="intro-banner-vdo-play-btn" target="_blank">
                                <img src="/web/gif/third.gif" alt="">
                            </a>
                        </div>
                        <div class="text-center"
                             style="color: #004897; font-family:GalanoGrotesqueAlt; font-weight: 600;">
                            <h3 style="font-weight: bold;">
                                100% Online permanece conectado y aparece en las busquedas de
                                eventos y categorias relacionadas. El que mas rapido contesta a las
                                solicitudes del contratante mas posibilidades tiene de obtener el
                                trabajo.
                            </h3>
                        </div>
                        <div class="text-center align-middle">
                            <a href="" style="font-size: 1.2rem;" class="  btn btn-primary btn-lg btn-round btn-block">
                                Explorar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-team" style="height: 53rem;
        background-image: url(web/assets/img/ondas2.png);
        background-repeat: no-repeat;
        background-size: cover;
        padding-top: 9rem;">

            <div class="container">
                <div class="row">
                    <div class="col-sm-7"
                         style="color: #fff !important; font-family: GalanoGrotesqueAlt;">

                        <h1 class="title"
                            style=" color: #fff; font-family: GalanoGrotesqueAlt; font-size: 4rem; font-weight: 600;">
                            Usar ACTÍVATE es sorprendentemente fácil</h1>


                        <h4>
                            Uso Sin anuncios.<br>
                            Contactar las 24horas Via Chat o Whatsapp.<br>
                            Compartir tu Ubicación.<br>
                            Recibir Comentarios acerca de ti y tu Servicio.<br>
                            Integrar tu perfil a Eventos.<br>
                            Promocionar un Curso.
                        </h4>
                        <div>
                            <h3>Probá 60 días Gratis invitando a 3 amigos
                            </h3>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <!-- AddToAny BEGIN -->
                            {{--<div>
                                <a href="" target="_blank"><img src="https://static.addtoany.com/buttons/a2a.svg" width="32" height="32" style="background-color:royalblue"></a>
                                <a href="F&amp;linkname=" target="_blank"><img src="https://static.addtoany.com/buttons/facebook.svg" width="32" height="32" style="background-color:royalblue"></a>
                                <a href="F&amp;linkname=" target="_blank"><img src="https://static.addtoany.com/buttons/twitter.svg" width="32" height="32" style="background-color:royalblue"></a>
                                <a href="F&amp;linkname=" target="_blank"><img src="https://static.addtoany.com/buttons/gmail.svg" width="32" height="32" style="background-color:royalblue"></a>
                                <a href="F&amp;linkname=" target="_blank"><img src="https://static.addtoany.com/buttons/whatsapp.svg" width="32" height="32" style="background-color:royalblue"></a>
                            </div>--}}
                            <!-- AddToAny END -->
                            <div class="addthis_inline_share_toolbox" Data-URL ="{{ route('web.referred', Auth::user()->referred_code) }}" Data-title ="¡Únete a la plataforma de Actívate!"></div>
                            {{--<a href="http://" class=" btn btn-icon btn-round btn-fafacebook"><i
                                    class="fab fa-facebook-f"></i></a>
                            <a href="http://" class=" btn btn-icon btn-round btn-fawhatsapp"><i
                                    class="fab fa-whatsapp"></i></a>
                            <a href="http://" class=" btn btn-icon btn-round btn-fatwitter"><i
                                    class="fab fa-twitter"></i></a>
                            <a href="http://" class=" btn btn-icon btn-round btn-fagoogle"><i class="fab fa-google"></i></a>--}}

                        </div>


                    </div>
                    <div class="col-sm-5 text-center">
                        <br>
                        <br>
                        <img src="/web/gif/premium.gif" class="img-fluid" alt="">

                    </div>
                </div>
            </div>
        </div>
        <div class="section section-team">
            <div class="container">
                <div class="row text-center">
                    <div class="col-sm-4">
                        <img src="web/gif/count_view.gif" alt="">
                        <h1 style="font-weight: bold; margin-bottom: 0;">7000</h1>
                        <h2>Visitas</h2>
                    </div>
                    <div class="col-sm-4">
                        <img src="web/gif/count_contratos.gif" alt="">
                        <h1 style="font-weight: bold; margin-bottom: 0;">5000</h1>
                        <h2>Contratos</h2>
                    </div>
                    <div class="col-sm-4">
                        <img src="web/gif/count_busqueda.gif" alt="">
                        <h1 style="font-weight: bold; margin-bottom: 0;">10000</h1>
                        <h2>Busquedas</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-team">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6" style="color: #004897;">
                        <br>
                        <h1 class="title"
                            style="font-family: GalanoGrotesqueAlt; font-size: 3rem; font-weight: 600; color: #004897;">
                            ¿Tienes Preguntas?</h1>
                        <h3>Encontrarás las respuestas a tus preguntas y otra información
                            util en Soluciones.
                            También puedes postear preguntas y compartir sugerencias a otros
                            miembros de la <strong>Comunidad
                                Actívate!</strong>
                        </h3>
                        <a href="{{ route('web.preguntas') }}" style="font-size:1.2rem;"
                           class="btn btn-spotify btn-lg btn-round">Ir a la
                            sección</a>

                    </div>
                    <div class="col-sm-6">
                        <img src="/web/gif/preguntas.gif" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>


        @include('web.modal.modalLogin')
        @include('web.modal.modalRegistro')
        @include('web.modal.modalSuscripcion')
        @include('web.layouts.footer')
    </div>

@endsection
@section('scripts')
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="/web/assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="/web/assets/js/plugins/bootstrap-datepicker.js"
            type="text/javascript"></script>
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxIR5t5wcMIWW-63l53acB1ygeVyvFakg&callback=initMap&libraries=places&v=weekly"
        defer
    ></script>
    <script>
        AOS.init();

        $(document).ready(function () {
            $('.gallery-item').ready(function () {
                $(this).find('.img-title').fadeIn(300);
            }, function () {
                $(this).find('.img-title').fadeOut(100);
            });

            /**
             * TYPEAHEAD
             */
                // Set the Options for "Bloodhound" suggestion engine
            var engine = new Bloodhound({
                    remote: {
                        url: '/home/search?q=%QUERY%',
                        wildcard: '%QUERY%'
                    },
                    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, {
                source: engine.ttAdapter(),

                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'usersList',

                // the key from the array we want to display (name,id,email,etc...)
                templates: {
                    empty: [
                        '<div class="list-group search-results-dropdown"><div class="list-group-item">Sin resultados</div></div>'
                    ],
                    header: [
                        '<div class="list-group search-results-dropdown">'
                    ],
                    suggestion: function (data) {
                        return '<a href="/profile/' + data.slug + '" class="list-group-item">' + data.name + '-' + data.cv_information.subcategory.subcategory + '</a>'
                    }
                },
            });
        })

        // optional
        $('#blogCarousel').carousel({
            interval: 5000
        });

        /**
         * GOOGLE FIN PLACE
         */
        "use strict";

        // This sample uses the Place Autocomplete widget to allow the user to search
        // for and select a place. The sample then displays an info window containing
        // the place ID and other information about the place that the user has
        // selected.
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIwzALxUPNbatRBj3Xi1Uhp0fFzwWNBkE&libraries=places">
        function initMap() {

            const input = document.getElementById("pac-input");

            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: 'py'}
            };

            const autocomplete = new google.maps.places.Autocomplete(input, options);

            autocomplete.setFields(["place_id", "geometry", "name"]);

            autocomplete.addListener("place_changed", () => {

                const place = autocomplete.getPlace();

                document.getElementById('place_id').value = place.place_id;
            });
        }

        /**
         * END GOOGLE FIN PLACE
         */
    </script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fd6f353d242424b"></script>
@endsection
