@extends('web.layouts.app')
@section('title')
    Actívate - Buscar
@endsection
@section('navbaroptions')
    <a class="dropdown-header">{{ Auth::user()->name }}</a>
    <a class="dropdown-item" href="{{ route('web.profile', Auth::id()) }}"><i class="fas fa-user"></i> Perfil</a>
    @if (Auth::id() == $user->id)
    <a class="dropdown-item" href="#crearCV" data-toggle="modal" data-target="#crearCV"><i class="far fa-address-card"></i> Crear CV</a>
    @endif
    <a class="dropdown-item" href="{{ route('chat') }}"><i class="far fa-comments"></i> Chat</a>
    <a class="dropdown-item" href="#"> <i class="far fa-eye"></i> Ver como visitante</a>
    <div class="dropdown-divider"></div>
    <a href="/" class="dropdown-item"><i class="fas fa-undo-alt"></i> Ir al inicio</a>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
@section('body_open')
    profile-page sidebar-collapse
@endsection
@section('content')
<!--vincular con el botom de ver todos de search-->
    <div class="wrapper">
        <div class="page-header  page-header-small" >
           <!--cambiar el fondo de acuerdo a la categoria-->
            <div class="page-header-image" data-parallax="true"
            
                 style="background-image:url('/web/assets/img/category/electricidad.jpg');">
            </div>

            <br>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!--Cambiar de acuerdo a la categoria-->
                        <br>
                        <h1 style="text-align: left;">Electricidad</h1>
                        <br>
                        <div style="text-align:left">
                        <a href="" class=" btn btn-round btn-primary btn-lg" data-target="#mySearch" data-toogle="modal"> Realizar otra busqueda</a>
</div>
                       
                    </div>

                   
                </div>
            </div>
        </div>

      


      
    </div>
    <div class="section">
        <div class="container">
            <br>
            <br>
            <div class="large-container">
                <div class="sec-title">
                    <span class="title">Perfiles encontrados</span>
                    <h2>Electricidad</h2>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-3">
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Rocio del Pilar
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Rocio del Pilar
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Rocio del Pilar
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="slide"><img class="rounded-circle" src="/web/assets/img/ryan.jpg">
                        <br>
                        <h5 class="text-center" style="font-size: 14px;">Rocio del Pilar
                            <p class="text-center" style="color: gray;">Developer</p></h5>
                        <div class="text-center" style="color: yellow;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>

            </div>
<br>
  
        </div>
    </div>
    @include('web.layouts.footer')
@endsection
@section('scripts')
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="/web/assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
@endsection
