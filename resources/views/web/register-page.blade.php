@extends('web.layouts.app')

@section('content')
    <div class="wrapper">
        <div class="page-header" style="background-color:rgba(1, 43, 95, -0.178)">
            <div class="page-header-image" style="background-image:url(/web/assets/img/login.jpg)"></div>
            <div class="content">
                <div class="container">


                    <div class="col-md-4 ml-auto mr-auto">
                        <div class="card card-login card-plain">
                            <form class="form" method="" action="">
                                <h3>Crear una cuenta</h3>
                                <div class="card-body">
                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              <i class="now-ui-icons users_circle-08"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Usuario">
                                    </div>
                                    <div class="input-group no-border input-lg">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              <i class="now-ui-icons text_caps-small"></i>
                                            </span>
                                        </div>
                                        <input type="password" placeholder="Contraseña" class="form-control"/>
                                    </div>
                                </div>

                                <div class="card-footer text-center">
                                    <a href="" class="btn btn-primary btn-round btn-lg btn-block">Iniciar
                                        Sesión</a>
                                    <br>
                                    <div class="pull-left">
                                        <h6>
                                            <a href="" class="link" style="color:#fff">Crear Cuenta</a>
                                        </h6>
                                    </div>
                                    <div class="pull-right">
                                        <h6>
                                            <a href="" class="link" style="color:#fff">Olvide mi contraseña</a>
                                        </h6>
                                    </div>

                                </div>


                            </form>

                        </div>
                        <p>Acceder con tus redes sociales</p>
                        <a href="" class="btn btn-round btn-info">Facebook</a>
                        <a href="" class="btn btn-round btn-danger"> Google</a>
                    </div>
                </div>
            </div>
        </div>

        @include('web.layouts.footer')
        @include('web.layouts.scripts')
    </div>
@endsection