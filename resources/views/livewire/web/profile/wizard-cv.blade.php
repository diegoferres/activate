<div>
    <div class="tab-pane active" {{ $currentStep != 1 ? 'style=display:none' : '' }} role="tabpanel" id="paso1">
        <form wire:submit.prevent="firstStep">
            <h3 class="">1. Cuéntanos, ¿A qué te dedicas?</h3>
            <div class="form-group">
                <label for="about">Dinos sobre tí</label>
                <textarea wire:model="about" name="about" id="about" maxlength="300"
                          class="form-control @error('about')is-invalid @enderror" cols="30"
                          rows="10" required></textarea>
                @error('about')
                <span class="invalid-feedback">{{ $message }}</span>
                @enderror
            </div>
            <ul class="list-inline pull-right">
                <li>
                    <button type="submit" class="btn btn-round  next-step">Continuar
                    </button>
                </li>
            </ul>
        </form>
    </div>
    <div class="tab-pane" {{ $currentStep != 2 ? 'style=display:none' : '' }} role="tabpanel" id="paso2">
        <h3>2. Agrega una subcategoría principal</h3>
        <form wire:submit.prevent="secondStep">
            <div class="form-group subcategoria">
                <label>Selecciona tu actividad principal</label>
                {{--<span wire:dirty wire:target="subcategory_id_main">Updating...</span>--}}
                {{--<input wire:model.lazy="foo">--}}
                {!! Form::select('subcategory_id_main', $subcategories , null , ['class' =>
                $errors->has('subcategory_id_main') ? 'custom-select is-invalid' : 'custom-select', 'placeholder' => 'Selecciona la subcategoría', 'wire:model' => 'subcategory_id_main', 'required']) !!}
                @error('subcategory_id_main')
                <span class="invalid-feedback">{{ $message }}</span>
                @enderror
            </div>
            <ul class="list-inline pull-right">
                <li>
                    <button wire:click="back" type="button" class="btn btn-round prev-step">Atras</button>
                </li>
                <li>
                    <button type="submit" class="btn btn-round next-step">Siguiente
                    </button>
                </li>
            </ul>
        </form>
    </div>
    <div class="tab-pane" {{ $currentStep != 3 ? 'style=display:none' : '' }} role="tabpanel" id="paso3">
        <h3>3. Lista de servicios y rango de precios</h3>
        <form wire:submit.prevent="thirdStep">
            <div id="subcategories">
                <div id="subcategory_slot">
                    <label>Lista de servicios y precios</label>

                    <div id="services_id" class="form-inline">
                        <div class="form-group">
                            <label for="inputServicio" class="sr-only">SubCategoría</label>
                            {!! Form::select('subcategory_id', $subcategories , null ,
                            ['class' => $errors->has('subcategory_id') ? 'custom-select is-invalid' : 'custom-select',
                            'placeholder' => 'Selecciona la subcategoría',
                            'wire:model' => 'subcategory_id', 'required']) !!}

                            @error('subcategory_id')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputServicio" class="sr-only">Servicios</label>
                            <input wire:model="service" type="text"
                                   class="form-control @error('service') is-invalid @enderror" name="service" required
                                   id="inputServicio"
                                   placeholder="Servicio">
                            @error('service')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputPrecioDesde" class="sr-only">Precios desde</label>
                            <input wire:model="price_from" type="text"
                                   class="form-control @error('price_from') is-invalid @enderror isnumber" name="price_from"
                                   required
                                   id="inputPrecioDesde"
                                   placeholder="Precios desde">
                            @error('price_from')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="inputPrecioHasta" class="sr-only">Precios hasta</label>
                            <input wire:model="price_to" type="text"
                                   class="form-control @error('price_to') is-invalid @enderror isnumber" name="price_to" required
                                   id="inputPrecioHasta"
                                   placeholder="Precios hasta">
                            @error('price_to')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mx-sm-3 mb-2">
                            {{--<a data-toggle="tooltip" onclick="addService(this); return false;"
                               href="javascript:void(0)" data-placement="right"
                               title="Agregar otro servicio" id="addService"><i
                                    class="fas fa-plus"></i></a>
                            &nbsp;
                            <a data-toggle="tooltip" onclick="deleteService(this); return false;"
                               href="javascript:void(0)" data-placement="right"
                               title="Eliminar servicio" id="deleteService" style="display: none"><i
                                    class="fas fa-minus"></i></a>--}}
                        </div>
                    </div>
                </div>
            </div>

            <!--aca muestra el resuldao-->
            <div id="listSubcategoria"></div>

            <!-- aca llamo a la funcion-->
            {{--<input type="button" id="addProfession" class="btn btn-info btn-round"
                   onclick="addSubcategory()" value="+ Agregar otra profesión"/>
            <input type="button" id="deleteProfession" class="btn btn-warning btn-round"
                   onclick="deleteSubcategory()" style="display: none"
                   value="- Eliminar profesión"/>--}}


            <ul class="list-inline pull-right">
                <li>
                    <button wire:click="back" type="button" class="btn btn-round prev-step">Atras</button>
                </li>
                <li>
                    <button type="submit" class="btn btn-round next-step">Siguiente
                    </button>
                </li>
            </ul>
        </form>
    </div>
    <div class="tab-pane" {{ $currentStep != 4 ? 'style=display:none' : '' }} role="tabpanel" id="paso4">
        <h3>4.¿Has trabajando en una empresa anteriormente?</h3>
        <form wire:submit.prevent="FourthStep">
            <div class="custom-control custom-radio custom-control-inline">
                <input wire:model="pregunta" type="radio" value="1" id="customSi" name="pregunta"
                       {{--onClick="formularCampos()"--}}
                       class="custom-control-input @error('pregunta') is-invalid @enderror" required>
                <label class="custom-control-label" for="customSi">SI</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input wire:model="pregunta" type="radio" value="2" id="customNo" name="pregunta"
                       {{--onClick="formularCampos()"--}}
                       class="custom-control-input @error('pregunta') is-invalid @enderror" required>
                <label class="custom-control-label" for="customNo">NO</label>
            </div>
            <div id=optionWorking>
                @if ($pregunta == 1)
                    <div>
                        <div>
                            <label>¿En donde?</label>
                        </div
                        >
                        <input wire:model="business" type="text"
                               class="form-control @error('business')is-invalid @enderror"
                               placeholder="Institución o empresa" name="business" required>
                        @error('business')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div>
                        <div>
                            <label>¿Cuánto tiempo? (en meses)</label>
                        </div>
                        <input wire:model="antiquity" type="number"
                               class="form-control @error('antiquity')is-invalid @enderror" placeholder="Antigüedad"
                               name="antiquity" required>
                        @error('antiquity')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div>
                        <label>¿Sigues trabajando en la empresa?</label>
                        <br>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input wire:model="current_work" type="radio"
                                       class="form-check-input @error('current_work')is-invalid @enderror"
                                       name="current_work" value="true" required>
                                Si
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input wire:model="current_work" type="radio"
                                       class="form-check-input @error('current_work')is-invalid @enderror"
                                       name="current_work" value="false" required>
                                No
                            </label>
                        </div>
                    </div>
                @elseif ($pregunta == 2)

                    <div>
                        <label>¿Selecciona una de las siguientes opciones?</label><br>
                        {{--<div class="custom-control custom-radio custom-control-inline">
                            <input wire:model="pregunta" type="radio" value="1" id="customSi" name="pregunta"
                                   --}}{{--onClick="formularCampos()"--}}{{--
                                   class="custom-control-input @error('work_status') is-invalid @enderror">
                            <label class="custom-control-label" for="customSi">Trabajo independientemente</label>
                        </div>--}}
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input wire:model="work_status" type="radio"
                                       class="form-check-input @error('work_status')is-invalid @enderror"
                                       name="work_status"
                                       value="Trabajo independientemente" required>Trabajo independientemente
                            </label>
                        </div>
                        <br>

                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input wire:model="work_status" type="radio"
                                       class="form-check-input @error('work_status')is-invalid @enderror"
                                       name="work_status"
                                       value="Estoy desempleado" required>Estoy desempleado
                            </label>
                        </div>
                        <br>

                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input wire:model="work_status" type="radio"
                                       class="form-check-input @error('work_status')is-invalid @enderror"
                                       name="work_status"
                                       value="Estoy buscando ganar un extra" required>Estoy buscando ganar un extra
                            </label>
                        </div>
                    </div>
                @endif
            </div>

            <ul class="list-inline pull-right">
                <li>
                    <button wire:click="back" type="button" class="btn btn-round prev-step">Atras</button>
                </li>
                <li>
                    <button type="submit" class="btn btn-round next-step">Siguiente
                    </button>
                </li>
            </ul>
        </form>
    </div>
    <div class="tab-pane" {{ $currentStep != 5 ? 'style=display:none' : '' }} role="tabpanel" id="paso5">
        <h3>5. ¿En qué horario te gustaria trabajar</h3>
        <h4>Selecciona el día y horario</h4>
        <form wire:submit.prevent="FifthStep">
            <div id="schedules">

                <div id="schedules_id" class="form-inline">
                    {{--@foreach ($posts as $index => $post)
                        <div wire:key="post-field-{{ $post->id }}">
                            <input type="text" wire:model="posts.{{ $index }}.title">

                            <textarea wire:model="posts.{{ $index }}.content"></textarea>
                        </div>
                    @endforeach--}}
                    <div class="form-group  mx-sm-3 mb-2">
                        <label for="inputServicio" class="sr-only">Día</label>
                        {!! Form::select('day', $days , null , ['class' => $errors->has('day') ? 'form-control is-invalid' : 'form-control' , 'placeholder' => 'Elige el día', 'wire:model' => 'day', 'required']) !!}
                        @error('day')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPrecioDesde" class="sr-only">Desde</label>
                        <input wire:model="hour_from" type="time"
                               class="form-control @error('hour_from') is-invalid @enderror" name="hour_from"
                               placeholder="Desde" required>
                        @error('hour_from')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPrecioHasta" class="sr-only">Hasta</label>
                        <input wire:model="hour_to" type="time"
                               class="form-control @error('hour_to') is-invalid @enderror" name="hour_to"
                               placeholder="Hasta" required>
                        @error('hour_to')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mx-sm-3 mb-2">
                        {{--<a data-toggle="tooltip" onclick="addSchedule(this); return false;"
                           href="javascript:void(0)" data-placement="right"
                           title="Agregar horario" id="addSchedule"><i
                                class="fas fa-plus"></i></a>
                        &nbsp;
                        <a data-toggle="tooltip" onclick="deleteSchedule(this); return false;"
                           href="javascript:void(0)" data-placement="right"
                           title="Eliminar horario" id="deleteSchedule" style="display: none"><i
                                class="fas fa-minus"></i></a>--}}
                    </div>
                </div>
            </div>

            <ul class="list-inline pull-right">
                <li>
                    <button wire:click="back" type="button" class="btn btn-round prev-step">Atras</button>
                </li>
                <li>
                    <button type="submit" class="btn btn-round next-step">Siguiente
                    </button>
                </li>
            </ul>
        </form>
    </div>
    <div class="tab-pane" {{ $currentStep != 6 ? 'style=display:none' : '' }} role="tabpanel" id="paso6">
        <h3>6. Formación Académica</h3>
        <h4>Estudios secundarios</h4>
        <form wire:submit.prevent="SixthStep">
            <div>
                <div id="colegio" class="form-inline">
                    <div class="form-group  mx-sm-1 mb-2">
                        <label for="inputcolegio" class="sr-only">Colegio</label>
                        <input wire:model="institution" type="text"
                               class="form-control @error('institution') is-invalid @enderror" name="institution"
                               placeholder="Colegio">
                        @error('institution')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputAñoInicial" class="sr-only">Año Inicial</label>
                        <input wire:model="from_year" type="number" min="1940" max="{{ date('Y') }}"
                               class="form-control @error('from_year') is-invalid @enderror" name="from_year"
                               id="inputAñoInicial"
                               placeholder="Año inicial">
                        {{--@error('from_year')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputAñoFinal" class="sr-only">Año final</label>
                        <input wire:model="to_year" type="number" min="1940" max="{{ date('Y') }}"
                               class="form-control @error('to_year') is-invalid @enderror" name="to_year"
                               id="inputAñoFinal"
                               placeholder="Año final">
                        {{--@error('to_year')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputTituloColegio" class="sr-only">Título</label>
                        <input wire:model="title" type="text" name="title"
                               class="form-control @error('title') is-invalid @enderror"
                               placeholder="Título obtenido">
                        {{--@error('title')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <input wire:model="academic_file" type="file" name="academic_file"
                           class="form-control @error('academic_file') is-invalid @enderror"
                           placeholder="Título obtenido">
                    <div class="mx-sm-1 mb-2">
                        {{--<a data-toggle="tooltip" onclick="addStudiesA(this); return false;"
                           href="javascript:void(0)" data-placement="right"
                           title="Agregar horario" id="addStudiesA"><i
                                class="fas fa-plus"></i></a>
                        &nbsp;
                        <a data-toggle="tooltip" onclick="deleteStudiesA(this); return false;"
                           href="javascript:void(0)" data-placement="right"
                           title="Eliminar horario" id="deleteStudiesA" style="display: none"><i
                                class="fas fa-minus"></i></a>--}}

                    </div>
                </div>
            </div>

            <h4>Estudios Universitarios</h4>
            <div>
                <div id="colegio" class="form-inline">
                    <div class="form-group  mx-sm-1 mb-2">
                        <label for="inputcolegio" class="sr-only">Universidad</label>
                        <input wire:model="institution_1" type="text"
                               class="form-control @error('institution_1') is-invalid @enderror" name="institution_1"
                               placeholder="Universidad">
                        {{--@error('institution_1')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputAñoInicial" class="sr-only">Año Inicial</label>
                        <input wire:model="from_year_1" type="number" min="1940" max="{{ date('Y') }}"
                               class="form-control @error('from_year_1') is-invalid @enderror" name="from_year_1"
                               id="inputAñoInicial"
                               placeholder="Año inicial">
                        {{--@error('from_year_1')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputAñoFinal" class="sr-only">Año final</label>
                        <input wire:model="to_year_1" type="number" min="1940" max="{{ date('Y') }}"
                               class="form-control @error('to_year_1') is-invalid @enderror" name="to_year_1"
                               id="inputAñoFinal"
                               placeholder="Año final">
                        {{--@error('to_year_1')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputTituloColegio" class="sr-only">Título</label>
                        <input wire:model="title_1" type="text" name="title_1"
                               class="form-control @error('title_1') is-invalid @enderror"
                               placeholder="Título obtenido">

                        {{--@error('title_1')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <input wire:model="academic_file_1" type="file" name="academic_file_1"
                           class="form-control @error('academic_file_1') is-invalid @enderror"
                           placeholder="Título obtenido">
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputTituloColegio" class="sr-only">Documento</label>
                        <input wire:model="academic_file_1" type="file" name="academic_file_1"
                               class=" @error('academic_file_1') is-invalid @enderror"
                               placeholder="Título obtenido">
                        {{--@error('title_1')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <div class="mx-sm-1 mb-2">
                        {{--<a data-toggle="tooltip" onclick="addStudiesA(this); return false;"
                           href="javascript:void(0)" data-placement="right"
                           title="Agregar horario" id="addStudiesA"><i
                                class="fas fa-plus"></i></a>
                        &nbsp;
                        <a data-toggle="tooltip" onclick="deleteStudiesA(this); return false;"
                           href="javascript:void(0)" data-placement="right"
                           title="Eliminar horario" id="deleteStudiesA" style="display: none"><i
                                class="fas fa-minus"></i></a>--}}

                    </div>
                </div>
            </div>
            <h4>Estudios Terciarios</h4>
            <div>
                <div id="colegio" class="form-inline">
                    <div class="form-group  mx-sm-1 mb-2">
                        <label for="inputcolegio" class="sr-only">Institución</label>
                        <input wire:model="institution_2" type="text"
                               class="form-control @error('institution_2') is-invalid @enderror" name="institution_2"
                               placeholder="Institución">
                        {{--@error('institution_2')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputAñoInicial" class="sr-only">Año Inicial</label>
                        <input wire:model="from_year_2" type="number" min="1940" max="{{ date('Y') }}"
                               class="form-control @error('from_year_2') is-invalid @enderror" name="from_year_2"
                               id="inputAñoInicial"
                               placeholder="Año inicial">
                       {{-- @error('from_year_2')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputAñoFinal" class="sr-only">Año final</label>
                        <input wire:model="to_year_2" type="number" min="1940" max="{{ date('Y') }}"
                               class="form-control @error('to_year_2') is-invalid @enderror" name="to_year_2"
                               id="inputAñoFinal"
                               placeholder="Año final">
                        @error('to_year_2')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mx-sm-1 mb-2">
                        <label for="inputTituloColegio" class="sr-only">Título</label>
                        <input wire:model="title_2" type="text" name="title_2"
                               class="form-control @error('title_2') is-invalid @enderror"
                               placeholder="Título obtenido">
                        {{--@error('title_2')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror--}}
                    </div>
                    <input wire:model="academic_file_2" type="file" name="academic_file_2"
                           class="form-control @error('academic_file_2') is-invalid @enderror"
                           placeholder="Título obtenido">
                    <div class="mx-sm-1 mb-2">
                        {{--<a data-toggle="tooltip" onclick="addStudiesA(this); return false;"
                           href="javascript:void(0)" data-placement="right"
                           title="Agregar horario" id="addStudiesA"><i
                                class="fas fa-plus"></i></a>
                        &nbsp;
                        <a data-toggle="tooltip" onclick="deleteStudiesA(this); return false;"
                           href="javascript:void(0)" data-placement="right"
                           title="Eliminar horario" id="deleteStudiesA" style="display: none"><i
                                class="fas fa-minus"></i></a>--}}

                    </div>
                </div>
            </div>
            <ul class="list-inline pull-right">
                <li>
                    <button wire:click="back" type="button" class="btn btn-round prev-step">Atras</button>
                </li>
                <li>
                    <button type="submit" class="btn btn-round next-step">Siguiente
                    </button>
                </li>
            </ul>
        </form>
    </div>
    <div class="tab-pane" {{ $currentStep != 7 ? 'style=display:none' : '' }} role="tabpanel" id="paso7">

        <h3>7. Selecciona tu ubicación</h3>
        <form wire:submit.prevent="SeventhStep">
            <input wire:model="latitud" type="hidden" name="latitud" id="latitud">
            <input wire:model="longitud" type="hidden" name="longitud" id="longitud">
            <input wire:model="city_name" type="hidden" name="city_name" id="city_name">
            <input wire:model="city_place_id" type="hidden" name="city_place_id" id="city_place_id">

            <div class="all-info-container">
                <div wire:ignore style="height: 200px" id="map"></div>
            </div>
            <ul class="list-inline pull-right">
                <li>
                    <button wire:click="back" type="button" class="btn btn-round prev-step">Atras</button>
                </li>
                <li>
                    <button type="submit" class="btn btn-round next-step">Guardar datos
                    </button>
                </li>
            </ul>

        </form>
    </div>
</div>
